class Config {
    static get env() {
        return process.env.NODE_ENV || 'production';
    }
    static get adminEmail() {
        return `admin@${this.domainName}`;
    }
    static get supportEmail() {
        return `support@${this.domainName}`;
    }
    static get domainName() {
        return process.env.DOMAIN_NAME || 'bshelf.ru';
    }
    static get port() {
        return process.env.PORT || 3000;
    }
    static get sessionKey() {
        return 'afjFSE@;fASz,.xpcsjsa@';
    }
    static get sessionCollection() {
        return 'sessions';
    }
    static get smtpHost() {
        return process.env.SMTP_HOST || 'smtp.gmail.com';
    }
    static get smtpSSL() {
        return process.env.SMTP_SSL || true;
    }
    static get smtpPort() {
        return process.env.SMTP_PORT || 465;
    }
    static get smtpUser() {
        return process.env.SMTP_USER || 'myhobbymylive128@gmail.com';
    }
    static get smtpPass() {
        return process.env.SMTP_PASSWORD || 'ghbdtnRfrEdfc2[Ltkf';
    }
    static get mongoHost() {
        return process.env.MONGO_HOST || 'localhost';
    }
    static get mongoPort() {
        return process.env.MONGO_PORT || '27017';
    }
    static get mongoDBName() {
        return process.env.MONGO_DB_NAME || 'bshelf';
    }
    static get mongoUrl() {
        return `mongodb://${this.mongoHost}:${this.mongoPort}/${this.mongoDBName}`;
    }
    static get vk() {
        return {
            clientId: '5675559',
            clientSecret: 'Trsrnsfn1Tz8ZE94xNsM'
        }
    }
    static get modules() {
        return {
            library: false,
            audiobooks: true
        };
    }
}

module.exports = Config;