const fs = require('fs');
const _ = require('lodash');
const path = require('path');
const cheerio = require('cheerio');
const request = require('request');
const Promise = require('bluebird');


class Scrapper {
    constructor(options = null) {
        this.options = options;
        this.currentPage = '/';
        this.__mkdir = Promise.promisify(fs.mkdir);
        this.__writeFile = Promise.promisify(fs.writeFile);
        this.__unlink = Promise.promisify(fs.unlink);
        this.__access = Promise.promisify(fs.access);
    }

    async ensureFolder(prefix, _path = [], ensureRoot = true) {
        if (ensureRoot) {
            prefix = path.join(__dirname, prefix);
            console.info(`Ensure path: ${prefix}`);
        }

        let mask = '0777';
        let pathToEnsure = prefix;

        if (!ensureRoot) {
            if (_path.length > 0) {
                pathToEnsure = path.join(pathToEnsure, _path[0]);
                _path.shift();
                console.info(`Ensure path: ${pathToEnsure}`);
            }
        }

        try {
            await this.__mkdir(pathToEnsure, mask);
            if (_path.length > 0) {
                return await this.ensureFolder(pathToEnsure, _path, false);
            } else {
                return pathToEnsure;
            }
        } catch (err) {
            if (err.code == 'EEXIST') {
                if (_path.length > 0) {
                    return await this.ensureFolder(pathToEnsure, _path, false);
                } else {
                    return pathToEnsure;
                }
            } else {
                throw new Error(err);
            }
        }
    }

    async isFileExists(relativePath) {
        let _path = path.join(__dirname, relativePath);
        try {
            await this.__access(_path, fs.constants.R_OK | fs.constants.W_OK);
            console.info(`File exists: ${relativePath}`);
            return true;
        } catch(err) {
            console.info(`File not exists: ${relativePath}`);
            return false;
        }
    }

    async deleteFile(relativePath) {
        try {
            await this.__unlink(path.join(__dirname, relativePath));
            console.info(`Delete file: ${relativePath}`);
        } catch (err) {
            if (err.code != 'ENOENT') {
                throw new Error(err);
            }
        }
    }

    async saveFile(relativePath, content) {
        await this.__writeFile(path.join(__dirname, relativePath), content);
    }

    async httpGet(params) {
        return new Promise((resolve, reject) => {
            console.info(`HTTP GET: ${params.url}`);
            request(params, (err, response, body) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(response);
                }
            });
        });
    }

    async getPages() {
        let data = await this.httpGet({url: this.options.url});
        let $ = cheerio.load(data.body);
        let links = $('.pagination').find('li').map(function () {
            if (this.children &&
                this.children.length > 0 &&
                this.children[0].children &&
                this.children[0].children.length > 0) {

                if (this.children[0].children[0].data == 'последняя') {

                    let url = this.children[0].children[0].parent.attribs.href;
                    let urlParts = url.split('/');
                    let page = parseInt(urlParts[4].replace('page', ''), 10);

                    return {
                        text: page
                    }
                } else {
                    return {
                        text: parseInt(this.children[0].children[0].data, 10)
                    }
                }
            } else {
                return {
                    text: null
                }
            }
        });

        links = _.filter(links, (link) => {
            return !_.isNaN(link.text);
        });

        if (links.length > 0) {
            return links[links.length - 1].text;
        } else {
            return 0;
        }
    }

    async getBookDetails(url) {
        let response = await this.httpGet({
            url: url
        });

        let $ = cheerio.load(response.body);
        let details = {
            year: parseInt($('.fa-calendar-o').parent().find('a').text(), 10),
            description: _.trim($('.topic-content.text.text-inside').text())
        };

        if (!_.isNumber(details.year) || _.isNaN(details.year)) {
            details.year = new Date().getFullYear();
        }

        let scripts = $('script');
        let ajaxParams = null;
        scripts.each((i, el) => {
            if (_.startsWith(_.trim($(el).html()), '$(document).audioPlayer(')) {
                let scriptContent = _.trim($(el).text());
                scriptContent = scriptContent.replace('$(document).audioPlayer(', '');
                let id = parseInt(scriptContent.split(',')[0], 10);
                let realm = parseInt(scriptContent.split(',')[1].replace(');', ''), 10);
                ajaxParams = {
                    id: id,
                    realm: realm
                };
            }
        });

        if (ajaxParams) {
            let ajaxResponse = await this.httpGet({
                url: `${this.options.url}/rest/bid/${ajaxParams.id}`
            });
            try {
                details.json = JSON.parse(ajaxResponse.body);
            } catch (err) {
                console.warn('Error parse json');
                details.json = null;
            }

        }

        return details;
    }

    async getPageBooks(page) {

        let response = await this.httpGet({
            url: `${this.options.url}/index/page${page}/`
        });

        let $ = cheerio.load(response.body);
        let articles = $('.content-inner article');

        let data = articles.map((i, article) => {

            let author = _.trim($(article).find('a[rel="author"]').text());
            let title = _.trim($(article).find('.topic-title').text());

            try {
                if (_.isEmpty(title)) throw new Error('Title is empty');
                let re = new RegExp(' - ','gi');
                let countToPayAttention = title.match(re);
                if (countToPayAttention) countToPayAttention = countToPayAttention.length;

                if (countToPayAttention > 1) {
                    console.warn(`ATTENTION: Please Review title - pre parse value ${title}`);
                }
                if (!_.isEmpty(author)) {
                    title = _.slice(title, (author + ' - ').length).join('');
                } else {
                    author = '__NO_AUTHOR__';
                }

                if (countToPayAttention > 1) {
                    console.warn(`ATTENTION: Please Review title - post parse value ${title}`);
                }

            } catch (err) {
                console.error(err);
                throw new Error(`Cant parse title ${this.options.url}/index/page${page}/`);
            }

            let series = $(article).find('.fa-book').parent();
            if (series.length > 0) {
                series = {
                    text: _.trim($(series).find('a').text()),
                    url: _.trim($(series).find('a').attr('href'))
                }
            } else {
                series = null;
            }
            let data = {
                url: $(article).find('.topic-header .topic-title a').attr('href'),
                title: title,
                author: author,
                genre: _.trim($(article).find('.topic-info .topic-blog').text()),
                performer: _.trim($(article).find('a[rel="performer"]').text()),
                series: series,
                image: $(article).find('.topic_preview').attr('src')
            };
            return data;
        });

        let result = [];
        for (let i = 0; i < data.length; i++) {
            let bookScrapped = await this.isFileExists(`data/${data[i].genre}/${data[i].author}/${data[i].url.replace(this.options.url + '/', '')}.json`);
            if (!bookScrapped) {
                data[i].details = await this.getBookDetails(data[i].url);
                if (data[i].details.json != null) {
                    result.push(data[i]);
                }
            }
        }

        return result;
    }

    async saveToFile(data) {
        await this.ensureFolder('data', [data.genre, data.author]);

        let fileContent = JSON.stringify(data, null, 2);
        let fileName = data.url.replace(`${this.options.url}/`, '') + '.json';

        await this.saveFile(`data/${data.genre}/${data.author}/${fileName}`, fileContent);
    }

    async run() {
        let pages = await this.getPages();
        if (!pages) return;

        let pagesToScrap = process.env.PAGES || pages;
        pagesToScrap = parseInt(pagesToScrap, 10);

        if (pagesToScrap > pages) {
            pagesToScrap = pages;
        }

        for (let i = 1; i <= pagesToScrap; i++) {
            let pageBooks = await this.getPageBooks(i);
            for (let j = 0; j < pageBooks.length; j++) {
                await this.saveToFile(pageBooks[j]);
            }
        }
        console.info('Scrap complete');
    }
}

let scrapper = new Scrapper({
    url: 'https://audioknigi.club'
});

scrapper.run().then(() => process.exit()).catch(err => {
    console.error(err);
    process.exit(1);
});