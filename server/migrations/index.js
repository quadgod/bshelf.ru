module.exports = async (env) => {
    if (env === 'development') {
        console.log('running seed/dev.user');
        await require('./seed/dev.user')();
        console.log('running seed/dev.medias');
        await require('./seed/dev.medias')();
        console.log('running seed/dev.books');
        await require('./seed/dev.books')();
    }
    return true;
};