const BookType = require('../../models/BookType');
const EntityType = require('../../models/EntityType');
const UserService = require('../../services/userService');
const BookService = require('../../services/bookService');
const MediaService = require('../../services/mediaService');
const BookAuthorService = require('../../services/bookAuthorService');
const BookPerformerService = require('../../services/bookPerformerService');

const description = 'Продолжается полная опасностей и приключений жизнь Алекса Эльфа (он же – Алексей Ветров) в королевстве Мардинан. Разбиты воинственные кочевники, создан скрепленный кровными узами Новый Союз, настало время мирного созидательного труда. Алекс наконец-то добирается до Подгорного королевства – и выясняется, что не всем по нраву этот Новый Союз. Привыкшая быть лидером, Империя не желает мириться с переменами на политической карте.';

module.exports = async () => {
    let bookService = new BookService();
    let userService = new UserService();
    let mediaService = new MediaService();
    let bookAuthorService = new BookAuthorService();
    let bookPerformerService = new BookPerformerService();

    let fantasyCategory = await bookService.one({
        query: {
            type: EntityType.FOLDER,
            parent: null,
            url: 'fantasy'
        }
    });

    let detectiveCategory = await bookService.one({
        query: {
            type: EntityType.FOLDER,
            parent: null,
            url: 'detective'
        }
    });

    let romanCategory = await bookService.one({
        query: {
            type: EntityType.FOLDER,
            parent: null,
            url: 'roman'
        }
    });

    let user = await userService.one({query: {email: 'dbseed@test.test'}});
    if (!user) {
        console.error('seed: medias user not found');
        return;
    }

    let author = await bookAuthorService.one({
        query: {
            fullName: 'Александр Сергеевич Пушкин'
        }
    });
    let performer = await bookPerformerService.one({
        query: {
            fullName: 'Максим Галкин'
        }
    });

    if (!author) {
        console.log('seed: author create');
        author = await bookAuthorService.add({
            model: {
                fullName: 'Александр Сергеевич Пушкин',
                createdBy: user._id,
                created: Date.now()
            }
        });
    }

    if (!performer) {
        console.log('seed: performer create');
        performer = await bookPerformerService.add({
            model: {
                fullName: 'Максим Галкин',
                createdBy: user._id,
                created: Date.now()
            }
        });
    }

    if (!fantasyCategory) {
        console.log('seed: fantasy category create');
        fantasyCategory = await bookService.add({
            model: {
                type: EntityType.FOLDER,
                parent: null,
                url: 'fantasy',
                title: 'Фантастика',
                description: 'Фантастика, фэнтези',
                createdBy: user._id,
                created: Date.now(),
                author: null,
                performer: null
            }
        });
    }

    if (!detectiveCategory) {
        console.log('seed: detective category create');
        detectiveCategory = await bookService.add({
            model: {
                type: EntityType.FOLDER,
                parent: null,
                url: 'detective',
                title: 'Детективы, триллеры',
                description: 'Детективы, боевики и триллеры',
                createdBy: user._id,
                created: Date.now(),
                author: null,
                performer: null
            }
        });
    }

    if (!romanCategory) {
        console.log('seed: roman category create');
        romanCategory = await bookService.add({
            model: {
                type: EntityType.FOLDER,
                parent: null,
                url: 'roman',
                title: 'Роман, проза',
                description: 'Аудиокниги-романы',
                createdBy: user._id,
                created: Date.now(),
                author: null,
                performer: null
            }
        });
    }

    let audio = await mediaService.one({query: {fileName: '/media/root/seed.mp3'}});
    let image = await mediaService.one({query: {fileName: '/media/root/seed.jpg'}});

    for (let i = 0; i < 100; i++) {
        let fBook = await bookService.one({
            query: {
                type: EntityType.BOOK,
                bookType: BookType.AUDIO,
                parent: fantasyCategory._id,
                url: `fant${i}`
            }
        });
        if (!fBook) {
            console.log('seed: create fantasy book ' + i);
            await bookService.add({
                model: {
                    type: EntityType.BOOK,
                    bookType: BookType.AUDIO,
                    parent: fantasyCategory._id,
                    url: `fant${i}`,
                    title: `Фантастическая книга ${i}`,
                    description: description,
                    createdBy: user._id,
                    created: Date.now(),
                    author: author._id,
                    performer: performer._id,
                    medias: [audio._id, audio._id, audio._id, audio._id, audio._id, audio._id, audio._id, audio._id, audio._id, audio._id],
                    cover: image._id,
                    tags: [`fant${i}`, `${i}fant`],
                    published: Math.round(Math.random())
                }
            });
        }
        let dBook = await bookService.one({
            query: {
                type: EntityType.BOOK,
                bookType: BookType.AUDIO,
                parent: detectiveCategory._id,
                url: `det${i}`
            }
        });
        if (!dBook) {
            console.log('seed: create detective book ' + i);
            await bookService.add({
                model: {
                    type: EntityType.BOOK,
                    bookType: BookType.AUDIO,
                    parent: detectiveCategory._id,
                    url: `det${i}`,
                    title: `Детективная книга ${i}`,
                    description: description,
                    createdBy: user._id,
                    created: Date.now(),
                    author: author._id,
                    performer: performer._id,
                    medias: [audio._id],
                    cover: image._id,
                    tags: [`det${i}`, `${i}det`],
                    published: Math.round(Math.random())
                }
            });
        }
        let rBook = await bookService.one({
            query: {
                type: EntityType.BOOK,
                bookType: BookType.AUDIO,
                parent: romanCategory._id,
                url: `roman${i}`
            }
        });
        if (!rBook) {
            console.log('seed: create roman book ' + i);
            await bookService.add({
                model: {
                    type: EntityType.BOOK,
                    bookType: BookType.AUDIO,
                    parent: romanCategory._id,
                    url: `roman${i}`,
                    title: `Романтическая книга ${i}`,
                    description: description,
                    createdBy: user._id,
                    created: Date.now(),
                    author: author._id,
                    performer: performer._id,
                    medias: [audio._id],
                    cover: image._id,
                    tags: [`roman${i}`, `${i}roman`],
                    published: Math.round(Math.random())
                }
            });
        }
    }
};