const dummyText = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
const mdDummyText = dummyText;

const Promise = require('bluebird');
const User = require('../models/User');
const Media = require('../models/Media');
const Entity = require('../models/Entity');
const EntityType = require('../models/EntityType');
const EntityComment = require('../models/EntityComment');
const EntityService = require('../services/entityService');
const ROLE = require('../../consts').ROLE;

module.exports = () => {
    return Promise.all([
        EntityComment.remove({}).exec(),
        Entity.remove({}).exec(),
        User.findOne({roles: ROLE.ADMIN}).exec()
    ]).then((results) => {
        let admin = results[2];
        if (!admin) {
            return Promise.reject('Admin user not found');
        } else {
            let folders = [
                {
                    type: EntityType.FOLDER,
                    title: 'Новости',
                    url: 'news',
                    createdBy: admin._id
                },
                {
                    type: EntityType.FOLDER,
                    title: 'Погода',
                    url: 'weather',
                    createdBy: admin._id
                },
                {
                    type: EntityType.FOLDER,
                    title: 'Книги',
                    url: 'books',
                    createdBy: admin._id
                }
            ];

            return Promise.reduce(folders, (total, folder) => {
                console.log(`Create entity category ${folder.url} - ${folder.title}`);
                return EntityService.add(folder).then((dbFolder) => {
                    total.push(dbFolder);
                    return Promise.resolve(total);
                });
            }, []).then(function (total) {
                return Promise.resolve({folders: total, createdBy: admin});
            });
        }
    }).then((data) => {
        let documents = [];
        data.folders.forEach((f) => {
            for (let i = 0; i < 100; i++) {
                documents.push({
                    type: EntityType.DOCUMENT,
                    parent: f._id,
                    createdBy: data.createdBy,
                    title: `${f.title}${i}`,
                    url: `${f.url}${i}`,
                    shortBody: dummyText,
                    body: mdDummyText
                });
            }
        });

        return Promise.reduce(documents, (total, doc) => {
            console.log(`Add document ${doc.url} - ${doc.title} (Doc type: ${doc.type})`);
            doc.published = Math.round(Math.random());
            doc.tags = [doc.title, doc.url];
            return EntityService.add(doc).then((dbDoc) => {
                total.push(dbDoc._id);
                return Promise.resolve(total);
            });
        }, []);
    }).then((documentsIds) => {
        return Media.findOne({mimeType: 'image/jpeg'}).exec().then((media) => {
            if (media) {
                return Promise.reduce(documentsIds, (total, id) => {
                    console.log(`Update media for entity id = ${id}`);
                    return Entity.update({_id: id}, {$set: {media: media._id}}).exec();
                }, []);
            } else {
                return Promise.resolve('dbseed done');
            }
        });
    });
};