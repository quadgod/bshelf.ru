const uuid = require('node-uuid');
const User = require('../../models/User');
const ROLE = require('../../../consts').ROLE;
const UserService = require('../../services/userService');

module.exports = async () => {
    let userService = new UserService();

    let user = await userService.one({
        query: {
            email: 'dbseed@test.test'
        }
    });

    if (!user) {
        console.log('seed: create user dbseed@test.test');
        let newUser = new User({
            nickname: `test_${uuid.v4()}`,
            password: 'password',
            email: 'dbseed@test.test',
            isDeleted: false,
            roles: [ROLE.ADMIN]
        });
        await newUser.save();
    } else {
        console.log('seed: found user dbseed@test.test');
    }
};