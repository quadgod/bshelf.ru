const uuid = require('node-uuid');
const fsHelper = require('../../helpers/fsHelper');
const EntityType = require('../../models/EntityType');
const MediaService = require('../../services/mediaService');
const UserService = require('../../services/userService');

module.exports = async () => {
    let userService = new UserService();
    let mediaService = new MediaService();
    let mp3Exists = await fsHelper.fileExists('./public/media/root/seed.mp3');
    let imageExists = await fsHelper.fileExists('./public/media/root/seed.jpg');

    if (!mp3Exists) {
        console.log('seed: copy seed.mp3');
        await fsHelper.ensureExists('./public/media', ['root']);
        await fsHelper.copyFile('./server/migrations/seed/seed.mp3', './public/media/root/seed.mp3');
    }

    if (!imageExists) {
        console.log('seed: copy seed.jpg');
        await fsHelper.ensureExists('./public/media/root', []);
        await fsHelper.copyFile('./server/migrations/seed/seed.jpg', './public/media/root/seed.jpg');
    }

    let seedAudio = await mediaService.one({query: {fileName: '/media/root/seed.mp3'}});
    let seedImage = await mediaService.one({query: {fileName: '/media/root/seed.jpg'}});

    if (seedAudio && seedImage) {
        console.log('seed: medias ok');
        return;
    }
    let user = await userService.one({query: {email: 'dbseed@test.test'}});
    if (!user) {
        console.error('seed: medias user not found');
        return;
    }

    if (!seedAudio) {
        let duration = await fsHelper.mp3Duration('./public/media/root/seed.mp3');
        let size = await fsHelper.fileSize('./public/media/root/seed.mp3');
        let model = {
            type: EntityType.MEDIA_FILE,
            createdBy: user._id,
            created: Date.now(),
            title: `seed_${uuid.v4()}.mp3`,
            fileName: `/media/root/seed.mp3`,
            mimeType: 'audio/mp3',
            size: size,
            duration: duration,
            parent: null
        };
        await mediaService.add({model: model});
        console.log('seed: mp3 created');
    }

    if (!seedImage) {
        let size = await fsHelper.fileSize('./public/media/root/seed.jpg');
        let model = {
            type: EntityType.MEDIA_FILE,
            createdBy: user._id,
            created: Date.now(),
            title: `seed_${uuid.v4()}.jpg`,
            fileName: `/media/root/seed.jpg`,
            mimeType: 'image/jpeg',
            size: size,
            duration: 0,
            parent: null
        };
        await mediaService.add({model: model});
        console.log('seed: jpg created');
    }
};