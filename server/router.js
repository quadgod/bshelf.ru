module.exports = (app) => {
    app.use('/api',     require('./controllers/api'));
    app.use('',         require('./controllers/static'));
};