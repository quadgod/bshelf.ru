const logger = require('./logger');
const VK = require('../config').vk;
const passport = require('passport');
const ERRORS = require('../consts').STRINGS.ERRORS;
const valueHelper = require('./helpers/valueHelper');


const LocalStrategy = require('passport-local').Strategy;
const VKStrategy = require('passport-vkontakte').Strategy;
const UserService = require('./services/userService');

const userService = new UserService();


passport.use(new VKStrategy({
    clientID: VK.clientId,
    clientSecret: VK.clientSecret,
    callbackURL: 'http://localhost:3000/account/vkontakte/callback'
}, (accessToken, refreshToken, requestParams, profile, done) => {
    if (!valueHelper.getValueAsStringOrNull(requestParams.email)) {
        done(null, false);
    } else {
        let params = {
            model: {
                email: valueHelper.getValueAsStringOrNull(requestParams.email),
                profile: profile
            }
        };
        userService
            .findOrRegisterVkontakte(params)
            .then(user => done(null, user))
            .catch(() => done(null, false));
    }
}));

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, (req, username, password, done) => {
    userService.one({
        query: {
            email: username,
            isDeleted: { $ne: true }
        },
        select: '_id salt hashedPassword'
    }).then(user => {
        if (!user || !user.validatePassword(password)) {
            if (!user) {
                logger.info(`${ERRORS.userNotFoundInvalidLogin} ${username}.`);
            } else {
                logger.info(`${ERRORS.invalidLogin} ${username}`);
            }
            done(null, false);
        } else {
            done(null, user);
        }
    }).catch(done);
}));

passport.serializeUser((user, done) => done(null, user._id));
passport.deserializeUser((id, done) => {
    userService.one({
        query: {
            _id: id,
            isDeleted: {$ne: true}
        },
        select: '-resetPasswordToken -activateAccountToken'
    }).then(user => {
        if (!user) {
            done(null, false);
        } else {
            done(null, userService.prepareForView(user));
        }
    }).catch(err => done(err));
});

module.exports = passport;