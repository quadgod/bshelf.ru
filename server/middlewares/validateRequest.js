const _ = require('lodash');
const logger = require('../logger');
const JoiValidate = require('express-validation');

class ValidateRequest {
    constructor(schema) {
        this.schema = schema;
    }

    invoke(req, res, next) {
        JoiValidate(this.schema)(req, res, (err) => {
            if (!_.isUndefined(err) && !_.isNull(err)) {
                logger.error('Request Error', {error: err});
                if (_.isObject(err) && err.hasOwnProperty('status') && err.hasOwnProperty('statusText')) {
                    res.boom.badRequest('Bad request', err.errors);
                } else {
                    res.boom.badImplementation();
                }
            } else {
                next();
            }
        });
    }

    static setup(schema) {
        let validate = new ValidateRequest(schema);
        return validate.invoke.bind(validate);
    }
}

module.exports = ValidateRequest;