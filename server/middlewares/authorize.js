const _ = require('lodash');

class Authorize {
    constructor(roles = [], api = false) {
        this.api = api;
        this.roles = roles;
    }

    invoke(req, res, next) {
        if (!req.user) {
            if (!this.api) {
                return res.redirect('/account/login');
            } else {
                return res.boom.unauthorized();
            }
        }

        let userRoles = req.user.roles || [];

        if (this.roles.length > 0 && _.intersection(userRoles, this.roles).length == 0) {
            if (!this.api) {
                return res.redirect('/account/login');
            } else {
                return res.boom.unauthorized();
            }
        }

        next();
    }

    static setup(roles = [], api = false) {
        let authorize = new Authorize(roles, api);
        return authorize.invoke.bind(authorize);
    }
}

module.exports = Authorize;