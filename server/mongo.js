const mongoose = require('mongoose');
const config = require('../config');
const Promise = require('bluebird');
const migrations = require('./migrations');
mongoose.Promise = Promise;

module.exports = () => {
    let connect = new Promise((resolve, reject) => {
        mongoose.connect(config.mongoUrl);
        mongoose.connection.on('connected', function () {
            console.log(`Mongoose default connection open to ${config.mongoUrl}`);
            resolve(mongoose);
        });
        mongoose.connection.on('error',function (err) {
            console.error(`Mongoose default connection error: ${err}`);
            reject(err);
        });
    });

    return connect.then(async () => {
        return await migrations(config.env);
    });
};