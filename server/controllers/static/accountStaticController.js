const logger = require('../../logger');
const passport = require('../../passport');
const ROLE = require('../../../consts').ROLE;
const BaseController = require('../baseController');
const viewHelper = require('../../helpers/viewHelper');
const authorize = require('../../middlewares/authorize');
const UserService = require('../../services/userService');

class AccountStaticController extends BaseController {
    constructor() {
        super(false);
        this.userService = new UserService();
    }

    get pageLogin() {
        return [
            (req, res) => {
                res.render('account/login', viewHelper.createModel('Логин', req));
            }
        ];
    }

    get pageRegistration() {
        return [
            (req, res) => {
                res.render('account/registration', viewHelper.createModel('Регистрация', req));
            }
        ];
    }

    get pageRegistrationComplete() {
        return [
            (req, res) => {
                res.render('account/registration-complete', viewHelper.createModel('Регистрация', req));
            }
        ];
    }

    get activateAccount() {
        return [
            (req, res) => {
                this.userService.activateAccount({
                    model: {
                        userid: req.query.userid,
                        token: req.query.token
                    }
                })
                .then(() => res.redirect('/account/registration/complete'))
                .catch(err => {
                    logger.error(`Activate account: Invalid request`, {
                        query: req.query,
                        ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
                        error: err
                    });
                    this.setupOnError(req, res)(err);
                });
            }
        ];
    }

    get logout() {
        return [
            (req, res) => {
                req.logout();

                if (req.query && req.query.returnUrl) {
                    res.redirect(req.query.returnUrl);
                } else {
                    res.redirect('/');
                }
            }
        ];
    }

    get pageRestorePassword() {
        return [
            (req, res) => {
                res.render('account/restore-password', viewHelper.createModel('Восстановление пароля', req));
            }
        ];
    }

    get pageResetPassword() {
        return [
            (req, res) => {
                let model = viewHelper.createModel('Сброс пароля', req);
                model.token = req.params.token;
                model.userid = req.params.userId;

                userService.findOne({
                    _id: model.userid,
                    isDeleted: {$ne: true},
                    'resetPasswordToken.token': model.token,
                    'resetPasswordToken.expire': { $gte: Date.now() }
                }).then(user => {
                    if (!user) {
                        logger.error('Error', {
                            error: {
                                token: model.token,
                                userId: model.userid,
                                ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress
                            }
                        });
                        res.render('account/reset-password-error', model);
                    } else {
                        res.render('account/reset-password', model);
                    }
                }).catch(err => {
                    logger.error(`Error`, { error: err });
                    res.render('account/reset-password-error', model);
                });
            }
        ];
    }

    get pageProfile() {
        return [
            authorize.setup([]),
            (req, res) => {
                this.userService
                    .one({
                        query: {_id: req.user._id},
                        select: `_id avatar roles email name nickname metadata.phone${req.user.isAdmin ? ' isDeleted' : ''}`
                    })
                    .then(user => {
                        req.user.metadata = req.user.metadata || {};
                        req.user.metadata.phone = user.metadata.phone;
                        let model = viewHelper.createModel('Профиль', req);
                        res.render('account/profile', model);
                    })
                    .catch(this.setupOnError(req, res));

            }
        ];
    }

    get pageFavorites() {
        return [
            authorize.setup([]),
            (req, res) => {
                let model = viewHelper.createModel('Избранное', req);
                res.render('account/favorites', model);
            }
        ]
    }

    get impersonate() {
        return [
            authorize.setup([ROLE.ADMIN]),
            (req, res) => {
                (async () => {
                    let user = await this.userService.one({query: {_id: req.params.id}});
                    if (!user) {
                        throw 404;
                    }
                    return user;
                })()
                .then(user => req.logIn(user, (err) => res.redirect('/')))
                .catch(this.setupOnError(req, res));
            }
        ]
    }

    get pageProfileView() {
        return [
            (req, res) => {

            }
        ]
    }

    get vkontakte() {
        return [
            passport.authenticate('vkontakte', {scope: ['email']}),
            (req, res) => {
                res.redirect('/');
            }
        ];
    }

    get vkontakteCallback() {
        return [
            passport.authenticate('vkontakte', { failureRedirect: '/account/login' }),
            (req, res) => {
                res.redirect('/');
            }
        ]
    }

    static setup(router) {
        let accountStaticController = new AccountStaticController();
        router.get('/account/login', accountStaticController.pageLogin);
        router.get('/account/logout', accountStaticController.logout);
        router.get('/account/registration/activate', accountStaticController.activateAccount);
        router.get('/account/registration/complete', accountStaticController.pageRegistrationComplete);
        router.get('/account/registration/', accountStaticController.pageRegistration);
        router.get('/account/restore-password/:userId/:token', accountStaticController.pageResetPassword);
        router.get('/account/restore-password', accountStaticController.pageRestorePassword);
        router.get('/account/profile', accountStaticController.pageProfile);
        router.get('/account/profile/:id', accountStaticController.pageProfileView);
        router.get('/account/profile/:id/impersonate', accountStaticController.impersonate);
        router.get('/account/favorites', accountStaticController.pageFavorites);
        router.get('/account/vkontakte', accountStaticController.vkontakte);
        router.get('/account/vkontakte/callback', accountStaticController.vkontakteCallback);
    }
}

module.exports = AccountStaticController;