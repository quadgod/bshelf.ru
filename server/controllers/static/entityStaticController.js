const Promise = require('bluebird');
const BaseController = require('../baseController');
const EntityType = require('../../models/EntityType');
const viewHelper = require('../../helpers/viewHelper');
const valueHelper = require('../../helpers/valueHelper');
const EntityService = require('../../services/entityService');

class EntityStaticController extends BaseController {
    constructor() {
        super(false);
        this.entityService = new EntityService();
    }

    buildQuery(req, category) {
        let params = {
            page: valueHelper.getValueAsIntOrDefault(req.query.page, 1, 1),
            limit: valueHelper.getValueAsIntOrDefault(req.query.limit, 10, 1, 10),
            query: {
                $and: [
                    {
                        type: {
                            $ne: EntityType.FOLDER
                        }
                    },
                    {
                        published: true
                    }
                ]
            }
        };

        let title = valueHelper.getValueAsStringOrNull(req.query.title);
        if (title) {
            params.query.$and.push({title: new RegExp(title, 'i')});
        }

        let tag = valueHelper.getValueAsStringOrNull(req.query.tag);
        if (tag) {
            params.query.$and.push({tags: tag});
        }

        if (category) {
            params.query.$and.push({parent: category._id});
        }

        return params;
    }
    buildModel(req, viewTitle) {
        let model = viewHelper.createModel(viewTitle, req);
        model.searchBar = {
            query: '',
            tags: []
        };

        let title = valueHelper.getValueAsStringOrNull(req.query.title);
        if (title) {
            model.searchBar.query = title;
        }

        let tag = valueHelper.getValueAsStringOrNull(req.query.tag);
        if (tag) {
            model.searchBar.tags.push(tag);
        }

        return model;
    }

    get pageItem() {
        let action = this.wrapAsync(async(req, res) => {
            let category = await this.entityService.one({
                query: {
                    parent: null,
                    url: req.params.url,
                    type: EntityType.FOLDER
                }
            });

            if (!category) throw 404;

            let params = {
                query: {
                    parent: category._id,
                    url: req.params.docUrl,
                    type: {$ne: EntityType.FOLDER},
                    published: true
                }
            };

            let [entity, categories] = await Promise.all([
                this.entityService.one(params),
                this.entityService.getCategories()
            ]);

            if (!entity) throw 404;

            let model = viewHelper.createModel(entity.title, req);
            model.entity = entity;
            model.categories = categories;
            model.categories.forEach(c => c.active = entity.parent._id.toString() == c._id.toString());
            res.render('entity/item', model);
        });

        return action;
    }
    get pageList() {
        let action = this.wrapAsync(async (req, res) => {

            let category = await this.entityService.one({query: {
                parent: null,
                url: req.params.url,
                type: EntityType.FOLDER
            }});

            if (!category) throw 404;

            let params = this.buildQuery(req, category);
            let model = this.buildModel(req, category.title);

            let [entities, categories] = await Promise.all([
                this.entityService.query(params),
                this.entityService.getCategories()
            ]);

            model.entities = entities;
            model.categories = categories;
            model.categories.forEach(c => c.active = c._id.toString() == category._id.toString());
            model.paginator = viewHelper.createPaginationModel(model.entities);
            res.render('entity/list', model);
        });

        return action;
    }

    static setup(router) {
        let entityStaticController = new EntityStaticController();
        router.get('/category/:url', entityStaticController.pageList);
        router.get('/category/:url/:docUrl', entityStaticController.pageItem);
    }
}

module.exports = EntityStaticController;