const express = require('express');
const bookStaticController = require('./bookStaticController');
const accountStaticController = require('./accountStaticController');
const indexStaticController = require('./indexStaticController');
const entityStaticController = require('./entityStaticController');
const dashboardStaticController = require('./dashboardStaticController');

const router = express.Router();

bookStaticController.setup(router);
accountStaticController.setup(router);
dashboardStaticController.setup(router);
entityStaticController.setup(router);
indexStaticController.setup(router);

module.exports = router;