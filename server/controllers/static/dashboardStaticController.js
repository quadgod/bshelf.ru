const ROLE = require('../../../consts').ROLE;
const BaseController = require('../baseController');
const viewHelper = require('../../helpers/viewHelper');
const authorize = require('../../middlewares/authorize');

class DashboardStaticController extends BaseController {
    constructor() {
        super(false);
    }

    get pageDashboard() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR]),
            (req, res) => {
                res.render('dashboard', viewHelper.createModel('Панель управления', req));
            }
        ];
    }

    static setup(router) {
        let dashboardStaticController = new DashboardStaticController();
        router.get('/dashboard', dashboardStaticController.pageDashboard);
    }
}

module.exports = DashboardStaticController;