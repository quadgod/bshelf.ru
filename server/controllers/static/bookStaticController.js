const _ = require('lodash');
const BookType = require('../../models/BookType');
const BaseController = require('../baseController');
const EntityType = require('../../models/EntityType');
const viewHelper = require('../../helpers/viewHelper');
const valueHelper = require('../../helpers/valueHelper');
const BookService = require('../../services/bookService');


class BookStaticController extends BaseController {
    constructor(modules = null) {
        super(false);

        if (!modules) this.__modules = require('../../../config').modules;
        this.__modules = this.__modules || {};
        this.bookService = new BookService();
    }

    buildQuery(req, bookType, category) {
        let params = {
            page: valueHelper.getValueAsIntOrDefault(req.query.page, 1, 1),
            limit: valueHelper.getValueAsIntOrDefault(req.query.limit, 10, 1, 10),
            query: {
                $and: [
                    {type: { $ne: EntityType.FOLDER }},
                    {bookType: bookType},
                    {published: true}
                ]
            }
        };

        let title = valueHelper.getValueAsStringOrNull(req.query.search);
        if (title) {
            params.query.$and.push({
                $or: [
                    {title: {$regex: new RegExp(title, 'i')}},
                    {description: {$regex: new RegExp(title, 'i')}}
                ]
            });
        }

        let tag = valueHelper.getValueAsStringOrNull(req.query.tag);
        if (tag) {
            params.query.$and.push({tags: tag});
        }

        if (category) {
            params.query.$and.push({
                $or: [
                    {parent: category._id},
                    {seriesParent: category._id}
                ]
            });
        }

        return params;
    }
    buildModel(req, viewTitle) {
        let model = viewHelper.createModel(viewTitle, req);
        model.searchBar = {
            query: '',
            tags: []
        };

        let title = valueHelper.getValueAsStringOrNull(req.query.title);
        if (title) {
            model.searchBar.query = title;
        }

        let tag = valueHelper.getValueAsStringOrNull(req.query.tag);
        if (tag) {
            model.searchBar.tags.push(tag);
        }

        return model;
    }

    get pageLibrary() {
        return [
            (req, res) => {
                let model = viewHelper.createModel('Библиотека', req);
                res.render('library', model);
            }
        ];
    }

    get pageAudiobooksList() {
        return this.wrapAsync(async (req, res) => {

            let params = this.buildQuery(req, BookType.AUDIO);
            let [categories, books] = await Promise.all([
                this.bookService.getCategories(BookType.AUDIO),
                this.bookService.query(params)
            ]);

            let model = this.buildModel(req, 'Аудиокниги');
            model.books = books;
            model.categories = _.isArray(categories) ? categories : categories.docs;
            model.categories.forEach(c => c.active = false);
            model.paginator = viewHelper.createPaginationModel(model.books);

            res.render('book/list', model);
        });
    }
    get pageAudiobooksCategoryList() {
        return this.wrapAsync(async (req, res) => {
            let category = await this.bookService.one({
                query: {
                    parent: null,
                    url: req.params.url,
                    type: EntityType.FOLDER
                }
            });

            if (!category) throw 404;

            let params = this.buildQuery(req, BookType.AUDIO, category);

            let [categories, books] = await Promise.all([
                this.bookService.getCategories(BookType.AUDIO),
                this.bookService.query(params)
            ]);

            let model = this.buildModel(req, category.title);
            model.books = books;
            model.categories = _.isArray(categories) ? categories : categories.docs;
            model.categories.forEach(c => c.active = c._id.toString() == category._id.toString());
            model.paginator = viewHelper.createPaginationModel(model.books);

            res.render('book/list', model);
        });
    }
    get pageAudiobookItem() {
        return this.wrapAsync(async(req, res) => {
            let category = await this.bookService.one({
                query: {
                    parent: null,
                    url: req.params.url,
                    type: EntityType.FOLDER
                }
            });

            if (!category) throw 404;

            let params = {
                query: {
                    parent: category._id,
                    url: req.params.docUrl,
                    type: {$ne: EntityType.FOLDER},
                    bookType: BookType.AUDIO,
                    published: true
                }
            };

            let [book, categories] = await Promise.all([
                this.bookService.one(params),
                this.bookService.getCategories(BookType.AUDIO)
            ]);

            if (!book) throw 404;

            let model = viewHelper.createModel(book.title, req);
            model.book = book;
            model.categories = categories.docs || categories;
            model.categories.forEach(c => {
                c.active = book.parent._id.toString() == c._id.toString()
            });
            res.render('book/item', model);
        });
    }
    get pageAudiobookPerformers() {
        return this.wrapAsync(async (req, res) => {
            let model = viewHelper.createModel('Исполнители', req);
            res.render('performers', model);
        });
    }
    get pageAuthors() {
        return this.wrapAsync(async (req, res) => {
            let model = viewHelper.createModel('Авторы', req);
            res.render('authors', model);
        });
    }




    static setup(router) {
        let bookStaticController = new BookStaticController();

        if (bookStaticController.__modules.library == true) {
            router.get('/library', bookStaticController.pageLibrary);
        }

        if (bookStaticController.__modules.audiobooks == true) {
            router.get('/audiobooks', bookStaticController.pageAudiobooksList);
            router.get('/audiobooks/:url', bookStaticController.pageAudiobooksCategoryList);
            router.get('/audiobooks/:url/:docUrl', bookStaticController.pageAudiobookItem);
            router.get('/performers', bookStaticController.pageAudiobookPerformers);
        }

        if (bookStaticController.__modules.audiobooks == true || bookStaticController.__modules.library == true) {
            router.get('/authors', bookStaticController.pageAuthors);
        }
    }
}

module.exports = BookStaticController;