const Promise = require('bluebird');
const viewHelper = require('../../helpers/viewHelper');
const EntityStaticController = require('./entityStaticController');

class IndexStaticController extends EntityStaticController {
    constructor() {
        super();
    }

    get pageList() {
        let action = this.wrapAsync(async (req, res) => {
            let params = this.buildQuery(req);
            let model = this.buildModel(req, 'Аудиокниги');

            let [entities, categories] = await Promise.all([
                this.entityService.query(params),
                this.entityService.getCategories()
            ]);

            model.entities = entities;
            model.categories = categories;
            model.categories.forEach(c => c.active = false);
            model.paginator = viewHelper.createPaginationModel(model.entities);
            res.render('entity/list', model);
        });
        return action;
    }

    static setup(router) {
        let indexStaticController = new IndexStaticController();
        router.get('/', indexStaticController.pageList);
    }
}

module.exports = IndexStaticController;