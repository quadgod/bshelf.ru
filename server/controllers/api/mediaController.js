const Joi = require('joi');
const _ = require('lodash');
const path = require('path');
const uuid = require('node-uuid');
const Promise = require('bluebird');
const multiparty = require('multiparty');
const ROLE = require('../../../consts').ROLE;
const fsHelper = require('../../helpers/fsHelper');
const BaseController = require('../baseController');
const EntityType = require('../../models/EntityType');
const authorize = require('../../middlewares/authorize');
const ERRORS = require('../../../consts').STRINGS.ERRORS;
const MediaService = require('../../services/mediaService');
const validate = require('../../middlewares/validateRequest');

class MediaController extends BaseController {
    constructor() {
        super(true);
        this.mediaService = new MediaService();
    }

    get upload() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                params: {
                    id: this.VALIDATION_TEMPLATES.stringRequired
                }
            }),
            (req, res) => {
                let form = new multiparty.Form();
                let parent = req.params.id == 'root' ? null : req.params.id;
                let formData = {};
                let buffer = [];
                let lengthBuffer = [];

                form.on('error', () => {
                    return res.boom.unsupportedMediaType(ERRORS.unsupportedFileTypeError);
                });
                form.on('field', (name, value) => {
                    formData[name] = value;
                });
                form.on('part', (part) => {
                    let mimeType = part.headers['content-type'] && part.headers['content-type'].toLowerCase();
                    if (!mimeType || !part.filename) {
                        form.emit('error', ERRORS.unsupportedFileTypeError);
                        return;
                    }

                    if (mimeType == 'application/octet-stream') {
                        if (path.extname(part.filename).toString().toLowerCase() == '.fb2') {
                            mimeType = 'fb2';
                        }
                    }

                    let allowedMimeTypes = [
                        'image/jpeg',
                        'image/gif',
                        'image/png',
                        'image/bmp',
                        'application/epub+zip',
                        'application/epub',
                        'text/plain',
                        'application/pdf',
                        'fb2'
                    ];

                    if (_.intersection(allowedMimeTypes, [mimeType]).length == 0 &&
                        !_.startsWith(mimeType, 'video/') &&
                        !_.startsWith(mimeType, 'audio/')) {
                        form.emit('error', ERRORS.unsupportedFileTypeError);
                        return;
                    }

                    if (mimeType) {
                        formData.filename = part.filename;
                        formData.mimeType = mimeType;
                        formData.size = form.bytesExpected;
                    }

                    part.on('data', (chunk) => {
                        buffer.push(chunk);
                    });

                    part.on('end', () => {
                        lengthBuffer = part.byteCount;
                    });
                });
                form.on('close', () => {
                    (async () => {
                        let currentDate = new Date();
                        let realFileName = `${uuid.v4()}${path.extname(formData.filename)}`;

                        if (!_.isNull(parent) && !(await this.mediaService.one({query: {_id: parent}}))) {
                            throw 400;
                        }

                        let parentFolder = parent ? parent.toString() : 'root';
                        await fsHelper.ensureExists('./public/media', [parentFolder]);

                        let relativePath = `./public/media/${parentFolder}/${realFileName}`;
                        let bytes = Buffer.concat(buffer, lengthBuffer);
                        await fsHelper.writeFile(relativePath, bytes);

                        let title = await this.mediaService.genFileTitle(parent, formData.filename);
                        let duration = formData.mimeType == 'audio/mp3' ? await fsHelper.mp3Duration(relativePath) : 0;
                        let model = {
                            type: EntityType.MEDIA_FILE,
                            createdBy: req.user._id,
                            created: currentDate,
                            title: title,
                            fileName: `/media/${parentFolder}/${realFileName}`,
                            mimeType: formData.mimeType,
                            size: formData.size,
                            duration: duration,
                            parent: parent
                        };

                        return this.mediaService.add({model: model});
                    })()
                    .then(media => res.json(media))
                    .catch(this.setupOnError(req, res));
                });

                form.parse(req);
            }
        ];
    }
    get add() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                body: {
                    parent: this.VALIDATION_TEMPLATES.stringRequired
                }
            }),
            (req, res) => {
                let media = req.body;

                media.createdBy = req.user._id;
                media.created = Date.now();

                if (media.mimeType == 'youtube') {
                    let regexp = /^.*(?:(?:youtu\.be\/|v\/|vi\/|u\/\w\/|embed\/)|(?:(?:watch)?\?v(?:i)?=|\&v(?:i)?=))([^#\&\?]*).*/;
                    let videoId = media.title.toString().match(regexp);
                    if (videoId.length == 0 || _.isEmpty(videoId[1])) {
                        return res.boom.badRequest('Bad youtube link');
                    } else {
                        media.fileName = videoId[1];
                    }
                }

                if (media.parent == 'root') {
                    media.parent = null;
                }

                this.mediaService
                    .add({model: media})
                    .then(newMedia => res.json(newMedia))
                    .catch(this.setupOnError(req, res));
            }
        ];
    }
    get getBreadcrumbs() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            (req, res) => {
                this.mediaService
                    .getParents(req.query.parent)
                    .then(breadcrumbs => res.json(breadcrumbs))
                    .catch(this.setupOnError(req, res));
            }
        ];
    }
    get getPaged() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                query: {
                    page: this.VALIDATION_TEMPLATES.page,
                    limit: this.VALIDATION_TEMPLATES.limit,
                    sort: this.VALIDATION_TEMPLATES.stringDefaultNull,
                    title: this.VALIDATION_TEMPLATES.stringDefaultNull,
                    parent: this.VALIDATION_TEMPLATES.stringRequired,
                    mimeTypes: Joi.array().unique().single().default(null)
                }
            }),
            (req, res) => {
                let params = {
                    page: req.query.page,
                    limit: req.query.limit,
                    query: {
                        $and: [
                            {parent: req.query.parent == 'root' ? null : req.query.parent}
                        ]
                    }
                };

                if (req.query.sort) {
                    params.sort = req.query.sort;
                }

                if (req.query.title) {
                    params.query.$and.push({
                        title: new RegExp(req.query.title, 'i')
                    });
                }

                if (req.query.mimeTypes) {
                    params.query.$and.push({
                        $or: [
                            { type: EntityType.FOLDER },
                            {
                                mimeType: {
                                    $in: req.query.mimeTypes
                                }
                            }
                        ]
                    });
                }

                this.mediaService
                    .query(params)
                    .then(medias => res.json(medias))
                    .catch(this.setupOnError(req, res));
            }
        ];
    }
    get delete() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                params: {
                    id: this.VALIDATION_TEMPLATES.stringRequired
                }
            }),
            (req, res) => {
                this.mediaService
                    .delete(req.params.id)
                    .then(() => res.json({removed: req.params.id}))
                    .catch(this.setupOnError(req, res));
            }
        ]
    }
    get getAll() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                params: {
                    id: this.VALIDATION_TEMPLATES.stringRequired
                },
                query: {
                    sort: this.VALIDATION_TEMPLATES.stringRequired,
                    mimeTypes: Joi.array().min(1).single().unique()
                }
            }),
            this.wrapAsync(async (req, res) => {
                let params = {
                    query: {
                        $and: [
                            {parent: req.params.id == 'root' ? null : req.params.id}
                        ]
                    }
                };

                if (req.query.sort) {
                    params.sort = req.query.sort;
                }

                if (req.query.mimeTypes) {
                    params.query.$and.push({
                        mimeType: {
                            $in: req.query.mimeTypes
                        }
                    });
                }

                let medias = await this.mediaService.query(params);
                res.json(medias);
            })
        ]
    }

    static setup(router) {
        let ctrl = new MediaController();

        router.get('/media/breadcrumbs',            ctrl.getBreadcrumbs);
        router.get('/media/:id/all',                ctrl.getAll);
        router.post('/media/:id/upload',            ctrl.upload);
        router.post('/media',                       ctrl.add);
        router.delete('/media/:id',                 ctrl.delete);
        router.get('/media',                        ctrl.getPaged);
    }
}

module.exports = MediaController;