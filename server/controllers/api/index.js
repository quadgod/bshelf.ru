const express = require('express');

const bookController = require('./bookController');
const userController = require('./userController');
const mediaController = require('./mediaController');
const entityController = require('./entityController');
const activityController = require('./activityController');

const router = express.Router();

activityController.setup(router);
userController.setup(router);
mediaController.setup(router);
entityController.setup(router);
bookController.setup(router);

module.exports = router;

