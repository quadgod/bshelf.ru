const ROLE = require('../../../consts').ROLE;
const BookType = require('../../models/BookType');
const BaseController = require('../baseController');
const EntityType = require('../../models/EntityType');
const authorize = require('../../middlewares/authorize');
const valueHelper = require('../../helpers/valueHelper');
const BookService = require('../../services/bookService');
const validate = require('../../middlewares/validateRequest');
const bookAuthorController = require('./bookAuthorController');
const bookPerformerController = require('./bookPerformerController');

class BookController extends BaseController {
    constructor() {
        super(true);
        this.bookService = new BookService();
    }

    get add() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                body: {
                    parent: this.VALIDATION_TEMPLATES.stringRequired
                }
            }),
            (req, res) => {
                let model = req.body;
                model.createdBy = req.user._id;
                model.created = Date.now();
                model.parent = model.parent == 'root' ? null : model.parent;
                this.bookService
                    .add({model: model})
                    .then(book => res.json(book))
                    .catch(this.setupOnError(req, res));
            }
        ];
    }
    get getPaged() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                query: {
                    page: this.VALIDATION_TEMPLATES.page,
                    limit: this.VALIDATION_TEMPLATES.limit,
                    parent: this.VALIDATION_TEMPLATES.stringRequired,
                    title: this.VALIDATION_TEMPLATES.stringDefaultNull
                }
            }),
            (req, res) => {
                let params = {
                    page: req.query.page,
                    limit: req.query.limit,
                    query: {
                        $and: [
                            {
                                parent: req.query.parent == 'root' ? null : req.query.parent
                            }
                        ]
                    }
                };

                if (req.query.title) {
                    params.query.$and.push({title: new RegExp(req.query.title, 'i')});
                }

                this.bookService
                    .query(params)
                    .then(books => res.json(books))
                    .catch(this.setupOnError(req, res));
            }
        ];
    }
    get getById() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                params: {
                    id: this.VALIDATION_TEMPLATES.stringRequired
                }
            }),
            (req, res) => {
                this.bookService
                    .one({query: {_id: req.params.id}})
                    .then(book => res.json(book))
                    .catch(this.setupOnError(req, res));
            }
        ];
    }
    get update() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                params: {
                    id: this.VALIDATION_TEMPLATES.stringRequired
                }
            }),
            (req, res) => {
                let model = req.body;
                if (model.hasOwnProperty('updateCreatedDate') && model.updateCreatedDate == true) {
                    model.created = Date.now();
                }
                delete model.updateCreatedDate;
                model.updatedBy = req.user._id;
                model.updated = Date.now();

                let params = {
                    query: {
                        _id: req.params.id
                    },
                    model: model
                };

                this.bookService
                    .update(params)
                    .then(book => res.json(book))
                    .catch(this.setupOnError(req, res));
            }
        ];
    }
    get getBreadcrumbs() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                query: {
                    parent: this.VALIDATION_TEMPLATES.stringRequired
                }
            }),
            (req, res) => {
                let parent = req.query.parent == 'root' ? null : req.query.parent;

                this.bookService
                    .getParents(parent)
                    .then(breadcrumbs => res.json(breadcrumbs))
                    .catch(this.setupOnError(req, res));
            }
        ];
    }
    get delete() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                params: {
                    id: this.VALIDATION_TEMPLATES.stringRequired
                }
            }),
            (req, res) => {
                this.bookService
                    .delete(req.params.id)
                    .then(removed => res.json({removed: removed}))
                    .catch(this.setupOnError(req, res));
            }
        ];
    }

    get bookPlaylist() {
        let validation = validate.setup({params: {id: this.VALIDATION_TEMPLATES.stringRequired}});
        let handler = this.wrapAsync(async (req, res) => {
            let book = await this.bookService.one({
                query: {
                    _id: req.params.id,
                    published: true,
                    mediasUnpublished: {$ne: true},
                    type: {
                        $ne: EntityType.FOLDER
                    },
                    bookType: BookType.AUDIO
                }
            });

            if (!book) throw 404;
            let playlist = book.medias.map(m => {
                return {
                    url: m.fileName,
                    title: m.title,
                    mimeType: m.mimeType,
                    duration: m.duration
                }
            });
            res.json(playlist);
        });
        return [validation, handler];
    }

    static setup(router) {
        let bookController = new BookController();
        router.get(     '/book',                     bookController.getPaged);
        router.post(    '/book',                     bookController.add);
        router.get(     '/book/breadcrumbs',         bookController.getBreadcrumbs);
        bookAuthorController.setup(router);
        bookPerformerController.setup(router);
        router.get(     '/book/:id/playlist',        bookController.bookPlaylist);
        router.get(     '/book/:id',                 bookController.getById);
        router.put(     '/book/:id',                 bookController.update);
        router.delete(  '/book/:id',                 bookController.delete);
    }
}

module.exports = BookController;