const ROLE = require('../../../consts').ROLE;
const BaseController = require('../baseController');
const authorize = require('../../middlewares/authorize');
const validate = require('../../middlewares/validateRequest');
const BookPerformerService = require('../../services/bookPerformerService');

class BookPerformerController extends BaseController {
    constructor() {
        super(true);
        this.bookPerformerService = new BookPerformerService();
    }

    get add() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                body: {
                    fullName: this.VALIDATION_TEMPLATES.fullName
                }
            }),
            (req, res) => {
                let model = req.body;
                model.createdBy = req.user._id;
                model.created = Date.now();

                this.bookPerformerService
                    .add({model: model})
                    .then(performer => res.json(performer))
                    .catch(this.setupOnError(req, res));
            }
        ];
    }
    get getPaged() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                query: {
                    page: this.VALIDATION_TEMPLATES.page,
                    limit: this.VALIDATION_TEMPLATES.limit,
                    fullName: this.VALIDATION_TEMPLATES.stringDefaultNull
                }
            }),
            (req, res) => {
                let params = {
                    page: req.query.page,
                    limit: req.query.limit,
                    query: {}
                };
                if (req.query.fullName) {
                    params.query.fullName = new RegExp(req.query.fullName, 'i');
                }

                this.bookPerformerService
                    .query(params)
                    .then(performers => res.json(performers))
                    .catch(this.setupOnError(req, res));
            }
        ];
    }
    get getById() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                params: {
                    id: this.VALIDATION_TEMPLATES.stringRequired
                }
            }),
            (req, res) => {
                this.bookPerformerService
                    .one({query: {_id: req.params.id}})
                    .then(performer => res.json(performer))
                    .catch(this.setupOnError(req, res));
            }
        ];
    }
    get update() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                params: {
                    id: this.VALIDATION_TEMPLATES.stringRequired
                },
                body: {
                    _id: this.VALIDATION_TEMPLATES.stringRequired,
                    fullName: this.VALIDATION_TEMPLATES.fullName
                }
            }),
            (req, res) => {
                let model = req.body;
                model.updatedBy = req.user._id;
                model.updated = Date.now();

                let params = {
                    query: {
                        _id: req.params.id
                    },
                    model: model
                };

                this.bookPerformerService
                    .update(params)
                    .then(performer => res.json(performer))
                    .catch(this.setupOnError(req, res));
            }
        ];
    }
    get delete() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                params: {
                    id: this.VALIDATION_TEMPLATES.stringRequired
                }
            }),
            (req, res) => {
                this.bookPerformerService
                    .delete(req.params.id)
                    .then(removed => res.json({removed: removed}))
                    .catch(this.setupOnError(req, res));
            }
        ];
    }

    static setup(router) {

        let bookPerformerController = new BookPerformerController();

        router.get(     '/book/performer',           bookPerformerController.getPaged);
        router.post(    '/book/performer',           bookPerformerController.add);
        router.get(     '/book/performer/:id',       bookPerformerController.getById);
        router.put(     '/book/performer/:id',       bookPerformerController.update);
        router.delete(  '/book/performer/:id',       bookPerformerController.delete);
    }
}

module.exports = BookPerformerController;