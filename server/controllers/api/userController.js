const Joi = require('joi');
const _ = require('lodash');
const path = require('path');
const sharp = require('sharp');
const uuid = require('node-uuid');
const Promise = require('bluebird');
const passport = require('passport');
const logger = require('../../logger');
const multiparty = require('multiparty');
const ROLE = require('../../../consts').ROLE;
const fsHelper = require('../../helpers/fsHelper');
const BaseController = require('../baseController');
const mailHelper = require('../../helpers/mailHelper');
const ERRORS = require('../../../consts').STRINGS.ERRORS;
const valueHelper = require('../../helpers/valueHelper');
const authorize = require('../../middlewares/authorize');
const UserService = require('../../services/userService');
const validate = require('../../middlewares/validateRequest');

class UserController extends BaseController {
    constructor() {
        super(true);
        this.userService = new UserService();
    }

    __genSelectFields(isAdmin) {
        return `_id avatar roles email name nickname metadata.phone${isAdmin ? ' isDeleted' : ''}`;
    }
    __genQuery(id, isAdmin) {
        return isAdmin ? {_id: id} : {_id: id, isDeleted: {$ne: true}};
    }

    get getPaged() {
        return [
            authorize.setup([ROLE.ADMIN], this.__api),
            validate.setup({
                query: {
                    page: this.VALIDATION_TEMPLATES.page,
                    limit: this.VALIDATION_TEMPLATES.limit,
                    nickname: this.VALIDATION_TEMPLATES.stringDefaultNull
                }
            }),
            (req, res) => {
                let params = {
                    page: req.query.page,
                    limit: req.query.limit,
                    query: {}
                };

                if (req.query.nickname) {
                    params.query.nickname = new RegExp(req.query.nickname, 'i');
                }

                this.userService
                    .query(params)
                    .then(users => res.json(users))
                    .catch(this.setupOnError(req, res));
            }
        ];
    }
    get getById() {
        return [
            authorize.setup([], this.__api),
            validate.setup({
                params: {
                    id: this.VALIDATION_TEMPLATES.stringRequired
                }
            }),
            (req, res) => {
                if (!req.user.isAdmin && req.user._id.toString() != req.params.id.toString()) {
                    return res.boom.notFound();
                }

                this.userService
                    .one({
                        query: this.__genQuery(req.params.id, req.user.isAdmin),
                        select: this.__genSelectFields(req.user.isAdmin)
                    })
                    .then(user => user ? res.json(user) : res.boom.notFound())
                    .catch(this.setupOnError(req, res));
            }
        ];
    }
    get current() {
        return [
            authorize.setup([], this.__api),
            (req, res) => {
                this.userService
                    .one({
                        query: this.__genQuery(req.user._id, req.user.isAdmin),
                        select: this.__genSelectFields(req.user.isAdmin)
                    })
                    .then(user => user ? res.json(this.userService.prepareForView(user)) : res.boom.notFound())
                    .catch(this.setupOnError(req, res));
            }
        ];
    }
    get update() {
        return [
            authorize.setup([], this.__api),
            validate.setup({
                params: {
                    id: this.VALIDATION_TEMPLATES.stringRequired
                },
                body: {
                    _id: this.VALIDATION_TEMPLATES.stringRequired,
                    nickname: this.VALIDATION_TEMPLATES.nickname,
                    name: Joi.object().keys({
                        first: this.VALIDATION_TEMPLATES.max50trucate,
                        last: this.VALIDATION_TEMPLATES.max50trucate,
                        middle: this.VALIDATION_TEMPLATES.max50trucate
                    }),
                    metadata: Joi.object().keys({
                        phone: this.VALIDATION_TEMPLATES.max50trucate
                    })
                }
            }),
            (req, res) => {
                let model = req.body;
                if (req.user._id.toString() != model._id.toString() && !req.user.isAdmin) {
                    return res.boom.forbidden();
                }

                let fieldsToUpdate = [
                    '_id',
                    'name',
                    'metadata',
                    'nickname'
                ];

                if (req.user.isAdmin) {
                    fieldsToUpdate = fieldsToUpdate.concat([
                        'roles',
                        'isDeleted'
                    ]);
                }

                model = _.pick(model, fieldsToUpdate);
                model.updated = Date.now();


                let params = {
                    query: {
                        _id: model._id
                    },
                    model: model
                };

                if (!req.user.isAdmin) {
                    params.query.isDeleted = {$ne: true};
                }

                this.userService
                    .update(params)
                    .then(user => res.json(user))
                    .catch(this.setupOnError(req, res));
            }
        ]
    }
    get resetPassword() {
        return [
            authorize.setup([], this.__api),
            validate.setup({
                params: {
                    id: this.VALIDATION_TEMPLATES.stringRequired
                },
                body: {
                    password: this.VALIDATION_TEMPLATES.password
                }
            }),
            (req, res) => {
                let params = {
                    query: {
                        _id: req.params.id
                    },
                    model: {
                        _id: req.params.id,
                        password: req.body.password
                    }
                };

                if (!req.user.isAdmin) {
                    params.model.oldPassword = valueHelper.getValueAsStringOrNull(req.body.oldPassword);
                    params.query.isDeleted = {$ne: true};
                }

                this.userService
                    .resetPassword(params)
                    .then(user => res.json(user))
                    .catch(this.setupOnError(req, res));
            }
        ];
    }
    get register() {
        return [
            validate.setup({
                body: {
                    password: this.VALIDATION_TEMPLATES.password,
                    nickname: this.VALIDATION_TEMPLATES.nickname,
                    email: Joi.string().email().required()
                }
            }),
            (req, res) => {
                let model = _.pick(req.body, ['email', 'nickname', 'password']);

                this.userService
                    .register({model: model})
                    .then(user => {
                        return mailHelper.sendRegistrationEmail(user, req).then(() => {
                            return Promise.resolve(user);
                        });
                    })
                    .then(user => res.json(user))
                    .catch(this.setupOnError(req, res));
            }
        ]

    }
    get login() {
        return [
            (req, res, next) => {
                passport.authenticate('local', (err, user) => {
                    if (err) {
                        return res.boom.badImplementation();
                    }

                    if (!user) {
                        return res.boom.badRequest();
                    }

                    req.logIn(user, function(err) {
                        if (err) { return res.boom.badImplementation(); }
                        return res.json(req.user);
                    });
                })(req, res, next);
            }
        ]
    }
    get restorePassword() {
        return [
            validate.setup({
                body: {
                    email: Joi.string().email().required()
                }
            }),
            (req, res) => {
                this.userService
                    .one({query: {email: req.body.email, isDeleted: {$ne: true}}})
                    .then(user => {
                        if (!user) {
                            return Promise.reject(404);
                        } else {
                            user.resetPasswordToken = user.resetPasswordToken || {};

                            let currentDate = new Date();
                            user.resetPasswordToken.token = uuid.v4();
                            user.resetPasswordToken.expire = new Date(currentDate.setTime(currentDate.getTime() + 3 * 86400000));
                            return mailHelper.sendRestorePasswordEmail(user, req).then(() => {
                                return user.save();
                            });
                        }
                    })
                    .then(() => {
                        return res.json({sent: true});
                    })
                    .catch(this.setupOnError(req, res));
            }
        ];
    }
    get resetPasswordByToken() {
        return [
            validate.setup({
                body: {
                    userId: this.VALIDATION_TEMPLATES.stringRequired,
                    token: this.VALIDATION_TEMPLATES.stringRequired,
                    password: this.VALIDATION_TEMPLATES.password
                }
            }),
            (req, res) => {
                logger.info('Check reset password token', {
                    token: req.body.token,
                    userId: req.body.userId,
                    ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress
                });
                this.userService.one({
                    query: {
                        _id: req.body.userId,
                        isDeleted: {$ne: true},
                        'resetPasswordToken.token': req.body.token,
                        'resetPasswordToken.expire': {$gte: Date.now()}
                    }
                }).then(user => {
                    if (!user) {
                        return Promise.reject(400);
                    } else {
                        user.password = req.body.password;
                        user.resetPasswordToken.token = null;
                        return user.save();
                    }
                }).then(() => {
                    return res.json({done: true});
                }).catch(this.setupOnError(req, res));
            }
        ]
    }
    get deleteAvatar() {
        return [
            authorize.setup([], this.__api),
            validate.setup({
                params: {
                    id: this.VALIDATION_TEMPLATES.stringRequired
                },
            }),
            (req, res) => {
                if (!req.user.isAdmin && req.params.id != req.user._id) {
                    return res.boom.notFound();
                }

                this.userService.one({query: {_id: req.params.id}}).then((user) => {
                    if (!user) {
                        return Promise.reject(404);
                    } else {
                        if (!_.isEmpty(user.avatar)) {
                            return fsHelper.unlink(`./public${user.avatar}`).then(() => {
                                user.avatar = null;
                                return user.save();
                            });
                        } else {
                            return Promise.resolve(user);
                        }
                    }
                })
                .then(user => res.json(user))
                .catch(this.setupOnError(req, res));
            }
        ]
    }
    get uploadAvatar() {
        return [
            authorize.setup([], this.__api),
            (req, res) => {
                let form = new multiparty.Form();
                let formData = {};
                let buffer = [];
                let lengthBuffer = [];
                form.on('error', (err) => {
                    return res.boom.unsupportedMediaType(err.message || ERRORS.unsupportedAvatarFormat, err);
                });
                form.on('field', (name, value) => {
                    formData[name] = value;
                });
                form.on('part', (part) => {
                    let mimeType = part.headers['content-type'] && part.headers['content-type'].toLowerCase();
                    if (!mimeType || !part.filename) {
                        form.emit('error', ERRORS.unsupportedAvatarFormat);
                        return;
                    }
                    if (_.intersection(['image/jpeg', 'image/png'], [mimeType]).length == 0) {
                        form.emit('error', ERRORS.unsupportedAvatarFormat);
                        return;
                    }
                    if (mimeType) {
                        if (form.bytesExpected > 1024 * 1024) {
                            form.emit('error', new Error(ERRORS.maxAvatarFileSize5mb));
                            return;
                        }

                        formData.avatarFileName = part.filename;
                        formData.avatarMimeType = mimeType;
                        formData.avatarSize = form.bytesExpected;
                    }
                    part.on('data', (chunk) => {
                        buffer.push(chunk);
                    });
                    part.on('end', () => {
                        lengthBuffer = part.byteCount;
                    });
                });
                form.on('close', () => {
                    if (!req.user.isAdmin && req.user._id.toString() != formData._id.toString()) {
                        return res.boom.forbidden();
                    }

                    fsHelper.ensureExists('./public/avatars', []).then(() => {
                        return this.userService.one({query: {_id: formData._id}});
                    }).then((user) => {
                        if (!user) {
                            return Promise.reject(404);
                        } else {
                            return Promise.resolve(user);
                        }
                    }).then((user) => {
                        let avatar = valueHelper.getValueAsStringOrNull(user.avatar);
                        if (avatar) {
                            return fsHelper.unlink(`./public${user.avatar}`).then(() => {
                                return Promise.resolve(user);
                            });
                        } else {
                            return Promise.resolve(user);
                        }
                    }).then((user) => {
                        if (formData.avatarFileName) {
                            let avatarName = uuid.v4();
                            let ext = path.extname(formData.avatarFileName);
                            return sharp(Buffer.concat(buffer, lengthBuffer))
                                .resize(300)
                                .toBuffer()
                                .then(data => {
                                    return fsHelper.writeFile(`./public/avatars/${avatarName}${ext}`, data).then(() => {
                                        user.avatar = `/avatars/${avatarName}${ext}`;
                                        return user.save();
                                    });
                                });
                        } else {
                            return Promise.resolve(user);
                        }
                    }).then((user) => {
                        return res.json(user);
                    }).catch(this.setupOnError(req, res));
                });
                form.parse(req);
            }
        ]
    }

    static setup(router) {
        let ctrl = new UserController();

        router.post(    '/user/registration',       ctrl.register);
        router.post(    '/user/login',              ctrl.login);
        router.post(    '/user/restore-password',   ctrl.restorePassword);
        router.post(    '/user/reset-password',     ctrl.resetPasswordByToken);
        router.get(     '/user/current',            ctrl.current);
        router.post(    '/user/avatar',             ctrl.uploadAvatar);
        router.delete(  '/user/:id/avatar',         ctrl.deleteAvatar);
        router.patch(   '/user/:id/reset-password', ctrl.resetPassword);
        router.get(     '/user/:id',                ctrl.getById);
        router.put(     '/user/:id',                ctrl.update);
        router.get(     '/user',                    ctrl.getPaged);
    }
}

module.exports = UserController;