const ROLE = require('../../../consts').ROLE;
const BaseController = require('../baseController');
const authorize = require('../../middlewares/authorize');
const EntityService = require('../../services/entityService');
const validate = require('../../middlewares/validateRequest');

class EntityController extends BaseController {
    constructor() {
        super(true);
        this.entityService = new EntityService();
    }

    get add() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                body: {
                    parent: this.VALIDATION_TEMPLATES.stringRequired
                }
            }),
            this.wrapAsync(async (req, res) => {
                let model = req.body;
                model.createdBy = req.user._id;
                model.created = Date.now();
                if (model.parent == 'root') {
                    model.parent = null;
                }

                let entity = await this.entityService.add({model: model});
                res.json(entity);
            })
        ];
    }
    get getById() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                params: {
                    id: this.VALIDATION_TEMPLATES.stringRequired
                }
            }),
            this.wrapAsync(async (req, res) => {
                let entity = await this.entityService.one({_id: req.params.id});
                if (entity) {
                    res.json(entity);
                } else {
                    throw 404;
                }
            })
        ];
    }
    get getBreadcrumbs() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                query: {
                    parent: this.VALIDATION_TEMPLATES.stringRequired
                }
            }),
            this.wrapAsync(async (req, res) => {
                let parent = req.query.parent;
                if (!parent || parent == 'root') {
                    parent = null;
                }

                let breadcrumbs = await this.entityService.getParents(parent);
                res.json(breadcrumbs);
            })
        ];
    }
    get getPaged() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                query: {
                    page: this.VALIDATION_TEMPLATES.page,
                    limit: this.VALIDATION_TEMPLATES.limit,
                    parent: this.VALIDATION_TEMPLATES.stringRequired,
                    title: this.VALIDATION_TEMPLATES.stringDefaultNull
                }
            }),
            this.wrapAsync(async (req, res) => {
                let params = {
                    page: req.query.page,
                    limit: req.query.limit,
                    query: {
                        parent: req.query.parent == 'root' ? null : req.query.parent
                    }
                };

                if (req.query.title) {
                    params.query.title = new RegExp(req.query.title, 'i');
                }

                let entities = await this.entityService.query(params);
                res.json(entities);
            })
        ];
    }
    get delete() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                params: {
                    id: this.VALIDATION_TEMPLATES.stringRequired
                }
            }),
            this.wrapAsync(async (req, res) => {
                let removed = await this.entityService.delete(req.params.id);
                res.json({removed: removed});
            })
        ];
    }
    get update() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            this.wrapAsync(async (req, res) => {
                let model = req.body;
                if (model.hasOwnProperty('updateCreatedDate') && model.updateCreatedDate == true) {
                    model.created = Date.now();
                }
                delete model.updateCreatedDate;
                model.updatedBy = req.user._id;
                model.updated = Date.now();

                let entity = await this.entityService.update({query: {_id: model._id}, model: model});
                res.json(entity);
            })
        ];
    }

    static setup(router) {
        let entityController = new EntityController();
        router.get(     '/entity/breadcrumbs',      entityController.getBreadcrumbs);
        router.get(     '/entity/:id',              entityController.getById);
        router.delete(  '/entity/:id',              entityController.delete);
        router.put(     '/entity/:id',              entityController.update);
        router.post(    '/entity',                  entityController.add);
        router.get(     '/entity',                  entityController.getPaged);
    }
}

module.exports = EntityController;