const Joi = require('joi');
const _ = require('lodash');
const Comment = require('../../models/Comment');
const BaseController = require('../baseController');
const valueHelper = require('../../helpers/valueHelper');
const BookService = require('../../services/bookService');
const EntityService = require('../../services/entityService');
const validate = require('../../middlewares/validateRequest');
const CommentService = require('../../services/commentService');

class ActivityController extends BaseController {
    constructor() {
        super(true);
    }

    __buildService(name) {
        switch (name) {
            case 'entity':
                return new EntityService();
            case 'book':
                return new BookService();
            case 'comment':
                return new CommentService(Comment, 'createdBy updatedBy');
            default:
                return null;
        }
    }

    async getComments(req) {
        let service = this.__buildService(req.params.for);
        if (!service) throw 404;
        let object = await service.one({query: {_id: req.params.id}});
        if (!object || (!object.published && !req.isAdminOrModerator)) throw 404;
        let params = {parent: req.params.id};
        if (req.user) params.createdBy = req.user._id;
        return service.comment.query(params);
    }
    get get() {
        return [
            validate.setup({
                params: {
                    activity: Joi.string().valid('comment'),
                    for: Joi.string().valid('entity', 'book'),
                    id: Joi.string().required()
                }
            }),
            this.wrapAsync(async (req, res) => {
                req.isAdminOrModerator = (req.user && (req.user.isAdmin || req.user.isModerator));
                switch (req.params.activity) {
                    case 'comment':
                        let comments = await this.getComments(req);
                        res.json(comments);
                        break;
                    default:
                        throw 404;
                }
            })
        ];
    }

    async addComment(req) {
        let service = this.__buildService(req.params.for);
        if (!service) throw 404;

        let body = valueHelper.getValueAsStringOrNull(req.body.comment);
        if (_.isEmpty(body) || body.length < 2) throw 400;

        if (!req.body || valueHelper.getValueAsStringOrNull(req.body.comment) || valueHelper)

        return service.comment.add({model: {
            body: body,
            parent: req.params.id,
            createdBy: req.user._id
        }});
    }
    async addRating(req) {
        let service = this.__buildService(req.params.for);
        if (!service) throw 404;
        let object = await service.one({query: {_id: req.params.id}});
        if (!object || (!object.published && !req.isAdminOrModerator)) throw 404;
        if (!req.body || (req.body.value !== true && req.body.value !== false)) throw 400;

        let params = {
            model: {
                parent: object._id,
                createdBy: req.user._id,
                value: req.body.value
            }
        };

        return service.rating.vote(params);
    }
    get add() {
        return [
            this.authorize([]),
            validate.setup({
                params: {
                    activity: Joi.string().valid('comment', 'rating'),
                    for: Joi.string().valid('entity', 'book', 'comment'),
                    id: Joi.string().required()
                }
            }),
            this.wrapAsync(async (req, res) => {
                req.isAdminOrModerator = (req.user && (req.user.isAdmin || req.user.isModerator));
                switch (req.params.activity) {
                    case 'comment':
                        res.json(await this.addComment(req));
                        break;
                    case 'rating':
                        res.json(await this.addRating(req));
                        break;
                    default:
                        throw 404;
                }
            })
        ];
    }

    async updateComment(req) {
        let service = this.__buildService(req.params.for);
        if (!service) throw 404;

        let params = {
            query: {
                _id: req.params.activityId
            },
            model: req.body
        };

        params.model.updated = Date.now();
        params.model.updatedBy = req.user._id;

        let takeFields = ['body', 'updated', 'updatedBy'];
        if (req.isAdminOrModerator) {
            takeFields.push('systemBody');
        } else {
            let comment = await service.comment.one({query: {_id: req.params.activityId, createdBy: req.user._id}});
            if (!comment) throw 403;
        }

        params.model = _.pick(params.model, takeFields);
        return await service.comment.update(params);
    }
    get update() {
        return [
            this.authorize([]),
            validate.setup({
                params: {
                    activity: Joi.string().valid('comment'),
                    for: Joi.string().valid('entity', 'book'),
                    id: Joi.string().required(),
                    activityId: Joi.string().required()
                }
            }),
            this.wrapAsync(async (req, res) => {
                req.isAdminOrModerator = (req.user && (req.user.isAdmin || req.user.isModerator));
                switch (req.params.activity) {
                    case 'comment':
                        let data = await this.updateComment(req);
                        res.json(data);
                        break;
                    default:
                        throw 404;
                }
            })
        ]
    }

    async deleteComment(req) {
        let service = this.__buildService(req.params.for);
        if (!service) throw 404;

        if (!req.isAdminOrModerator) {
            let comment = await service.comment.one({query: {_id: req.params.activityId, createdBy: req.user._id}});
            if (!comment) throw 403;
        }

        await service.comment.delete({query: {_id: req.params.activityId}});
        await service.comment.updateCounter(req.params.id);
        return {deleted: req.params.activityId};
    }
    get delete() {
        return [
            this.authorize([]),
            validate.setup({
                params: {
                    activity: Joi.string().valid('comment'),
                    for: Joi.string().valid('entity', 'book'),
                    id: Joi.string().required(),
                    activityId: Joi.string().required()
                }
            }),
            this.wrapAsync(async (req, res) => {
                req.isAdminOrModerator = (req.user && (req.user.isAdmin || req.user.isModerator));
                switch (req.params.activity) {
                    case 'comment':
                        let data = await this.deleteComment(req);
                        res.json(data);
                        break;
                    default:
                        throw 404;
                }
            })
        ];
    }

    static setup(router) {
        let ctrl = new ActivityController();
        router.post('/activity/:activity/:for/:id', ctrl.add);
        router.get('/activity/:activity/:for/:id', ctrl.get);
        router.put('/activity/:activity/:for/:id/:activityId', ctrl.update);
        router.delete('/activity/:activity/:for/:id/:activityId', ctrl.delete);
    }
}

module.exports = ActivityController;