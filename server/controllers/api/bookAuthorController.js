const ROLE = require('../../../consts').ROLE;
const BaseController = require('../baseController');
const authorize = require('../../middlewares/authorize');
const validate = require('../../middlewares/validateRequest');
const BookAuthorService = require('../../services/bookAuthorService');

class BookAuthorController extends BaseController {
    constructor() {
        super(true);
        this.bookAuthorService = new BookAuthorService();
    }

    get add() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                body: {
                    fullName: this.VALIDATION_TEMPLATES.fullName
                }
            }),
            (req, res) => {
                let model = req.body;
                model.createdBy = req.user._id;
                model.created = Date.now();

                this.bookAuthorService
                    .add({model: model})
                    .then(author => res.json(author))
                    .catch(this.setupOnError(req, res));
            }
        ];
    }
    get getPaged() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                query: {
                    page: this.VALIDATION_TEMPLATES.page,
                    limit: this.VALIDATION_TEMPLATES.limit,
                    fullName: this.VALIDATION_TEMPLATES.stringDefaultNull
                }
            }),
            (req, res) => {
                let params = {
                    page: req.query.page,
                    limit: req.query.limit,
                    query: {}
                };
                if (req.query.fullName) {
                    params.query.fullName = new RegExp(req.query.fullName, 'i');
                }

                this.bookAuthorService
                    .query(params)
                    .then(authors => res.json(authors))
                    .catch(this.setupOnError(req, res));
            }
        ];
    }
    get getById() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                params: {
                    id: this.VALIDATION_TEMPLATES.stringRequired
                }
            }),
            (req, res) => {
                this.bookAuthorService
                    .one({query: {_id: req.params.id}})
                    .then(author => res.json(author))
                    .catch(this.setupOnError(req, res));
            }
        ];
    }
    get update() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                params: {
                    id: this.VALIDATION_TEMPLATES.stringRequired
                },
                body: {
                    _id: this.VALIDATION_TEMPLATES.stringRequired,
                    fullName: this.VALIDATION_TEMPLATES.fullName
                }
            }),
            (req, res) => {
                let model = req.body;
                model.updatedBy = req.user._id;
                model.updated = Date.now();
                let params = {
                    model: model,
                    query: {
                        _id: req.params.id
                    }
                };
                this.bookAuthorService
                    .update(params)
                    .then(author => res.json(author))
                    .catch(this.setupOnError(req, res));
            }
        ];
    }
    get delete() {
        return [
            authorize.setup([ROLE.ADMIN, ROLE.MODERATOR], this.__api),
            validate.setup({
                params: {
                    id: this.VALIDATION_TEMPLATES.stringRequired
                }
            }),
            (req, res) => {
                this.bookAuthorService
                    .delete(req.params.id)
                    .then(removed => res.json({removed: removed}))
                    .catch(this.setupOnError(req, res));
            }
        ];
    }

    static setup(router) {

        let bookAuthorController = new BookAuthorController();

        router.get(     '/book/author',              bookAuthorController.getPaged);
        router.post(    '/book/author',              bookAuthorController.add);
        router.get(     '/book/author/:id',          bookAuthorController.getById);
        router.put(     '/book/author/:id',          bookAuthorController.update);
        router.delete(  '/book/author/:id',          bookAuthorController.delete);
    }
}

module.exports = BookAuthorController;