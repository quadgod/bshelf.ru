const Joi = require('joi');
const logger = require('../logger');
const authorize = require('../middlewares/authorize');

class ErrorCallback {
    constructor(req, res, isApi) {
        this.res = res;
        this.req = req;
        this.isApi = isApi;
    }

    invoke(err) {
        try {
            logger.error('Error', {error: JSON.stringify(err)});
        } catch(e) {
            logger.error('Error', {error: err});
        }
        logger.error('Error', {error: err});
        if (this.isApi) {
            if (err == 'email' || err == 'nickname') {
                return this.res.boom.conflict(err);
            } else if (err == 400) {
                return this.res.boom.badRequest();
            } else if (err == 403) {
                return this.res.boom.forbidden();
            } else if (err == 404) {
                return this.res.boom.notFound();
            } else if (err == 409 || err.code == 11000) {
                return this.res.boom.conflict();
            } else {
                return this.res.boom.badImplementation(err);
            }
        } else {
            if (err == 404) {
                return this.res.status(404).render('errors/404', {disableFooter: true, disableHeader: true});
            } else {
                return this.res.status(500).render('errors/500', {disableFooter: true, disableHeader: true});
            }
        }
    }

    static setup(req, res, isApi = true) {
        let errorCallback = new ErrorCallback(req, res, isApi);
        return errorCallback.invoke.bind(errorCallback);
    }
}


class BaseController {
    constructor(api = true) {
        this.__api = api;
        this.VALIDATION_TEMPLATES = {
            limit: Joi.number().integer().min(1).default(10),
            page: Joi.number().integer().min(1).default(1),
            stringDefaultNull: Joi.string().default(null),
            stringRequired: Joi.string().required(),
            nickname: Joi.string().min(2).max(30),
            max50trucate: Joi.string().max(50).truncate(),
            fullName: Joi.string().min(2).max(200).required(),
            password: Joi.string().min(6).max(30).required(),
            comment: Joi.string().min(2).max(3000).truncate().required()
        };
    }

    wrapAsync(handler) {
        return (req, res) => {
            handler(req, res).catch(this.setupOnError(req, res));
        }
    }

    authorize(roles = []) {
        return authorize.setup(roles, this.__api);
    }

    setupOnError(req, res) {
        let errorCallback = new ErrorCallback(req, res, this.__api);
        return errorCallback.invoke.bind(errorCallback);
    }
}

module.exports = BaseController;