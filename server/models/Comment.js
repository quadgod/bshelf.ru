const Rating = require('./Rating');
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

let CommentSchema = new mongoose.Schema({
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    created: {
        type: Date,
        required: true,
        default: Date.now
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: false,
        null: true,
        default: null
    },
    updated: {
        type: Date,
        null: true,
        required: false,
        default: null
    },
    body: {
        type: String,
        required: true,
        trim: true,
        minlength: 2,
        maxlength: 5000
    },
    systemBody: {
        type: String,
        null: true,
        trim: true,
        default: null,
        maxlength: 5000
    },
    parent: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    rating: {
        type: Number,
        required: true,
        default: 0
    }
});

CommentSchema.pre('remove', function(next) {
    Rating.remove({parent: this._id}).then(() => {
        next();
    }).catch(next);
});

CommentSchema.plugin(mongoosePaginate);
let Comment = mongoose.model('Comment', CommentSchema);

module.exports = Comment;