const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const validate = require('mongoose-validator');
const crypto = require('crypto');
const _ = require('lodash');
const consts = require('../../consts');
const uuid = require('node-uuid');
const valueHelper = require('../helpers/valueHelper');

let UserSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true,
        validate: [ validate({ validator: 'isEmail' }) ],
        trim: true
    },
    nickname: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    created: {
        type: Date,
        required: true,
        default: Date.now
    },
    updated: {
        type: Date,
        default: null
    },
    vkId: {
        type: String,
        trim: true,
        null: true,
        select: false,
        default: null
    },
    facebookId: {
        type: String,
        trim: true,
        null: true,
        select: false,
        default: null
    },
    googleId: {
        type: String,
        trim: true,
        null: true,
        select: false,
        default: null
    },
    avatar: {
        type: String,
        trim: true,
        null: true,
        default: null
    },
    name: {
        first: {
            type: String,
            trim: true,
            default: null
        },
        last: {
            type: String,
            trim: true,
            default: null
        },
        middle: {
            type: String,
            trim: true,
            default: null
        }
    },
    metadata: {
        phone: {
            type: String,
            select: false,
            trim: true,
            default: null
        }
    },
    roles: {
        type: [String],
        enum: consts.ROLES,
        required: true,
        default: [consts.ROLE.USER]
    },
    hashedPassword: {
        type: String,
        select: false
    },
    salt: {
        type: String,
        select: false
    },
    isDeleted: {
        type: Boolean,
        required: true,
        default: true
    },
    activateAccountToken: {
        token: {
            type: String,
            default: uuid.v4
        },
        expire: {
            type: Date,
            default: function () {
                let currentDate = new Date();
                return new Date(currentDate.setTime(currentDate.getTime() + 3 * 86400000));
            }
        }
    },
    resetPasswordToken: {
        token: {
            type: String,
            default: uuid.v4
        },
        expire: {
            type: Date,
            default: function () {
                let currentDate = new Date();
                return new Date(currentDate.setTime(currentDate.getTime() + 3 * 86400000));
            }
        }
    }
});

UserSchema.methods = {
    validatePassword: function (password) {
        return this.hashPassword(password) === this.hashedPassword;
    },
    makeSalt: function () {
        return crypto.randomBytes(16).toString('base64');
    },
    hashPassword: function (password) {
        if (valueHelper.isNullOrWhiteSpace(password) || valueHelper.isNullOrWhiteSpace(this.salt)) {
            return '';
        }
        let salt = Buffer.from(this.salt, 'base64');
        return crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha256').toString('base64');
    },
    toJSON: function () {
        let obj = this.toObject();
        delete obj.hashedPassword;
        delete obj.salt;
        delete obj.password;
        delete obj.resetPasswordToken;
        delete obj.activateAccountToken;
        return obj;
    }
};

UserSchema.virtual('password').set(function (password) {
    this.salt = this.makeSalt();
    this.hashedPassword = this.hashPassword(password);
}).get(function () {
    return this.hashedPassword;
});

UserSchema.plugin(mongoosePaginate);
let User = mongoose.model('User', UserSchema);

module.exports = User;