const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

let BookPerformerSchema = new mongoose.Schema({
    fullName: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    created: {
        type: Date,
        required: true,
        default: Date.now
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: false,
        null: true,
        default: null
    },
    updated: {
        type: Date,
        null: true,
        required: false,
        default: null
    }
});

BookPerformerSchema.plugin(mongoosePaginate);
let BookPerformer = mongoose.model('BookPerformer', BookPerformerSchema);
module.exports = BookPerformer;