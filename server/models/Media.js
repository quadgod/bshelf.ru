const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const entityType = require('../models/EntityType');
let MediaSchema = new mongoose.Schema({
    type: {
        type: Number,
        required: true,
        enum: [entityType.FOLDER, entityType.MEDIA_FILE],
        default: entityType.FOLDER
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    created: {
        type: Date,
        required: true,
        default: Date.now
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        null: true,
        default: null
    },
    updated: {
        type: Date,
        null: true,
        default: null
    },
    title: {
        type: String,
        trim: true,
        required: true,
        minlength: 1
    },
    fileName: {
        type: String,
        null: true,
        default: null
    },
    duration: {
        type: Number,
        required: true,
        default: 0
    },
    mimeType: {
        type: String,
        null: true,
        default: null
    },
    size: {
        type: Number,
        default: 0
    },
    parent: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Media',
        default: null
    }
});

MediaSchema.index({type: 1, title: 1, parent: 1}, {unique: true});
MediaSchema.plugin(mongoosePaginate);
let Media = mongoose.model('Media', MediaSchema);
module.exports = Media;