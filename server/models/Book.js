const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const BookType = require('./BookType');
const EntityType = require('./EntityType');

let BookSchema = new mongoose.Schema({
    type: {
        type: Number,
        required: true,
        enum: [EntityType.FOLDER, EntityType.BOOK_SERIES, EntityType.BOOK]
    },
    bookType: {
        type: Number,
        enum: [BookType.AUDIO, BookType.EBOOK, BookType.REMOTE_BOOK]
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    created: {
        type: Date,
        required: true,
        default: Date.now
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        null: true,
        default: null
    },
    updated: {
        type: Date,
        null: true,
        default: null
    },
    title: {
        type: String,
        trim: true,
        required: true
    },
    description: {
        type: String,
        trim: true,
        default: ''
    },
    cover: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Media',
        default: null
    },
    year: {
        type: Number,
        null: true,
        default: null
    },
    ISBN: {
        type: String,
        null: true,
        trim: true,
        default: null
    },
    medias: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Media',
        default: []
    }],
    tags: [{
        type: String,
        default: []
    }],
    pages: {
        type: Number,
        default: 0
    },
    duration: {
        type: Number,
        default: 0
    },
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'BookAuthor'
    },
    performer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'BookPerformer'
    },
    mediasUnpublished: {
        type: Boolean,
        required: true,
        default: false
    },
    sortOrder: {
        type: Number,
        required: true,
        default: 1
    },
    published: {
        type: Boolean,
        required: true,
        default: true
    },
    url: {
        type: String,
        required: true,
        match: /^[a-z-_0-9]+$/
    },
    parent: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Book',
        default: null
    },
    seriesParent: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Book',
        default: null
    },
    comments: {
        type: Number,
        required: true,
        default: 0
    },
    rating: {
        type: Number,
        required: true,
        default: 0
    },
    searchString: {
        type: String,
        default: ''
    },
    counters: {
        audiobooks: {
            published: {
                type: Number,
                required: true,
                default: 0
            },
            total: {
                type: Number,
                required: true,
                default: 0
            }
        },
        ebooks: {
            published: {
                type: Number,
                required: true,
                default: 0
            },
            total: {
                type: Number,
                required: true,
                default: 0
            }
        },
        remote: {
            published: {
                type: Number,
                required: true,
                default: 0
            },
            total: {
                type: Number,
                required: true,
                default: 0
            }
        }
    }
}, {toObject: {virtuals: true}, toJSON: {virtuals: true}});

BookSchema.index({type: 1, url: 1, parent: 1}, {unique: true});
BookSchema.index({
    searchString: 'text',
    title: 'text',
    description: 'text'
}, {
    name: 'search_text_index',
    weights: {
        searchString: 10,
        title: 5,
        description: 4
    }
});
BookSchema.plugin(mongoosePaginate);
let Book = mongoose.model('Book', BookSchema);
module.exports = Book;