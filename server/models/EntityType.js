module.exports = {
    FOLDER: 1,
    DOCUMENT: 2,
    MEDIA_FILE: 3,
    BOOK_SERIES: 4,
    BOOK: 5
};