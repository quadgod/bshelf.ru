const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const EntityType = require('./EntityType');
const LayoutType = require('./LayoutType');

let EntitySchema = new mongoose.Schema({
    url: {
        type: String,
        required: true,
        match: /^[a-z-_0-9]+$/
    },
    type: {
        type: Number,
        required: true,
        enum: [EntityType.FOLDER, EntityType.DOCUMENT],
        default: EntityType.FOLDER
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    created: {
        type: Date,
        required: true,
        default: Date.now
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        null: true,
        default: null
    },
    updated: {
        type: Date,
        null: true,
        default: null
    },
    title: {
        type: String,
        trim: true,
        required: true,
        maxlength: 300,
        minlength: 2
    },
    shortBody: {
        type: String,
        null: true,
        default: null
    },
    body: {
        type: String,
        null: true,
        default: null
    },
    sortOrder: {
        type: Number,
        required: true,
        default: 1
    },
    published: {
        type: Boolean,
        default: true
    },
    pinnedUp: {
        type: Boolean,
        default: false
    },
    media: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Media',
        null: true,
        default: null
    },
    tags: [{
        type: String,
        default: []
    }],
    parent: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Entity'
    },
    comments: {
        type: Number,
        required: true,
        default: 0
    },
    rating: {
        type: Number,
        required: true,
        default: 0
    },
    counters: {
        published: {
            type: Number,
            required: true,
            default: 0
        },
        total: {
            type: Number,
            required: true,
            default: 0
        }
    },
    layoutType: {
        type: Number,
        required: true,
        enum: [LayoutType.DEFAULT],
        default: LayoutType.DEFAULT
    }
});

EntitySchema.index({type: 1, url: 1, parent: 1}, {unique: true});
EntitySchema.plugin(mongoosePaginate);
let Entity = mongoose.model('Entity', EntitySchema);
module.exports = Entity;