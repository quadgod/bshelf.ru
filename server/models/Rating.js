const mongoose = require('mongoose');

let RatingSchema = new mongoose.Schema({
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    created: {
        type: Date,
        required: true,
        default: Date.now
    },
    parent: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    value: {
        type: Boolean,
        default: true,
        required: true
    }
});

let Rating = mongoose.model('Rating', RatingSchema);
module.exports = Rating;