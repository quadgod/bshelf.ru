const _ = require('lodash');
const uuid = require('node-uuid');
const Promise = require('bluebird');
const User = require('../models/User');
const ROLE = require('../../consts').ROLE;

class UserService {
    constructor() {}

    prepareForView(user) {
        let model = user.toJSON();
        model.isAdmin = _.includes(model.roles, ROLE.ADMIN);
        model.isModerator = _.includes(model.roles, ROLE.MODERATOR);
        return model;
    }

    async query(params) {
        let {query, limit, page, sort, populate} = params;
        let options = {
            page: page,
            limit: limit,
            sort: sort || 'nickname'
        };
        if (populate) {
            options.populate = populate
        }
        return await User.paginate(query, options);
    }
    async one(params) {
        let {query, select, populate} = params;
        let action = User.findOne(query);
        if (populate) action.populate(populate);
        if (select) action.select(select);
        return await action.exec();
    }
    async delete(params) {
        let {query, populate, select} = params;
        let action = User.findOneAndUpdate(query, {$set: {isDeleted: true}}, {new: true});
        if (populate) action.populate(populate);
        if (select) action.select(select);
        return await action;
    }
    async update(params) {
        let {query, model, select, populate} = params;
        let action = User.findOneAndUpdate(query, {$set: model}, {new: true});
        if (populate) action.populate(populate);
        if (select) action.select(select);
        return await action.exec();
    }
    async resetPassword(params) {
        let {query, model} = params;
        let user = await User.findOne(query).select('_id salt hashedPassword').exec();
        if (!user) {
            throw 404;
        }

        if (model.hasOwnProperty('oldPassword')) {
            if (user.validatePassword(model.oldPassword)) {
                user.password = model.password;
            } else {
                throw 400;
            }
        } else {
            user.password = model.password;
        }

        return await user.save();
    }
    async register(params) {
        let {model, populate, select} = params;
        let [emailsCount, nicknameCount, totalCount] = await Promise.all([
            User.count({email: model.email}),
            User.count({nickname: model.nickname}),
            User.count({})
        ]);

        if (emailsCount > 0) {
            throw 'email';
        }

        if (nicknameCount > 0) {
            throw 'nickname';
        }

        let user = new User(model);

        if (totalCount > 0) {
            user.roles = [ROLE.USER];
        } else {
            user.roles = [ROLE.ADMIN];
        }

        user = await user.save();

        if (populate || select) {
            if (populate) user.populate(populate);
            if (select) user.select(select);
            return await user.exec();
        } else {
            return user;
        }
    }
    async activateAccount(params) {
        let {model, populate, select} = params;

        let user = await User.findOne({
            $and: [
                {_id: model.userid},
                {'activateAccountToken.token': model.token},
                {'activateAccountToken.token': {$ne: null}},
                {'activateAccountToken.expire': {$gte: Date.now()}}
            ]
        }).exec();

        if (!user) {
            throw 404;
        }

        user.isDeleted = false;
        user.activateAccountToken.token = null;
        user = await user.save();

        if (populate || select) {
            if (populate) user.populate(populate);
            if (select) user.select(select);
            return await user.exec();
        } else {
            return user;
        }
    }
    async findOrRegisterVkontakte(params) {
        let {model} = params;
        let selectFields = '_id email vkId isDeleted salt hashedPassword';
        let user = await User.findOne({vkId: model.profile.id}).select(selectFields).exec();

        if (user && !user.isDeleted) {
            return user;
        }

        if (user && user.isDeleted) {
            throw new Error('User is blocked');
        }

        user = await User.findOne({email: model.email}).select(selectFields).exec();

        if (user && user.isDeleted) {
            throw new Error('User is blocked');
        } else {
            user.vkId = model.profile.id;
            return await user.save();
        }

        let newUserModel = {
            nickname: model.profile.usrername,
            password: uuid.v4(),
            email: model.email,
            isDeleted: false,
            name: {
                first: model.profile.name && model.profile.name.givenName || null,
                last: model.profile.name && model.profile.name.familyName || null
            }
        };

        let nicknameExists = true;
        let suffix = Math.random() * (10000000 - 1) + 1;

        while (nicknameExists) {
            let found = await User.find({nickname: newUserModel.nickname}).limit(1);
            if (found) {
                suffix = Math.random() * (10000000 - 1) + 1;
                newUserModel.nickname = `${newUserModel.nickname}_${suffix}`;
            } else {
                nicknameExists = false;
            }
        }

        let newUser = new User(newUserModel);
        return await newUser.save();
    }
}

module.exports = UserService;