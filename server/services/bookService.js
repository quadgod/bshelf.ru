const _ = require('lodash');
const Promise = require('bluebird');
const mongoose = require('mongoose');
const Book = require('../models/Book');
const BookType = require('../models/BookType');
const EntityType = require('../models/EntityType');
const valueHelper = require('../helpers/valueHelper');
const RatingService = require('../services/ratingService');
const CommentService = require('../services/commentService');
const BookAuthorService = require('../services/bookAuthorService');

class BookService {
    constructor() {
        this.populateFields = 'createdBy updatedBy author performer parent medias cover seriesParent';
        this.rating = new RatingService(Book, this.populateFields);
        this.comment = new CommentService(Book, this.populateFields);
    }

    async query(params) {
        let {query, limit, page, sort, populate} = params;

        if (_.isUndefined(limit) || _.isUndefined(page)) {
            return await Book.find(query).populate(populate || this.populateFields).sort(sort || 'type -created');
        } else {
            let options = {
                page: valueHelper.getValueAsIntOrDefault(page, 1, 1),
                limit: valueHelper.getValueAsIntOrDefault(limit, 10, 1),
                sort: sort || 'type -created',
                populate: populate || this.populateFields
            };
            return await Book.paginate(query, options);
        }
    }
    async one(params) {
        let {query, select, populate} = params;
        let action = Book.findOne(query).populate(populate || this.populateFields);
        if (select) action.select(select);
        return await action.exec();
    }
    async add(params) {
        let {model, populate} = params;
        let book = new Book(model);
        await book.save();

        if (book.author) {
            let bookAuthorService = new BookAuthorService();
            let author = await bookAuthorService.one({query: {_id: book.author}});
            if (author) {
                book.searchString = `${author.fullName} - ${book.title}`;
                await book.save();
            }
        }

        if (book.parent && book.type == EntityType.BOOK) {
            let parent = await Book.findOne({_id: book.parent});
            if (parent && parent.type == EntityType.BOOK_SERIES) {
                book.seriesParent = parent.parent;
                await book.save();
            }
            await this.updateParentsCounters(book.parent);
        }
        await this.calcDuration(book._id);
        return await book.populate(populate || this.populateFields);
    }
    async calcDuration(id) {
        let book = await this.one({query: {_id: id}});
        if (!book) return;

        if (book.type != EntityType.BOOK) {
            book.duration = 0;
            await book.save();
            return;
        }

        if (book.type == EntityType.BOOK && book.bookType == BookType.AUDIO) {
            if (book.medias && book.medias.length > 0) {
                book.duration = 0;
                book.medias.forEach((m) => {
                    book.duration += m.duration;
                });
            } else {
                book.duration = 0;
            }
            await book.save();
        }
    }
    async updateParentsCounters(id) {
        if (!id) return;

        let parents = await this.getParents(id);
        parents.reverse();

        for (let i = 0; i < parents.length; i++) {
            let objectId = parents[i]._id;
            await this.updateCounters(objectId);
            let counters = await Book.aggregate([
                {
                    $match: {
                        $or: [
                            {parent: new mongoose.Types.ObjectId(objectId), type: EntityType.BOOK_SERIES},
                            {parent: new mongoose.Types.ObjectId(objectId), type: EntityType.FOLDER},
                        ]
                    }
                },
                {
                    $group: {
                        _id: null,
                        audiobooksTotal: {$sum: '$counters.audiobooks.total'},
                        audiobooksPublished: {$sum: '$counters.audiobooks.published'},
                        ebooksTotal: {$sum: '$counters.ebooks.total'},
                        ebooksPublished: {$sum: '$counters.ebooks.published'},
                        remoteTotal: {$sum: '$counters.remote.total'},
                        remotePublished: {$sum: '$counters.remote.published'},
                        count: {$sum: 1}
                    }
                }
            ]).allowDiskUse(true);
            if (counters.length > 0) {
                let counter = counters[0];
                let bookToUpdate = await Book.findOne({_id: parents[i]._id});
                if (bookToUpdate) {
                    bookToUpdate.counters.audiobooks.total += counter.audiobooksTotal;
                    bookToUpdate.counters.ebooks.total += counter.ebooksTotal;
                    bookToUpdate.counters.remote.total += counter.remoteTotal;
                    bookToUpdate.counters.audiobooks.published += counter.audiobooksPublished;
                    bookToUpdate.counters.ebooks.published += counter.ebooksPublished;
                    bookToUpdate.counters.remote.published += counter.remotePublished;
                    await bookToUpdate.save();
                }
            }
        }
    }
    async updateCounters(id) {
        if (!id) return;
        let [
            audiobooksTotalCount,
            ebooksTotalCount,
            remoteTotalCount,
            audiobooksPublishedCount,
            ebooksTotalPublishedCount,
            remoteTotalPublishedCount,
            book
        ] = await Promise.all([
            Book.count({parent: id, bookType: BookType.AUDIO}),
            Book.count({parent: id, bookType: BookType.EBOOK}),
            Book.count({parent: id, bookType: BookType.REMOTE_BOOK}),
            Book.count({parent: id, bookType: BookType.AUDIO, published: true}),
            Book.count({parent: id, bookType: BookType.EBOOK, published: true}),
            Book.count({parent: id, bookType: BookType.REMOTE_BOOK, published: true}),
            Book.findOne({_id: id}).populate('parent')
        ]);

        if (book) {
            book.counters.audiobooks.total = audiobooksTotalCount;
            book.counters.ebooks.total = ebooksTotalCount;
            book.counters.remote.total = remoteTotalCount;
            book.counters.audiobooks.published = audiobooksPublishedCount;
            book.counters.ebooks.published = ebooksTotalPublishedCount;
            book.counters.remote.published = remoteTotalPublishedCount;
            await book.save();
        }
    }
    async update(params) {
        let {query, model, select, populate} = params;
        let action = Book.findOneAndUpdate(query, {$set: model}, {new: true}).populate(populate || this.populateFields);
        if (select) action.select(select);
        let book = await action;
        if (!book) return null;

        if (book.parent && book.type == EntityType.BOOK) {
            await this.updateParentsCounters(book.parent);
        }
        await this.calcDuration(book._id);
        return book;
    }
    async delete(id) {
        let book = await Book.findOne({_id: id}).populate('parent');
        if (!book) throw 404;

        let childrenIds = await this.getChildrenIds([id]);
        if (childrenIds.length > 0) throw 403;

        this.rating.delete({query: {parent: book._id}});
        this.comment.delete({query: {parent: book._id}});

        if (book && book.parent && (book.parent.type == EntityType.FOLDER || book.parent.type == EntityType.BOOK_SERIES)) {
            await Book.remove({_id: id});
            await this.updateParentsCounters(book.parent);
        } else {
            await Book.remove({_id: id});
        }

        if (book.parent) {
            await this.calcDuration(book.parent._id);
        }

        return childrenIds;
    }
    async getParents(id = null, path = []) {
        if (!id || id == 'root') {
            id = null;
        }

        if (!id) {
            return path;
        } else {
            let parent = await Book.findOne({
                $and: [
                    {_id: id},
                    {
                        $or: [
                            {type: EntityType.FOLDER},
                            {type: EntityType.BOOK_SERIES}
                        ]
                    }
                ]
            }).populate(this.populateFields);

            if (parent) {
                path.unshift(parent);
            }

            if (parent && parent.parent) {
                return this.getParents(parent.parent, path);
            } else {
                return path;
            }
        }
    }
    async getChildrenIds(parentIds = [], foundIds = []) {
        let books = await Book.find({parent: {$in: parentIds}});
        if (books.length > 0) {
            let ids = _.map(books, '_id');
            foundIds = foundIds.concat(ids);
            return this.getChildrenIds(ids, foundIds);
        } else {
            return foundIds;
        }
    }
    async getCategories(bookType) {
        let sortModifier = '';

        switch (bookType) {
            case BookType.AUDIO:
                sortModifier = 'audiobooks';
                break;
            case BookType.EBOOK:
                sortModifier = 'ebooks';
                break;
            default:
                sortModifier = 'remote';
                break;
        }

        let params = {
            query: {
                parent: null,
                type: EntityType.FOLDER
            },
            page: 1,
            limit: 10,
            sort: `-counters.${sortModifier}.published -rating`
        };

        params.query[`counters.${sortModifier}.published`] = {$gt: 0};
        return await this.query(params);
    }
}

module.exports = BookService;