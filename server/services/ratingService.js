const Promise = require('bluebird');
const Rating = require('../models/Rating');

class RatingService {
    constructor(schema, objectPopulateFields = null) {
        this.schema = schema;
        this.objectPopulateFields = objectPopulateFields;
    }

    async vote(params) {
        let {model, populate} = params;
        let object = await this.schema.findOne({_id: model.parent});
        if (!object) throw 404;

        let rating = await Rating.findOne({
            parent: model.parent,
            createdBy: model.createdBy
        });

        if (rating && rating.value == model.value) {
            await rating.remove();
            rating = null;
        } else {
            rating = await Rating.findOneAndUpdate({
                parent: model.parent,
                createdBy: model.createdBy
            }, {
                parent: model.parent,
                createdBy: model.createdBy,
                value: model.value
            }, {
                upsert: true,
                new: true,
                setDefaultsOnInsert: true
            }).populate('createdBy');
        }

        object = await this.updateCounter(object._id);
        if (this.objectPopulateFields) {
            object = await object.populate(populate || this.objectPopulateFields);
        }

        return {
            object: object,
            rating: rating
        };
    }
    async delete(params) {
        let {query} = params;
        await Rating.remove(query);
    }
    async updateCounter(id) {
        let object = await this.schema.findOne({_id: id});
        if (!object) throw 404;

        let [positiveCount, negativeCount] = await Promise.all([
            Rating.count({parent: id, value: true}),
            Rating.count({parent: id, value: false})
        ]);

        let rating = 0;

        if (positiveCount > negativeCount) {
            rating = positiveCount - negativeCount;
        }

        if (negativeCount > positiveCount) {
            rating = ((negativeCount - positiveCount) * -1);
        }

        object.rating = rating;
        return await object.save();
    }
}

module.exports = RatingService;