const Book = require('../models/Book');
const BookAuthor = require('../models/BookAuthor');

class BookAuthorService {

    constructor() {
        this.populateFields = 'createdBy updatedBy';
    }

    async query(params) {
        let {query, limit, page, sort, populate} = params;
        let options = {
            page: page || 1,
            limit: limit || 10,
            sort: sort || 'fullName -created',
            populate: populate || this.populateFields
        };
        return await BookAuthor.paginate(query, options);
    }
    async one(params) {
        let {query, select, populate} = params;
        let action = BookAuthor.findOne(query).populate(populate || this.populateFields);
        if (select) action.select(select);
        return await action;
    }
    async add(params) {
        let {model, select, populate} = params;
        let newAuthor = new BookAuthor(model);
        let author = await newAuthor.save();
        let action = author.populate(populate || this.populateFields);
        if (select) action.select(select);
        return await action;
    }
    async update(params) {
        let {query, model, select, populate} = params;
        let action = BookAuthor.findOneAndUpdate(query, {$set: model}, {new: true}).populate(populate || this.populateFields);
        if (select) action.select(select);
        return await action;
    };
    async delete(id) {
        let booksCount = await Book.count({author: id});
        if (booksCount > 0) throw 403;
        await BookAuthor.remove({_id: id});
        return id;
    }
}

module.exports = BookAuthorService;