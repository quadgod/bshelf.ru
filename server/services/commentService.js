const _ = require('lodash');
const mongoose = require('mongoose');
const Rating = require('../models/Rating');
const Comment = require('../models/Comment');
const RatingService = require('./ratingService');
const valueHelper = require('../helpers/valueHelper');

class CommentService {
    constructor(schema, objectPopulateFields = null) {
        this.schema = schema;
        this.defaultSort = {created: 1};
        this.objectPopulateFields = objectPopulateFields;
        this.defaultPopulateFields = 'createdBy updatedBy';
        this.rating = new RatingService(Comment, this.defaultPopulateFields);
    }

    async add(params) {
        let {model, populate} = params;

        let object = await this.schema.findOne({_id: model.parent});
        if (!object) throw 404;

        let newComment = new Comment(model);
        let comment = await newComment.save();

        object = await this.updateCounter(object._id);
        comment = await comment.populate(this.defaultPopulateFields);

        if (this.objectPopulateFields) {
            object = await object.populate(populate || this.objectPopulateFields);
        }

        return {
            object: object,
            comment: comment
        };
    }
    async delete(params) {
        let {query} = params;
        let comments = await Comment.find(query);
        for (let i = 0; i < comments.length; i++) {
            await comments[i].remove();
        }
    }
    async update(params) {
        let {query, model, populate} = params;

        let comment = await Comment.findOne(query);
        if (!comment) throw 404;

        let keys = Object.keys(model);
        keys.forEach(k => {
            comment[k] = model[k];
        });

        comment = await comment.save();
        return await comment.populate(populate || this.defaultPopulateFields);
    }
    async one(params) {
        let {query, populate} = params;
        return await Comment.findOne(query).populate(populate || this.defaultPopulateFields);
    }
    async query(params) {
        let {parent, createdBy, page, limit, populate, sort} = params;
        let aggregateOptions = [];
        aggregateOptions.push({$match: {parent: new mongoose.Types.ObjectId(parent)}});

        if (createdBy) {
            aggregateOptions.push({
                $lookup: {
                    from: 'ratings',
                    localField: '_id',
                    foreignField: 'parent',
                    as: 'ratings'
                }
            });
            aggregateOptions.push({
                $project: {
                    createdBy: 1,
                    updatedBy: 1,
                    rating: 1,
                    body: 1,
                    systemBody: 1,
                    updated: 1,
                    created: 1,
                    ratings: {
                        $filter: {
                            input: '$ratings',
                            as: 'rating',
                            cond: {
                                $eq: ['$$rating.createdBy', new mongoose.Types.ObjectId(createdBy)]
                            }
                        }
                    }
                }
            });
        }
        aggregateOptions.push({$sort: sort || this.defaultSort});

        if (!_.isUndefined(page) && !_.isUndefined(limit)) {
            page = valueHelper.getValueAsIntOrDefault(page, 1, 1);
            limit = valueHelper.getValueAsIntOrDefault(limit, 10, 1);
            aggregateOptions.push({
                "$skip": ((page - 1) * limit)
            });
            aggregateOptions.push({
                "$limit": limit
            });
        }

        let comments = await Comment.aggregate(aggregateOptions).allowDiskUse(true);
        return await Comment.populate(comments, this.defaultPopulateFields);
    }
    async updateCounter(id) {
        let object = await this.schema.findOne({_id: id});
        if (!object) throw 404;

        object.comments = await Comment.count({parent: id});
        return await object.save();
    }
}

module.exports = CommentService;