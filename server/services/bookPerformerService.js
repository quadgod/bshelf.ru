const Book = require('../models/Book');
const BookPerformer = require('../models/BookPerformer');

class BookPerformerService {

    constructor() {
        this.populateFields = 'createdBy updatedBy';
    }

    async query(params) {
        let {query, limit, page, sort, populate} = params;
        let options = {
            page: page || 1,
            limit: limit || 10,
            sort: sort || 'fullName -created',
            populate: populate || this.populateFields
        };
        return await BookPerformer.paginate(query, options);
    }
    async one(params) {
        let {query, select, populate} = params;
        let action = BookPerformer.findOne(query).populate(populate || this.populateFields);
        if (select) action.select(select);
        return await action;
    }
    async add(params) {
        let {model, select, populate} = params;
        let newBookPerformer = new BookPerformer(model);
        let performer = await newBookPerformer.save();
        let action = performer.populate(populate || this.populateFields);
        if (select) action.select(select);
        return await action;
    }
    async update(params) {
        let {query, model, select, populate} = params;
        let action = BookPerformer.findOneAndUpdate(query, {$set: model}, {new: true}).populate(populate || this.populateFields);
        if (select) action.select(select);
        return await action;
    };
    async delete(id) {
        let booksCount = await Book.count({performer: id});
        if (booksCount > 0) throw 403;
        await BookPerformer.remove({_id: id});
        return id;
    }
}

module.exports = BookPerformerService;