const _ = require('lodash');
const Promise = require('bluebird');
const mongoose = require('mongoose');
const User = require('../models/User');
const Entity = require('../models/Entity');
const EntityType = require('../models/EntityType');
const RatingService = require('../services/ratingService');
const CommentService = require('../services/commentService');

class EntityService {
    constructor() {
        this.defaultPopulateFields = 'createdBy updatedBy parent media';
        this.defaultQuerySort = 'type pinnedUp sortOrder -created';
        this.rating = new RatingService(Entity, this.defaultPopulateFields);
        this.comment = new CommentService(Entity, this.defaultPopulateFields);
    }

    async query(params) {
        let {query, limit, page, sort, populate} = params;

        if (_.isUndefined(limit) || _.isUndefined(page)) {
            return await Entity
                .find(query)
                .populate(populate || this.defaultPopulateFields)
                .sort(sort || this.defaultQuerySort);
        } else {
            let options = {
                page: page,
                limit: limit,
                sort: sort || this.defaultQuerySort,
                populate: populate || this.defaultPopulateFields
            };
            return await Entity.paginate(query, options);
        }
    }
    async one(params) {
        let {query, select, populate} = params;
        let action = Entity
            .findOne(query)
            .populate(populate || this.defaultPopulateFields);
        if (select) action.select(select);
        return await action;
    }
    async getParents(id = null, path = []) {
        if (!id) return path;

        let parent = await Entity.findOne({_id: id, type: EntityType.FOLDER});
        if (parent) {
            path.unshift(parent);
        }

        if (parent && parent.parent) {
            return await this.getParents(parent.parent, path);
        } else {
            return path;
        }
    }
    async getChildrenIds(parentIds = [], foundIds = []) {
        let found = await Entity.find({parent: {$in: parentIds}});

        if (found.length > 0) {
            let ids = _.map(found, '_id');
            foundIds = foundIds.concat(ids);
            return await this.getChildrenIds(ids, foundIds);
        } else {
            return foundIds;
        }
    }
    async updateParentsCounters(id) {
        if (!id) return;

        let parents = await this.getParents(id);
        parents.reverse();

        for (let i = 0; i < parents.length; i++) {
            let objectId = parents[i]._id;
            await this.updateCounters(objectId);
            let counters = await Entity.aggregate([
                {
                    $match: {
                        parent: new mongoose.Types.ObjectId(objectId), type: EntityType.FOLDER
                    }
                },
                {
                    $group: {
                        _id: null,
                        entityTotal: {$sum: '$counters.total'},
                        entityPublished: {$sum: '$counters.published'},
                        count: {$sum: 1}
                    }
                }
            ]).allowDiskUse(true);

            if (counters.length > 0) {
                let counter = counters[0];
                let entityToUpdate = await Entity.findOne({_id: parents[i]._id});
                if (entityToUpdate) {
                    entityToUpdate.counters.total += counter.entityTotal;
                    entityToUpdate.counters.published += counter.entityPublished;
                    await entityToUpdate.save();
                }
            }
        }
    }
    async updateCounters(id) {
        if (!id) return;
        let [
            totalCount,
            publishedCount,
            entity
        ] = await Promise.all([
            Entity.count({parent: id, type: {$ne: EntityType.FOLDER}}),
            Entity.count({parent: id, type: {$ne: EntityType.FOLDER}, published: true}),
            Entity.findOne({_id: id}).populate('parent')
        ]);

        if (entity) {
            entity.counters.total = totalCount;
            entity.counters.published = publishedCount;
            await entity.save();
        }
    }
    async add(params) {
        let {model, populate} = params;
        let newEntity = new Entity(model);
        let entity = await newEntity.save();
        if (entity.type != EntityType.FOLDER) {
            await this.updateParentsCounters(entity.parent);
        }
        return await entity.populate(populate || this.defaultPopulateFields);
    }
    async delete(id) {
        let entity = await Entity.findOne({_id: id});
        if (!entity) throw 404;

        let childrenIds = await this.getChildrenIds([id]);
        if (childrenIds.length > 0) throw 403;

        this.rating.delete({query: {parent: id}});
        this.comment.delete({query: {parent: id}});
        await Entity.remove({_id: id});
        await this.updateParentsCounters(entity.parent);
        return childrenIds;
    }
    async update(params) {
        let {query, model, populate} = params;
        let entity = await Entity.findOneAndUpdate(query, {$set: model}, {new: true});
        if (!entity) {
            throw 404;
        }
        await this.updateParentsCounters(entity.parent);
        return await entity.populate(populate || this.defaultPopulateFields);
    }
    async getCategories() {
        return await this.query({query: {type: EntityType.FOLDER, parent: null}, sort: 'sortOrder -created'});
    }
}

module.exports = EntityService;