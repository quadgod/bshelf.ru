const _ = require('lodash');
const path = require('path');
const uuid = require('node-uuid');
const Promise = require('bluebird');
const Book = require('../models/Book');
const Media = require('../models/Media');
const Entity = require('../models/Entity');
const fsHelper = require('../helpers/fsHelper');
const EntityType = require('../models/EntityType');
const valueHelper = require('../helpers/valueHelper');

class MediaService {
    constructor() {
        this.defaultPopulateFields = 'createdBy updatedBy parent';
        this.defaultSort = 'type -created';
    }
    async query(params) {
        let {query, limit, page, sort, populate} = params;

        if (_.isUndefined(limit) || _.isUndefined(page)) {
            return await Media
                .find(query)
                .populate(populate || this.defaultPopulateFields)
                .sort(sort || this.defaultSort);
        } else {
            let options = {
                page: valueHelper.getValueAsIntOrDefault(page, 1, 1),
                limit: valueHelper.getValueAsIntOrDefault(limit, 10, 1),
                sort: sort || this.defaultSort,
                populate: populate || this.defaultPopulateFields
            };
            return await Media.paginate(query, options);
        }
    }
    async one(params) {
        let {query, select, populate} = params;
        let action = Media.findOne(query).populate(populate || this.defaultPopulateFields);
        if (select) action.select(select);
        return await action;
    }
    async add(params) {
        let {model, populate} = params;
        let media = await (new Media(model)).save();
        return await media.populate(populate || this.defaultPopulateFields);
    }
    async delete(id) {
        let [media, subEntriesCount, booksCount, entitiesCount] = await Promise.all([
            Media.findOne({_id: id}),
            Media.count({parent: id}),
            Book.count({$or: [{cover: id}, {medias: id}]}),
            Entity.count({media: id})
        ]);

        if (!media) {
            throw 404;
        }

        if (subEntriesCount > 0 || booksCount > 0 || entitiesCount > 0) {
            throw 403;
        }

        if (media.type == EntityType.FOLDER || media.mimeType == 'youtube') {
            await Media.remove({_id: id});
            return;
        }

        await fsHelper.unlink(`./public${media.fileName}`);
        await Media.remove({_id: id});
    }
    async getParents(id = null, path = []) {
        if (!id || id == 'root') {
            id = null;
        }

        if (!id) {
            return path;
        } else {
            let parent = await Media.findOne({_id: id, type: EntityType.FOLDER});
            if (parent) {
                path.unshift(parent);
            }
            if (parent && parent.parent) {
                return await this.getParents(parent.parent, path);
            } else {
                return path;
            }
        }
    }
    async genFileTitle(parent, title) {
        let ext = path.extname(title);
        if (ext != '') {
            title = path.basename(title, ext);
        }

        let newTitle = title;
        while ((await Media.count({parent: parent, title: newTitle, type: EntityType.MEDIA_FILE})) > 0) {
            newTitle = `${title}_${uuid.v4()}`;
        }

        return newTitle;
    }
    async getChildrenIds(parentIds = [], foundIds = []) {
        let medias = await Media.find({parent: {$in: parentIds}});
        if (medias.length > 0) {
            let ids = _.map(medias, '_id');
            foundIds = foundIds.concat(ids);
            return await this.getChildrenIds(ids, foundIds);
        } else {
            return foundIds;
        }
    }
}

module.exports = MediaService;