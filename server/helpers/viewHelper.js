const _ = require('lodash');
const config = require('../../config');

class ViewHelper {
    createModel(title, req) {
        let nav = {
            audiobooks: {
                active: _.startsWith(req.url.toLowerCase(), '/audiobooks'),
                title: 'Аудиокниги'
            },
            library: {
                active: _.startsWith(req.url.toLowerCase(), '/library'),
                title: 'Библиотека'
            },
            authors: {
                active: _.startsWith(req.url.toLowerCase(), '/authors'),
                title: 'Авторы'
            },
            performers: {
                active: _.startsWith(req.url.toLowerCase(), '/performers'),
                title: 'Исполнители'
            },
            profile: {
                active: _.startsWith(req.url.toLowerCase(), '/account/profile'),
                title: 'Профиль'
            },
            favorites: {
                active: _.startsWith(req.url.toLowerCase(), '/account/favorites'),
                title: 'Избранное'
            }
        };

        let model = {
            nav: nav,
            adminEmail: config.adminEmail,
            supportEmail: config.supportEmail,
            domainName: config.domainName,
            modules: config.modules
        };

        if (title) {
            model.title = `${title}`;
        } else {
            model.title = config.domainName;
        }

        if (req.user) {
            model.user = req.user;
        }
        return model;
    }
    createPaginationModel(data) {
        let pagesArray = [];
        let page = data.page;
        let pages = data.pages;

        if (page < 1) {
            page = 1;
        }

        if (page > pages) {
            page = pages;
        }

        let paginator = {
            page: page,
            pages: pages,
            limit: data.limit
        };

        if (pages == 1) {
            paginator.items = pagesArray;
            return paginator;
        }



        for (let i = page - 3; i < page; i++) {
            if (i >= 1) pagesArray.push(i);
        }

        for (let i = page; i < page + 4; i++) {
            if (i <= pages) pagesArray.push(i);
        }

        if (pages > 7 && pagesArray.length < 7) {
            if (pagesArray[pagesArray.length - 1] != pages) {
                for (let i = (pagesArray[pagesArray.length - 1] + 1); i < pages && pagesArray.length < 7; i++) {
                    pagesArray.push(i);
                }
            }

            if (pagesArray.length < 7) {
                let startArray = [];
                for (let i = pagesArray[0] - (7 - pagesArray.length); i <= pagesArray[0]; i++) {
                    if (i > 1 && (startArray.length + pagesArray.length) < 7) {
                        startArray.push(i);
                    }
                }
                pagesArray = [].concat(startArray, pagesArray);
            }
        }

        paginator.items = pagesArray.map((p) => {
            return {
                num: p,
                active: p == page
            }
        });

        return paginator;
    }
}

module.exports = new ViewHelper();