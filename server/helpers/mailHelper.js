const config = require('../../config');
const nodemailer = require('nodemailer');

class MailHelper {
    constructor() {
        this.mailer = nodemailer.createTransport({
            host: config.smtpHost,
            port: config.smtpPort,
            secure: config.smtpSSL,
            auth: {
                user: config.smtpUser,
                pass: config.smtpPass
            }
        });
    }

    sendRegistrationEmail(user, req) {
        let token = user.activateAccountToken.token;
        let link = `${req.protocol}://${req.headers.host}/account/registration/activate?userid=${user._id}&token=${token}`;
        let subject = `Подтверждение регистрации ${req.headers.host}`;
        let html =
            `<div style="font-size: 14px">
                <p>Привет,</p>
                <p>
                    Пожалуйста перейдите по этой <a href="${link}">ссылке</a> для 
                    подтверждения регистрации на сайте ${req.headers.host}.
                </p>
            </div>`;

        return this.mailer.sendMail({
            to: user.email,
            subject: subject,
            html: html
        });
    }

    sendRestorePasswordEmail(user, req) {
        let subject = `Сброс пароля ${req.headers.host}`;
        let link = `${req.protocol}://${req.headers.host}/account/restore-password/${user._id}/${user.resetPasswordToken.token}`;
        let html =
            `<div style="font-size: 14px">
            <p>Привет,</p>
            <p>
                Пожалуйста перейдите по этой <a href="${link}">ссылке</a> для 
                сброса пароля ${req.headers.host}.
            </p>
        </div>`;
        return this.mailer.sendMail({
            to: user.email,
            subject: subject,
            html: html,
        });
    }
}

module.exports = new MailHelper();