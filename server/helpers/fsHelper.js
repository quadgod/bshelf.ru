const fs = require('fs');
const path = require('path');
const Promise = require('bluebird');

class FSHelper {
    constructor() {
        this.__mkdir = Promise.promisify(fs.mkdir);
        this.__unlink = Promise.promisify(fs.unlink);
        this.__mp3Duration = Promise.promisify(require('mp3-duration'));
        this.__readdir = Promise.promisify(fs.readdir);
        this.__access = Promise.promisify(fs.access);
        this.__stat = Promise.promisify(fs.stat);
    }

    ensureExists(prefix, _path = [], ensureRoot = true) {
        if (ensureRoot) {
            prefix = path.join(appRoot, prefix);
            console.info(`Ensure path: ${prefix}`);
        }

        let mask = '0777';
        let pathToEnsure = prefix;

        if (!ensureRoot) {
            if (_path.length > 0) {
                pathToEnsure = path.join(pathToEnsure, _path[0]);
                _path.shift();
                console.info(`Ensure path: ${pathToEnsure}`);
            }
        }

        return this.__mkdir(pathToEnsure, mask).then(() => {
            if (_path.length > 0) {
                return this.ensureExists(pathToEnsure, _path, false);
            } else {
                return Promise.resolve(pathToEnsure);
            }
        }).catch((err) => {
            if (err.code == 'EEXIST') {
                if (_path.length > 0) {
                    return this.ensureExists(pathToEnsure, _path, false);
                } else {
                    return Promise.resolve(pathToEnsure);
                }
            } else {
                return Promise.reject(err);
            }
        });
    }
    unlink(relativePath) {
        let _path = path.join(appRoot, relativePath);
        return this.__unlink(_path).then(() => {
            return Promise.resolve(_path);
        }).catch((err) => {
            if (err && err.code != 'ENOENT') {
                return Promise.reject(err);
            } else {
                return Promise.resolve(_path);
            }
        });
    }
    writeFile(relativePath, buffer) {
        let _path = path.join(appRoot, relativePath);
        return new Promise((resolve, reject) => {
            let wstream = fs.createWriteStream(_path);
            wstream.write(buffer, (err) => {
                if (err) {
                    wstream.end();
                    return reject(err);
                } else {
                    wstream.end();
                    return resolve(_path);
                }
            });
        });
    }
    async readdir(relativePath) {
        let _path = path.join(appRoot, relativePath);
        return await this.__readdir(_path);
    }
    async fileExists(relativePath) {
        let _path = path.join(appRoot, relativePath);
        try {
            await this.__access(_path, fs.constants.R_OK | fs.constants.W_OK);
            return true;
        } catch(err) {
            return false;
        }
    }
    async fileSize(relativePath) {
        let _path = path.join(appRoot, relativePath);
        let stat = await this.__stat(_path);
        return stat.size;
    }
    copyFile(source, target) {
        let _source = path.join(appRoot, source);
        let _target = path.join(appRoot, target);
        return new Promise((resolve, reject) => {
            let rd = fs.createReadStream(_source);
            rd.on('error', rejectCleanup);
            let wr = fs.createWriteStream(_target);
            wr.on('error', rejectCleanup);
            function rejectCleanup() {
                rd.destroy();
                wr.end();
                reject(err);
            }
            wr.on('finish', resolve);
            rd.pipe(wr);
        });
    }
    mp3Duration(relativePath) {
        return this.__mp3Duration(path.join(appRoot, relativePath)).then((duration) => {
            return Promise.resolve(duration);
        }).catch((err) => {
            console.error(err);
            return Promise.resolve(0);
        });
    }
}

module.exports = new FSHelper();