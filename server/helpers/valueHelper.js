const _ = require('lodash');

class ValueHelper {
    getValueAsIntOrDefault(value, defaultValue, min, max) {
        let temp = null;
        try {
            temp = parseInt(value, 10);
        } catch (err) {
            temp = defaultValue;
        }

        if (!temp || _.isNaN(temp)) {
            return defaultValue;
        }

        if (min && temp < min) {
            return min;
        }

        if (max && temp > max) {
            return max;
        }

        return temp;
    }

    getValueAsStringOrNull(value) {
        if (!_.isUndefined(value) && !_.isNull(value) && !_.isEmpty(_.trim(value.toString()))) {
            return _.trim(value.toString()).toString();
        } else {
            return null;
        }
    }

    isNullOrWhiteSpace(value) {
        return (_.isUndefined(value) || _.isNull(value) || !_.isString(value) || _.isEmpty(_.trim(value)));
    }
}

module.exports = new ValueHelper();