module.exports = {
    ROLE: {
        ADMIN: 'admin',
        USER: 'user',
        MODERATOR: 'moderator'
    },
    ROLES: ['admin', 'moderator', 'user'],
    STRINGS: {
        MESSAGES: {
            ratingSuccess: 'Спасибо за вашу оценку!',
            saveSuccess: 'Данные успешно обновлены.',
            changePasswordSuccess: 'Пароль успешно изменен.'
        },
        ERRORS: {
            unauthorizedError: 'Действие доступно только авторизованным пользователям.',
            minLengthError: 'Значение поля `{PATH}` (`{VALUE}`) короче чем минимально допустимая длинна ({MINLENGTH}).',
            maxLengthError: 'Значение поля `{PATH}` (`{VALUE}`) превышает максимально допустимую длинну ({MAXLENGTH}).',
            requiredError: '`{PATH}` обязательное поле.',
            userNotFoundInvalidLogin: 'Пользователь не найден, неверный логин:',
            userNotFound: 'Пользователь не найден.',
            invalidLogin: 'Неверный логин.',
            unsupportedAvatarFormat: 'Неверный формат аватара. Разрешены только jpeg или png.',
            maxAvatarFileSize5mb: 'Максимально допустимый размер файла 5 мегабайт.',
            unableToDeleteFolderHasChild: 'Невозможно удалить - содержит дочерние элементы',
            unableToDeleteHasRefs: 'Невозможно удалить - элемент используется',
            elementNotFound: 'Элемент не найден',
            notAuthorized: 'Действие доступно только авторизованным пользователям',
            badRequest: 'Ошибка запроса',
            internalServerError: 'Внутренняя ошибка сервера',
            entityNotFound: 'Объект не найден',
            entityDocumentTitleConflict: 'Документ с такой ссылкой уже существует',
            entityFolderTitleConflict: 'Документ с такой ссылкой уже существует',
            genreConflict: 'Жанр с такой ссылкой уже существует',
            seriesConflict: 'Серия с такой ссылкой уже существует',
            authorFullNameConflict: 'Автор с таким именем уже существует',
            performerFullNameConflict: 'Исполнитель с таким именем уже существует',
            bookTitleConflict: 'Книга с такой ссылкой уже существует',
            mediaFolderConflict: 'Папка с таким именем уже существует',
            youtubeConflictError: 'Такая ссылка уже существует',
            userConflictError: 'Пользователь с таким псевдонимом или email уже существует',
            commentNotFound: 'Комментарий не найден',
            commentAlreadyVoted: 'Вы уже голосовали за этот комментарий',
            commentForbiddenDelete: 'Вы можете удалять только свои комментарии',
            commentForbiddenUpdate: 'Вы можете обновлять только свои комментарии',
            unsupportedFileTypeError: 'Неверный формат файла. Поддерживаются только: image/*, audio/*, video/*, pdf, txt, fb2 или epub.',
            minPasswordLength: 'Минимальная длинная пароля 6 символов',
            badOldPassword: 'Текущий пароль неверен.',
            validationError: 'Ошибка валидации',
            userIsBlocked: 'Пользователь заблокирован'
        }
    }
};