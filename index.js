const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const config = require('./config');
const boom = require('express-boom');
const path = require('path');

let session = require('express-session');
let MongoDBStore = require('connect-mongodb-session')(session);
global.appRoot = path.resolve(__dirname);

require('./server/mongo')().then(() => {
    const app = express();
    let moment = require('moment');
    moment.locale('ru');
    app.locals.moment = moment;
    app.locals.md = require('markdown-it')();
    app.locals._ = require('lodash');

    app.enable('trust proxy', 1);
    app.set('views', './views');
    app.set('view engine', 'pug');

    app.use(boom());
    app.use(cors());
    app.use(cookieParser());
    app.use(bodyParser.json({ limit: '500mb' }));
    app.use(session({
        secret: config.sessionKey,
        cookie: {
            maxAge: 1000 * 60 * 60 * 24 * 7 // 1 week
        },
        store: new MongoDBStore({
            uri: config.mongoUrl,
            collection: config.sessionCollection
        }),
        resave: true,
        saveUninitialized: true
    }));

    let passport = require('./server/passport');
    app.use(passport.initialize());
    app.use(passport.session());

    require('./server/router')(app);

    app.use(function(req, res, next) {
        return res.status(404).render('errors/404', {disableFooter: true, disableHeader: true});
    });

    let server = app.listen(config.port, function () {
        console.log('\nServer ready on port %d\n', server.address().port); 
    });
}).catch((err) => {
    console.error(err);
});