import { getParameterByName } from './urlHelper';

const goToLoginAndSaveReturnUrl = () => {
    localStorage.setItem('returnUrl', window.location.href);
    window.location.href = `/account/login?returnUrl=${window.location.href}`;
};

const logoutAndReturn = () => {
    window.location.href = `/account/logout?returnUrl=${window.location.href}`;
};

const goToReturnUrl = () => {
    let returnUrl = getParameterByName('returnUrl', window.location.href);
    if (!returnUrl) {
        returnUrl = localStorage.getItem('returnUrl');
    }

    if (!returnUrl) {
        returnUrl = '/';
    }
    localStorage.removeItem('returnUrl');
    window.location.href = returnUrl;
};

export { goToLoginAndSaveReturnUrl, goToReturnUrl, logoutAndReturn };