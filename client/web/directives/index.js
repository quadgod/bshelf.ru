import angular from 'angular';
import * as shareSocialButton from './shareSocialButton';
import * as fileChange from '../../dashboard/directives/fileChangeDirective';

let directives = angular.module('app.directives', []);
directives.directive(fileChange.name, fileChange.directive);
directives.directive(shareSocialButton.name, shareSocialButton.directive);
