const name = 'shareSocialButton';
const directive = ['$window', function($window) {
    return {
        restrict: 'E',
        scope: {
            type: '<',
            cssClass: '<',
            image: '<',
            model: '<'
        },
        template: '<img ng-src="">',
        link: function(scope, element, attributes) {
            if (scope.type == 'vk') {
                let options = {
                    image: 'https://www.google.ru/images/nav_logo242.png',
                    title: encodeURI(scope.model.title),
                    //description: scope.model.shortBody,
                    noparse: false
                };
                element.append(VK.Share.button(options, {
                    type: 'custom',
                    text: `<img src="${scope.image}" class="${scope.cssClass}" />`
                }));
            }
        }
    };
}];

export { name, directive };