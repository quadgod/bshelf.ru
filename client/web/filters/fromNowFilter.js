import moment from 'moment';
moment.locale('ru');

const name = 'fromNow';
const filter = [function () {
    return function (date) {
        return moment(date).fromNow();
    };
}];

export { name, filter };