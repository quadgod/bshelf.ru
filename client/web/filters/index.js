import angular from 'angular';
import * as FromNowFilter from './fromNowFilter';

let filters = angular.module('app.filters', []);
filters.filter(FromNowFilter.name, FromNowFilter.filter);