import angular from 'angular';
import 'jquery';
import 'angular-ui-bootstrap';
import 'angular-resource';
import 'angular-ui-notification';
import 'angular-file-upload';
import 'angular-sanitize';
import 'lodash';
import 'bootstrap-sass';
import 'angular-cookies';
import './components';
import './services';
import './directives';
import './filters';
import config from './app.config';
import run from './app.run';

let injectables = [
    'ngResource',
    'ui.bootstrap',
    'ui-notification',
    'angularFileUpload',
    'ngSanitize',
    'ngCookies',
    'app.components',
    'app.services',
    'app.directives',
    'app.filters'
];

let app = angular.module('app', injectables);
app.config(config);
app.run(run);

export default app;