const name = 'activityService';
const service = ['$resource', function ($resource) {
    return $resource('/api/activity/:activity/:for/:object', {}, {
        'add': {
            method: 'POST',
            params: {
                activity: '@activity',
                for: '@for',
                object: '@object'
            }
        },
        'query': {
            method: 'GET',
            params: {
                activity: '@activity',
                for: '@for',
                object: '@object'
            },
            isArray: true
        },
        'delete': {
            method: 'DELETE',
            params: {
                activity: '@activity',
                for: '@for',
                object: '@object',
                activityId: '@activityId'
            },
            url: '/api/activity/:activity/:for/:object/:activityId'
        },
        'update': {
            method: 'PUT',
            params: {
                activity: '@activity',
                for: '@for',
                object: '@object',
                activityId: '@activityId'
            },
            url: '/api/activity/:activity/:for/:object/:activityId'
        }
    });
}];

export { name, service };