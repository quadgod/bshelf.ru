import angular from 'angular';
import * as ActivityService from './activityService';
import * as UserService from '../../dashboard/services/userService';
import * as BookService from '../../dashboard/services/bookService';

let services = angular.module('app.services', []);
services.service(UserService.name, UserService.service);
services.service(BookService.name, BookService.service);
services.service(ActivityService.name, ActivityService.service);