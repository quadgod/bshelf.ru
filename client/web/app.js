'use strict';

import '../../node_modules/bootstrap-sass/assets/stylesheets/_bootstrap.scss';
import '../../node_modules/font-awesome/scss/font-awesome.scss';
import './sass/style.scss';

import './app.module';
import angular from 'angular';

angular.element(document).ready(function() {
    angular.bootstrap(document, ['app']);
});