const name = 'restorePasswordForm';
const component = {
    template: require('raw!./restorePasswordForm.html'),
    controller: ['Notification', 'userService', function (Notification, userService) {
        this.busy = false;
        this.sent = false;
        this.data = {email: ''};
        this.submit = (form) => {
            form.$setSubmitted();
            if (form.$valid && !this.busy) {
                this.busy = true;
                userService.restorePassword(this.data).$promise.then((data) => {
                    this.sent = data.sent;
                }).catch((err) => {
                    this.busy = false;
                    if (err.data.message == 'user' || err.status == 400) {
                        Notification.error('Пользователь с таким email не существует.');
                    } else {
                        Notification.error('Внутренняя ошибка сервера.');
                    }
                });
            }
        };
    }]
};

export { name, component };