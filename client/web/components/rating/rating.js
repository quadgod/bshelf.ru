import * as consts from '../../../../consts';

const name = 'rating';
const component = {
    bindings: {
        for: '<',
        object: '<',
        rating: '<',
        user: '<'
    },
    template: require('raw!./rating.html'),
    controller: ['activityService', 'Notification', function (activityService, $notification) {
        this.busy = false;
        this.displayRating = angular.copy(this.rating);
        this.vote = (vote) => {
            if (!this.busy) {
                this.busy = true;
                activityService.add({
                    activity: 'rating',
                    for: this.for,
                    object: this.object,
                    value: vote
                }).$promise.then((result) => {
                    this.displayRating = result.object.rating;
                    this.busy = false;
                    $notification.info(consts.STRINGS.MESSAGES.ratingSuccess);
                }).catch((err) => {
                    if (err.status == 401) {
                        $notification.error(consts.STRINGS.ERRORS.unauthorizedError);
                    } else if (err.status == 404) {
                        $notification.error(consts.STRINGS.ERRORS[`${this.type}NotFound`]);
                    } else {
                        $notification.error(consts.STRINGS.ERRORS.internalServerError);
                    }
                    this.busy = false;
                });
            }
        };
    }]
};

export { name, component };