import { goToReturnUrl } from '../../helpers/authHelper';

const name = 'loginForm';
const component = {
    template: require('raw!./loginForm.html'),
    controller: ['Notification', 'userService', function (Notification, userService) {
        this.busy = false;
        this.data = {
            email: '',
            password: ''
        };
        this.loginVK = () => {
            window.location.href = '/account/vkontakte';
        };
        this.submit = (form) => {
            form.$setSubmitted();
            if (form.$valid && !this.busy) {
                this.busy = true;
                userService.login(this.data).$promise.then(() => {
                    goToReturnUrl();
                }).catch((err) => {
                    this.busy = false;
                    Notification.error('Неверный email или пароль.');
                });
            }
        };
    }]
};

export { name, component };