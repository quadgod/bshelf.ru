import angular from 'angular';
import * as Rating from './rating/rating';
import * as Comments from './comments/comments';
import * as LoginForm from './loginForm/loginForm';
import * as SearchBar from './searchBar/searchBar';
import * as AudioPlayer from './audioPlayer/audioPlayer';
import * as ProfileEditor from './profileEditor/profileEditor';
import * as RegistrationForm from './registrationForm/registrationForm';
import * as ResetPasswordForm from './resetPasswordForm/resetPasswordForm';
import * as RestorePasswordForm from './restorePasswordForm/restorePasswordForm';

let components = angular.module('app.components', []);
components.component(Rating.name, Rating.component);
components.component(Comments.name, Comments.component);
components.component(SearchBar.name, SearchBar.component);
components.component(LoginForm.name, LoginForm.component);
components.component(AudioPlayer.name, AudioPlayer.component);
components.component(ProfileEditor.name, ProfileEditor.component);
components.component(RegistrationForm.name, RegistrationForm.component);
components.component(ResetPasswordForm.name, ResetPasswordForm.component);
components.component(RestorePasswordForm.name, RestorePasswordForm.component);