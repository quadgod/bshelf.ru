const name = 'registrationForm';
const component = {
    template: require('raw!./registrationForm.html'),
    controller: ['$scope', '$rootScope', 'userService', 'Notification', function ($scope, $rootScope, userService, Notification) {
        this.comparePasswords = (form) => {
            if (form.repeatPassword.$error.hasOwnProperty('cmppasswords')) {
                if (this.data.password != this.data.repeatPassword) {
                    form.repeatPassword.$setValidity('cmppasswords', false);
                } else {
                    form.repeatPassword.$setValidity('cmppasswords', true);
                }
            } else {
                if (form.password.$valid && form.repeatPassword.$valid && form.password.$dirty && form.repeatPassword.$dirty && this.data.password != this.data.repeatPassword) {
                    form.repeatPassword.$setValidity('cmppasswords', false);
                } else {
                    form.repeatPassword.$setValidity('cmppasswords', true);
                }
            }
        };
        this.submit = (form) => {
            this.comparePasswords(form);
            if (form.$valid && !this.busy) {
                this.busy = true;
                let payload = {...this.data};
                delete payload.repeatPassword;
                userService.registration(payload).$promise.then(() => {
                    this.busy = false;
                    this.regComplete = true;
                }).catch((err) => {
                    let errorType = err.data.message || err.message || err.toString();
                    switch (errorType) {
                        case 'email':
                            Notification.error('Пользователь с таким Email уже зарегистрирован.');
                            form.email.$setValidity('conflict', false);
                            break;
                        case 'nickname':
                            Notification.error('Псевдоним занят другим пользователем.');
                            form.nickname.$setValidity('conflict', false);
                            break;
                        default:
                            Notification.error('Внутренняя ошибка сервера. Обратитесь к администратору.');
                            break;
                    }
                    this.busy = false;
                });
            }
        };
        this.regComplete = false;
        this.busy = false;
        this.data = {
            nickname: '',
            email: '',
            password: '',
            repeatPassword: ''
        };
    }]
};

export { name, component };