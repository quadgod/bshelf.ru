import { goToLoginAndSaveReturnUrl } from '../../helpers/authHelper';
import * as consts from '../../../../consts';

const name = 'comments';
const component = {
    bindings: {
        for: '<',
        object: '<',
        user: '<'
    },
    template: require('raw!./comments.html'),
    controller: ['activityService', 'Notification', function (activityService, $notification) {
        this.busy = false;
        this.comments = [];
        this.login = () => {
            goToLoginAndSaveReturnUrl();
        };
        this.submit = (form) => {
            if (form.$valid && !this.busy) {
                this.busy = true;
                activityService.add({
                    activity: 'comment',
                    for: this.for,
                    object: this.object,
                    comment: this.body
                }).$promise.then(() => {
                    delete this.body;
                    form.$setPristine();
                    form.$setUntouched();
                    this.load();
                }).catch((err) => {
                    if (err.status == 401) {
                        $notification.error(consts.STRINGS.ERRORS.unauthorizedError);
                    } else {
                        $notification.error(consts.STRINGS.ERRORS.internalServerError);
                    }
                    this.busy = false;
                });
            }
        };
        this.edit = (comment) => {
            this.comments.forEach((c) => {
                if (c._id == comment._id) {
                    c.editMode = !c.editMode;
                    c.editBody = angular.copy(c.body);
                    c.editSystemBody = angular.copy(c.systemBody);
                } else {
                    c.editMode = false;
                }
            });
        };
        this.update = (form, comment) => {
            if (form.$valid && !this.busy) {
                this.busy = true;
                let params = {
                    activity: 'comment',
                    for: this.for,
                    object: this.object,
                    activityId: comment._id,
                    body: comment.editBody,
                    systemBody: comment.editSystemBody
                };
                activityService.update(params).$promise.then((result) => {
                    comment.body = result.body;
                    comment.systemBody = result.systemBody;
                    comment.editMode = false;
                    this.busy = false;
                }).catch((err) => {
                    if (err.status == 404) {
                        $notification.error(consts.STRINGS.ERRORS.commentNotFound);
                    } else {
                        $notification.error(consts.STRINGS.ERRORS.internalServerError);
                    }
                    this.busy = false;
                });
            }
        };
        this.delete = (comment) => {
            activityService.delete({
                activity: 'comment',
                for: this.for,
                object: this.object,
                activityId: comment._id
            }).$promise.then(() => {
                this.load();
            }).catch((err) => {
                if (err.status == 401) {
                    $notification.error(consts.STRINGS.ERRORS.unauthorizedError);
                } else if (err.status == 403) {
                    $notification.error(consts.STRINGS.ERRORS.commentForbiddenDelete);
                } else if (err.status == 404) {
                    $notification.error(consts.STRINGS.ERRORS.commentNotFound);
                } else {
                    $notification.error(consts.STRINGS.ERRORS.internalServerError);
                }
            });
        };

        this.load = () => {
            this.busy = true;
            activityService.query({
                activity: 'comment',
                for: this.for,
                object: this.object
            }).$promise.then((result) => {
                result.forEach(() => {
                    editMode: false
                });
                this.comments = result;
                this.busy = false;
            }).catch(() => {
                $notification.error(consts.STRINGS.ERRORS.internalServerError);
                this.busy = false;
            });
        };
        this.load();
    }]
};

export { name, component };