import './searchBar.scss';

import _ from 'lodash';
const name = 'searchBar';
const component = {
    bindings: {
        url: '<',
        query: '<',
        tags: '<'
    },
    template: require('raw!./searchBar.html'),
    controller: [function () {
        this.search = (event) => {
            event.preventDefault();
            if (!_.isUndefined(this.query) && !_.isNull(this.query) && !_.isEmpty(_.trim(this.query))) {
                window.location.search = `?title=${_.trim(this.query)}`;
            } else {
                window.location.search = ``;
            }
        };
        this.removeTag = (event) => {
            window.location.search = ``;
        };
    }]
};

export { name, component };