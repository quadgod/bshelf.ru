import angular from 'angular';
import * as consts from '../../../../consts';

const name = 'profileEditor';
const component = {
    bindings: {
        user: '<'
    },
    template: require('raw!./profileEditor.html'),
    controller: ['Notification', '$rootScope', 'userService', function ($notification, $rootScope, userService) {
        this.busy = false;

        this.info = {
            model: angular.copy(this.user),
            updateInfo: (form) => {
                if (!this.busy && form.$valid) {
                    this.busy = true;

                    userService.update(this.info.model).$promise.then(() => {
                        this.busy = false;
                        $notification.success(consts.STRINGS.MESSAGES.saveSuccess);
                        $rootScope.$emit('profileUpdated', this.info.model);
                    }).catch((err) => {
                        if (err.status == 409) {
                            $notification.error(consts.STRINGS.ERRORS.userConflictError);
                        } else if (err.status == 400) {
                            $notification.error(consts.STRINGS.ERRORS.validationError);
                        } else {
                            $notification.error(consts.STRINGS.ERRORS.internalServerError);
                        }
                        this.busy = false;
                    });
                }
            },
            avatarChange: (files) => {
                if (files && files.length > 0) {
                    if (['image/jpeg', 'image/png'].indexOf(files[0].type) == -1) {
                        $notification.error(consts.STRINGS.ERRORS.unsupportedAvatarFormat);
                    } else {
                        var formData = new FormData();
                        formData.append('_id', this.user._id);
                        formData.append('avatar', files[0]);
                        this.busy = true;
                        $('#avatar').val(null);

                        userService.uploadAvatar(formData).$promise.then((response) => {
                            this.info.model.avatar = response.avatar;
                            this.busy = false;
                            $rootScope.$emit('profileUpdated', this.info.model);
                        }).catch((err) => {
                            if (err == 404) {
                                $notification.error(consts.STRINGS.ERRORS.userNotFound);
                            } if (err.status == 415) {
                                $notification.error(err.data && err.data.message || err.message || consts.STRINGS.ERRORS.unsupportedAvatarFormat);
                            } else {
                                $notification.error(consts.STRINGS.ERRORS.internalServerError);
                            }
                            this.busy = false;
                        });
                    }
                }
            },
            uploadAvatar: () => {
                if (!this.busy) {
                    $('#avatar').trigger('click');
                }
            },
            deleteAvatar: () => {
                if (!this.busy) {
                    this.busy = true;
                    userService.deleteAvatar({id: this.user._id}).$promise.then((response) => {
                        this.info.model.avatar = response.avatar;
                        this.busy = false;
                        $rootScope.$emit('profileUpdated', this.info.model);
                    }).catch((err) => {
                        if (err == 404) {
                            $notification.error(consts.STRINGS.ERRORS.userNotFound);
                        } else {
                            $notification.error(consts.STRINGS.ERRORS.internalServerError);
                        }
                        this.busy = false;
                    });
                }
            }
        };
        this.password = {
            model: {
                oldPassword: null,
                password: null,
                confirmPassword: null
            },
            changePassword: (form) => {
                if (this.password.model.password != this.password.model.confirmPassword) {
                    form.confirmPassword.$setValidity('notEqual', false);
                } else {
                    form.confirmPassword.$setValidity('notEqual', true);
                }

                if (!this.busy && form.$valid) {
                    this.busy = true;
                    userService.resetPassword({...this.password.model, ...{_id: this.user._id}}).$promise.then(() => {
                        $notification.success(consts.STRINGS.MESSAGES.changePasswordSuccess);
                        this.password.model.oldPassword = null;
                        this.password.model.password = null;
                        this.password.model.confirmPassword = null;
                        form.$setPristine();
                        form.$setUntouched();
                        this.busy = false;
                    }).catch((err) => {
                        if (err.status == 400) {
                            $notification.error(consts.STRINGS.ERRORS.badOldPassword);
                        } else if (err.status == 404) {
                            $notification.error(consts.STRINGS.ERRORS.userNotFound);
                        } else {
                            $notification.error(consts.STRINGS.ERRORS.internalServerError);
                        }
                        this.busy = false;
                    });
                }
            }
        };

    }]
};

export { name, component };