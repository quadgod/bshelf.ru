import * as consts from '../../../../consts';

const name = 'resetPasswordForm';
const component = {
    template: require('raw!./resetPasswordForm.html'),
    bindings: {
        token: '<',
        userid: '<'
    },
    controller: ['Notification', 'userService', function ($notification, userService) {
        this.busy = false;
        this.done = false;
        this.data = {
            password: '',
            repeatPassword: ''
        };
        this.comparePasswords = (form) => {
            if (form.repeatPassword.$error.hasOwnProperty('cmppasswords')) {
                if (this.data.password != this.data.repeatPassword) {
                    form.repeatPassword.$setValidity('cmppasswords', false);
                } else {
                    form.repeatPassword.$setValidity('cmppasswords', true);
                }
            } else {
                if (form.password.$valid && form.repeatPassword.$valid && form.password.$dirty && form.repeatPassword.$dirty && this.data.password != this.data.repeatPassword) {
                    form.repeatPassword.$setValidity('cmppasswords', false);
                } else {
                    form.repeatPassword.$setValidity('cmppasswords', true);
                }
            }
        };
        this.submit = (form) => {
            this.comparePasswords(form);
            if (form.$valid && !this.busy) {
                this.busy = true;
                let payload = {...this.data, ...{userId: this.userid, token: this.token}};
                delete payload.repeatPassword;
                userService.resetPasswordByToken(payload).$promise.then((data) => {
                    this.busy = false;
                    this.done = data.done;
                }).catch((err) => {
                    $notification.error(consts.STRINGS.ERRORS.internalServerError);
                    this.busy = false;
                });
            }
        };
    }]
};

export { name, component };