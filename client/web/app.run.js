import { logoutAndReturn } from './helpers/authHelper';

export default ['$rootScope', function ($rootScope) {
    $rootScope.logout = () => {
        logoutAndReturn();
    };
}];