import _ from 'lodash';
import * as EntityType from '../../server/models/EntityType';

export default [
    '$urlRouterProvider', '$stateProvider', '$sceDelegateProvider', 'NotificationProvider',
    function ($urlRouterProvider, $stateProvider, $sceDelegateProvider, NotificationProvider) {
        $sceDelegateProvider.resourceUrlWhitelist([
            'self',
            'https://www.youtube.com/**']);
        NotificationProvider.setOptions({
            delay: 5000,
            startTop: 20,
            startRight: 10,
            verticalSpacing: 20,
            horizontalSpacing: 20,
            positionX: 'right',
            positionY: 'top'
        });
        $urlRouterProvider.when('', '/document/root').otherwise('/document/root');
        $stateProvider.state('dashboard', {
            abstract: true,
            url: '/',
            template: require('raw!./root.html'),
            controller: ['$rootScope', 'currentUser', function ($rootScope, currentUser) {
                $rootScope.currentUser = currentUser;
            }],
            resolve: {
                currentUser: ['userService', function (userService) {
                    return userService.current().$promise;
                }]
            }
        });
        $stateProvider.state('dashboard.document', {
            url: 'document/:parent?page?limit?title',
            template: `
                <ui-view>
                    <document-list  url="'dashboard.document'"
                                    entities="entities"
                                    breadcrumbs="breadcrumbs">
                    </document-list>
                </ui-view>
            `,
            controller: ['$scope', 'entities', 'breadcrumbs', function ($scope, entities, breadcrumbs) {
                $scope.entities = entities;
                $scope.breadcrumbs = breadcrumbs;
            }],
            resolve: {
                entities: ['entityService', '$stateParams', function (entityService, $stateParams) {
                    let query = {
                        page: $stateParams.page || 1,
                        limit: $stateParams.limit || 10,
                        parent: $stateParams.parent || 'root',
                        title: $stateParams.title || null
                    };

                    return entityService.query(query).$promise;
                }],
                breadcrumbs: ['entityService', '$stateParams', function (entityService, $stateParams) {
                    return entityService.breadcrumbs({ parent: $stateParams.parent || 'root' }).$promise;
                }]
            }
        });
        $stateProvider.state('dashboard.document.editor', {
            url: '/edit/:id',
            template: `<document-editor breadcrumbs="breadcrumbs" document="document"></document-editor>`,
            controller: ['$scope', '$stateParams', 'breadcrumbs', 'document', function ($scope, $stateParams, breadcrumbs, document) {
                $scope.breadcrumbs = breadcrumbs;
                $scope.document = document || {
                    url: '',
                    type: EntityType.DOCUMENT,
                    title: '',
                    shortBody: '',
                    body: '',
                    sortOrder: 1,
                    published: false,
                    pinnedUp: false,
                    media: null,
                    tags: [],
                    parent: $stateParams.parent || 'root'
                };
            }],
            resolve: {
                document: ['entityService', '$stateParams', function (entityService, $stateParams) {
                    if (_.isUndefined($stateParams.id) || _.isNull($stateParams.id) || _.isEmpty($stateParams.id)) {
                        return Promise.resolve(null);
                    } else {
                        return entityService.getById({id: $stateParams.id}).$promise;
                    }
                }],
                breadcrumbs: ['entityService', '$stateParams', function (entityService, $stateParams) {
                    return entityService.breadcrumbs({parent: $stateParams.parent || 'root'}).$promise;
                }]
            }
        });
        $stateProvider.state('dashboard.media', {
            url: 'media/:parent?page?limit?title',
            template: `
                <ui-view>
                    <media-list url="'dashboard.media'" medias="medias" breadcrumbs="breadcrumbs"></media-list>
                </ui-view>
            `,
            controller: ['$scope', 'medias', 'breadcrumbs', function ($scope, medias, breadcrumbs) {
                $scope.medias = medias;
                $scope.breadcrumbs = breadcrumbs;
            }],
            resolve: {
                medias: ['mediaService', '$stateParams', function (mediaService, $stateParams) {
                    let query = {
                        page: $stateParams.page || 1,
                        limit: $stateParams.limit || 10,
                        parent: $stateParams.parent || 'root',
                        title: $stateParams.title || null
                    };

                    return mediaService.query(query).$promise;
                }],
                breadcrumbs: ['mediaService', '$stateParams', function (mediaService, $stateParams) {
                    return mediaService.breadcrumbs({parent: $stateParams.parent || 'root'}).$promise;
                }]
            }
        });
        $stateProvider.state('dashboard.book', {
            url: 'book/:parent?page?limit?title',
            template: `
                <ui-view>
                    <book-list books="books" breadcrumbs="breadcrumbs" url="'dashboard.book'"></book-list>
                </ui-view>
            `,
            controller: ['$scope', 'books', 'breadcrumbs', function ($scope, books, breadcrumbs) {
                $scope.books = books;
                $scope.breadcrumbs = breadcrumbs;
            }],
            resolve: {
                books: ['bookService', '$stateParams', function (bookService, $stateParams) {
                    let query = {
                        page: $stateParams.page || 1,
                        limit: $stateParams.limit || 10,
                        parent: $stateParams.parent || 'root',
                        title: $stateParams.title || null
                    };
                    return bookService.query(query).$promise;
                }],
                breadcrumbs: ['bookService', '$stateParams', function (bookService, $stateParams) {
                    return bookService.breadcrumbs({parent: $stateParams.parent || 'root'}).$promise;
                }]
            }
        });
        $stateProvider.state('dashboard.book.editor', {
            url: '/edit/:id',
            template: '<book-editor book="book" breadcrumbs="breadcrumbs"></book-editor>',
            controller: ['$scope', 'book', 'breadcrumbs', function ($scope, book, breadcrumbs) {
                $scope.book = book;
                $scope.breadcrumbs = breadcrumbs;
            }],
            resolve: {
                book: ['bookService', '$stateParams', function (bookService, $stateParams) {
                    if (_.isUndefined($stateParams.id) || _.isNull($stateParams.id) || _.isEmpty($stateParams.id)) {
                        return Promise.resolve(null);
                    } else {
                        return bookService.getById({id: $stateParams.id}).$promise;
                    }
                }],
                breadcrumbs: ['bookService', '$stateParams', function (bookService, $stateParams) {
                    return bookService.breadcrumbs({parent: $stateParams.parent || 'root' }).$promise;
                }]
            }
        });
        $stateProvider.state('dashboard.bookAuthor', {
            url: 'book-author?page?limit?fullName',
            template: `
                <ui-view>
                    <book-author-list authors="authors" url="url"></book-author-list>
                </ui-view>
            `,
            controller: ['$scope', '$state', 'authors', function ($scope, $state, authors) {
                $scope.authors = authors;
                $scope.url = $state.$current.name;
            }],
            resolve: {
                authors: ['$stateParams', 'bookAuthorService', function ($stateParams, bookAuthorService) {
                    return bookAuthorService.query({
                        page: $stateParams.page || 1,
                        limit: $stateParams.limit || 10,
                        fullName: $stateParams.fullName || null
                    }).$promise;
                }]
            }
        });
        $stateProvider.state('dashboard.bookAuthor.editor', {
            url: '/editor/:id',
            template: `<book-author-editor author="author"></book-author-editor>`,
            controller: ['$scope', '$state', 'author', function ($scope, $state, author) {
                $scope.author = author;
            }],
            resolve: {
                author: ['$stateParams', 'bookAuthorService', function ($stateParams, bookAuthorService) {
                    if ($stateParams.id) {
                        return bookAuthorService.getById({id: $stateParams.id}).$promise;
                    } else {
                        return Promise.resolve(null);
                    }
                }]
            }
        });
        $stateProvider.state('dashboard.bookPerformer', {
            url: 'book-performer?page?limit?fullName',
            template: `
                <ui-view>
                    <book-performer-list performers="performers" url="url"></book-performer-list>
                </ui-view>
            `,
            controller: ['$scope', '$state', 'performers', function ($scope, $state, performers) {
                $scope.performers = performers;
                $scope.url = $state.$current.name;
            }],
            resolve: {
                performers: ['$stateParams', 'bookPerformerService', function ($stateParams, bookPerformerService) {
                    return bookPerformerService.query({
                        page: $stateParams.page || 1,
                        limit: $stateParams.limit || 10,
                        fullName: $stateParams.fullName || null
                    }).$promise;
                }]
            }
        });
        $stateProvider.state('dashboard.bookPerformer.editor', {
            url: '/editor/:id',
            template: `<book-performer-editor performer="performer"></book-performer-editor>`,
            controller: ['$scope', '$state', 'performer', function ($scope, $state, performer) {
                $scope.performer = performer;
            }],
            resolve: {
                performer: ['$stateParams', 'bookPerformerService', function ($stateParams, bookPerformerService) {
                    if ($stateParams.id) {
                        return bookPerformerService.getById({id: $stateParams.id}).$promise;
                    } else {
                        return Promise.resolve(null);
                    }
                }]
            }
        });
        $stateProvider.state('dashboard.user', {
            url: 'user?page?limit?nickname',
            template: `<ui-view><user-list url="url" users="users"></user-list></ui-view>`,
            controller: ['$scope', '$state', 'users', function ($scope, $state, users) {
                $scope.users = users;
                $scope.url = $state.$current.name;
            }],
            resolve: {
                users: ['$stateParams', 'userService', function ($stateParams, userService) {
                    return userService.query({
                        nickname: $stateParams.nickname,
                        page: $stateParams.page || 1,
                        limit: $stateParams.limit || 10
                    }).$promise;
                }],
                currentUser: ['$window', 'userService', function ($window, userService) {
                    userService.current().$promise.then((result) => {
                        if (result && !result.isAdmin) {
                            $window.location.href = '/dashboard';
                        } else {
                            return Promise.resolve(result);
                        }
                    }).catch(() => {
                        $window.location.href = '/dashboard';
                    });
                }]
            }
        });
        $stateProvider.state('dashboard.user.editor', {
            url: '/edit/:id',
            template: '<user-editor user="user" current="currentUser"></user-editor>',
            controller: ['$scope', 'user', 'currentUser', function ($scope, user, currentUser) {
                $scope.user = user;
                if (!$scope.currentUser && currentUser) {
                    $scope.currentUser = currentUser;
                }
            }],
            resolve: {
                currentUser: ['$window', 'userService', function ($window, userService) {
                    userService.current().$promise.then((result) => {
                        if (result && !result.isAdmin) {
                            $window.location.href = '/dashboard';
                        } else {
                            return Promise.resolve(result);
                        }
                    }).catch(() => {
                        $window.location.href = '/dashboard';
                    });
                }],
                user: ['$stateParams', 'userService', function ($stateParams, userService) {
                    if ($stateParams.id) {
                        return userService.getById({id: $stateParams.id}).$promise;
                    } else {
                        return Promise.resolve(null);
                    }
                }]
            }
        });
    }
];