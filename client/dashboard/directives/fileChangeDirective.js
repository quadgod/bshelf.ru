const name = 'fileChange';
const directive = [function() {
    return {
        restrict: 'A',
        scope: {
            fileChange: '&'
        },
        link: function(scope, element, attributes) {
            element.bind('change', function() {
                if (!this.files || !this.files.length) {
                    scope.$apply(() => {
                        scope.fileChange()(null);
                    });
                } else {
                    scope.$apply(() => {
                        scope.fileChange()(this.files);
                    });
                }
            });
        }
    };
}];

export { name, directive };