import angular from 'angular';
import * as ngThumbDirective from './ngThumbDirective';
import * as fileChangeDirective from './fileChangeDirective';

let directives = angular.module('app.directives', []);
directives.directive(ngThumbDirective.name, ngThumbDirective.directive);
directives.directive(fileChangeDirective.name, fileChangeDirective.directive);