export default ['$rootScope', function ($rootScope) {
    $rootScope.title = 'Панель управления';
    $rootScope.toggled = true;
    $rootScope.toggleNav = (event) => {
        if (event) {
            event.preventDefault();
        }
        $rootScope.toggled = !$rootScope.toggled;
    };
}];