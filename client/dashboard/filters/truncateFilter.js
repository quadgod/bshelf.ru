import _ from 'lodash';

const name = 'truncate';
const filter = ['$sce', function ($sce) {
    return function (val, limit, end) {
        return _.truncate(val, {
            'length': limit,
            'omission': end
        });
    };
}];

export { name, filter };
