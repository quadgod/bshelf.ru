import angular from 'angular';
import * as RangeFilter from './rangeFilter';
import * as SceLinkFilter from './sceLinkFilter';
import * as InfoLinkFilter from './infoLinkFilter';
import * as FileSizeFilter from './fileSizeFilter';
import * as TruncateFilter from './truncateFilter';
import * as BookDurationFilter from './bookDurationFilter';
import * as DurationFilter from './durationFilter';

let filters = angular.module('app.filters', []);
filters.filter(RangeFilter.name, RangeFilter.filter);
filters.filter(SceLinkFilter.name, SceLinkFilter.filter);
filters.filter(InfoLinkFilter.name, InfoLinkFilter.filter);
filters.filter(FileSizeFilter.name, FileSizeFilter.filter);
filters.filter(TruncateFilter.name, TruncateFilter.filter);
filters.filter(BookDurationFilter.name, BookDurationFilter.filter);
filters.filter(DurationFilter.name, DurationFilter.filter);