const name = 'sceLink';
const filter = ['$sce', function ($sce) {
    return function (val, start, end) {
        let link = '';
        if (start) {
            link = start.toString() + val.toString();
        }

        if (end) {
            link = link + end.toString();
        }

        if (!start && !end) {
            link = val;
        }

        if (!start) {
            return $sce.trustAsResourceUrl(link.toString());
        } else {
            return $sce.trustAsResourceUrl(link.toString());
        }
    };
}];

export { name, filter };
