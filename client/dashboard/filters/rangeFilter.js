const name = 'range';
const filter = function () {
    return function (val, range) {
        range = parseInt(range, 10);
        for (let i = 0; i < range; i++) {
            val.push(i);
        }
        return val;
    };
};

export { name, filter };
