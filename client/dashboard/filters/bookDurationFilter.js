import _ from 'lodash';

const name = 'bookDuration';
const filter = [function () {

    function padTime(t) {
        return t < 10 ? '0' + t : t;
    }

    return function (book) {

        let duration = 0;

        if (book.medias && _.isArray(book.medias) && book.medias.length > 0) {
            book.medias.forEach((media) => {
                if (!_.isUndefined(media.duration) && !_.isNull(media.duration) && _.isNumber(media.duration) && !_.isNaN(media.duration)) {
                    duration += media.duration;
                }
            });
        }

        if (!_.isNumber(duration) || _.isNaN(duration) || duration == 0) {
            return '00:00:00';
        }

        let hours = Math.floor(duration / 3600);
        let minutes = Math.floor((duration % 3600) / 60);
        let seconds = Math.floor(duration % 60);

        return `${padTime(hours)}:${padTime(minutes)}:${padTime(seconds)}`;
    };
}];

export { name, filter };
