import _ from 'lodash';

const name = 'duration';
const filter = [function () {

    function padTime(t) {
        return t < 10 ? '0' + t : t;
    }

    return function (duration) {
        if (!_.isNumber(duration) || _.isNaN(duration) || duration == 0) {
            return '00:00:00';
        }

        let hours = Math.floor(duration / 3600);
        let minutes = Math.floor((duration % 3600) / 60);
        let seconds = Math.floor(duration % 60);

        return `${padTime(hours)}:${padTime(minutes)}:${padTime(seconds)}`;
    };
}];

export { name, filter };
