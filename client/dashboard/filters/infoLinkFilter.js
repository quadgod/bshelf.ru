const name = 'infoLink';
const filter = ['$sce', function ($sce) {
    return function (val, mimeType) {
        if (mimeType) {
            if (mimeType == 'youtube') {
                let link = `https://www.youtube.com/watch?v=${val}`;
                return link;
            }  else {
                let host = window.location.hostname;
                let protocol = window.location.protocol;
                let link = `${protocol}//${host}${val}`;
                return link;
            }
        } else {
            return '';
        }
    };
}];

export { name, filter };
