import _ from 'lodash';

const name = 'fileSize';
const filter = [function () {
    return function (bytes, precision) {
        if (_.isNaN(parseFloat(bytes)) || !_.isFinite(bytes)) return '-';
        if (typeof precision === 'undefined') precision = 1;
        let units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'],
            number = Math.floor(Math.log(bytes) / Math.log(1024));
        return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) +  ' ' + units[number];
    };
}];

export { name, filter };
