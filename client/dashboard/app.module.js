import angular from 'angular';
import 'clipboard-js';
import 'jquery';
import 'angular-ui-bootstrap';
import 'angular-ui-router';
import 'angular-resource';
import 'angular-ui-notification';
import 'angular-file-upload';
import 'angular-sanitize';
import 'lodash';
import './components';
import './directives';
import './services';
import './filters';
import config from './app.config';
import run from './app.run';

let injectables = [
    'ngResource',
    'ui.router',
    'ui.bootstrap',
    'ui-notification',
    'angularFileUpload',
    'ngSanitize',
    'app.components',
    'app.directives',
    'app.services',
    'app.filters'
];

let app = angular.module('app', injectables);
app.config(config);
app.run(run);

export default app;