import * as consts from '../../../consts';

const name = 'bookAuthorList';
const component = {
    bindings: {
        url: '=',
        authors: '='
    },
    templateUrl: '/templates/dashboard/components/bookAuthorList.html',
    controller: ['$stateParams', '$state', '$uibModal', 'Notification', 'bookAuthorService',
        function ($stateParams, $state, $modal, $notification, bookAuthorService) {
            this.search = {
                value: $stateParams.fullName || null,
                invoke: () => {
                    $state.go('.', {fullName: this.search.value, page: 1});
                }
            };
            this.load = () => {
                let query = {
                    page: $stateParams.page || 1,
                    limit: $stateParams.limit || 10,
                    fullName: this.search.value || null
                };
                bookAuthorService.query(query).$promise.then((found) => {
                    if (found.docs.length > 0) {
                        this.authors = found;
                    } else {
                        if (found.pages < found.page) {
                            query.page = found.pages;
                            $state.go('.', query);
                        } else {
                            this.authors = found;
                        }
                    }
                }).catch((err) => {
                    console.error(err);
                    $notification.error(consts.STRINGS.ERRORS.internalServerError);
                });
            };
            this.delete = (author) => {
                let modal = $modal.open({
                    templateUrl: '/templates/dashboard/modals/removeConfirm.html',
                    controller: ['$scope', ($scope) => {
                        $scope.busy = false;
                        $scope.context = author.fullName;
                        $scope.delete = function () {
                            $scope.busy = true;
                            bookAuthorService.delete({action: author._id}).$promise.then(() => {
                                $scope.busy = false;
                                modal.close(author);
                            }).catch((err) => {
                                console.error(err);
                                $notification.error(consts.STRINGS.ERRORS.internalServerError);
                                $scope.busy = false;
                            });
                        };
                        $scope.cancel = function () {
                            modal.dismiss('cancel');
                        };
                    }]
                });

                modal.result.then(() => this.load());
            };
        }
    ]
};

export { name, component };