const name = 'paginator';
const component = {
    bindings: {
        url: '=',
        page: '=',
        limit: '=',
        pages: '=',
        total: '='
    },
    templateUrl: '/templates/dashboard/components/paginator.html',
    controller: ['$state', function($state) {
        this.changeLimit = (newLimit) => {
            this.limit = newLimit;
            $state.go(this.url, { page: 1, limit: this.limit });
        };
        this.next = () => {
            $state.go(this.url, { page: this.page + 1, limit: this.limit });
        };
        this.prev = () => {
            $state.go(this.url, { page: this.page - 1, limit: this.limit });
        };
    }]
};

export { name, component };