import * as consts from '../../../consts';

const name = 'bookPerformerEditor';
const component = {
    bindings: {
        performer: '='
    },
    templateUrl: '/templates/dashboard/components/bookPerformerEditor.html',
    controller: ['bookPerformerService', 'Notification', '$state', function (bookPerformerService, $notification, $state) {
        this.state = {
            busy: false
        };

        this.handleError = (err) => {
            if (err.status == 409) {
                $notification.error(consts.STRINGS.ERRORS.performerFullNameConflict);
            } else {
                $notification.error(consts.STRINGS.ERRORS.internalServerError);
            }
            this.state.busy = false;
        };

        this.save = (event, form) => {
            event.preventDefault();
            if (form.$valid) {
                this.state.busy = true;
                if (this.performer._id) {
                    bookPerformerService.update(this.performer).$promise.then((response) => {
                        $state.go('^', {fullName: response.fullName, page: 1});
                    }).catch(this.handleError);
                } else {
                    bookPerformerService.add(this.performer).$promise.then((response) => {
                        $state.go('^', {fullName: response.fullName, page: 1});
                    }).catch(this.handleError);
                }
            }
        };
    }]
};

export { name, component };