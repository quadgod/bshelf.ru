import _ from 'lodash';
import * as consts from '../../../consts';

const name = 'documentEditor';
const component = {
    bindings: {
        document: '=',
        breadcrumbs: '='
    },
    templateUrl: '/templates/dashboard/components/documentEditor.html',
    controller: ['$timeout', 'entityService', 'Notification', '$state', function ($timeout, $entity, $notification, $state) {
        this.busy = false;
        this.tags = this.document.tags.join(' ');

        this.submit = (form) => {
            if (form.$valid) {
                this.busy = true;
                if (!_.isUndefined(this.tags) && !_.isNull(this.tags) && !_.isEmpty(this.tags)) {
                    this.document.tags = _.uniq(_.filter(this.tags.toString().split(' '), (item) => {
                        return item != '';
                    }));
                } else {
                    this.document.tags = [];
                }


                let data = {...this.document};
                if (data.media) {
                    data.media = data.media._id;
                }

                if (!data._id) {
                    $entity.add(data).$promise.then(() => {
                        $timeout(() => {
                            $state.go('^', {page: 1, title: null}, {reload: true});
                        });
                    }).catch((err) => {
                        if (err.status == 409) {
                            $notification.error(consts.STRINGS.ERRORS.entityDocumentTitleConflict);
                        } else {
                            $notification.error(consts.STRINGS.ERRORS.internalServerError);
                        }
                        this.busy = false;
                    });
                } else {
                    $entity.update(data).$promise.then(() => {
                        $timeout(() => {
                            $state.go('^', {page: 1, title: null}, {reload: true});
                        });
                    }).catch((err) => {
                        if (err.status == 409) {
                            $notification.error(consts.STRINGS.ERRORS.entityDocumentTitleConflict);
                        } else {
                            $notification.error(consts.STRINGS.ERRORS.internalServerError);
                        }
                        this.busy = false;
                    });
                }
            }
        };
    }]
};

export { name, component };