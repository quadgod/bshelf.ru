import * as consts from '../../../consts';

const name = 'bookPerformerList';
const component = {
    bindings: {
        url: '=',
        performers: '='
    },
    templateUrl: '/templates/dashboard/components/bookPerformerList.html',
    controller: ['$stateParams', '$state', '$uibModal', 'Notification', 'bookPerformerService',
        function ($stateParams, $state, $modal, $notification, bookPerformerService) {
            this.search = {
                value: $stateParams.fullName || null,
                invoke: () => {
                    $state.go('.', {fullName: this.search.value, page: 1});
                }
            };
            this.load = () => {
                let query = {
                    page: $stateParams.page || 1,
                    limit: $stateParams.limit || 10,
                    fullName: this.search.value || null
                };
                bookPerformerService.query(query).$promise.then((found) => {
                    if (found.docs.length > 0) {
                        this.performers = found;
                    } else {
                        if (found.pages < found.page) {
                            query.page = found.pages;
                            $state.go('.', query);
                        } else {
                            this.performers = found;
                        }
                    }
                }).catch((err) => {
                    console.error(err);
                    $notification.error(consts.STRINGS.ERRORS.internalServerError);
                });
            };
            this.delete = (performer) => {
                let modal = $modal.open({
                    templateUrl: '/templates/dashboard/modals/removeConfirm.html',
                    controller: ['$scope', ($scope) => {
                        $scope.busy = false;
                        $scope.context = this.performers.fullName;
                        $scope.delete = function () {
                            $scope.busy = true;
                            bookPerformerService.delete({action: performer._id}).$promise.then(() => {
                                $scope.busy = false;
                                modal.close();
                            }).catch((err) => {
                                $notification.error(consts.STRINGS.ERRORS.internalServerError);
                                $scope.busy = false;
                            });
                        };
                        $scope.cancel = function () {
                            modal.dismiss('cancel');
                        };
                    }]
                });

                modal.result.then(() => this.load());
            };
        }
    ]
};

export { name, component };