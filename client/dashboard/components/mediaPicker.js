const pickerTemplate =
`<div class="b-picker">
    <div class="row">
        <div class="col-sm-12 text-center" ng-class="{'b-picker__centered': $ctrl.media != null}">
            <span class="btn btn-default b-curp b-picker__select" ng-if="$ctrl.media == null" ng-click="$ctrl.select()" ng-disabled="$ctrl.disabled">
                <i class="{{$ctrl.buttonIcon}}"></i>
                {{ $ctrl.selectText }}
            </span>
            <preview-media ng-if="$ctrl.media != null"
                           media="$ctrl.media"
                           item-class="'b-curp'"
                           icon-class="'fa-2x b-curp'"
                           container-class="'b-item b-picker__media'"
                           action="$ctrl.select">               
            </preview-media>        
        </div>
    </div>
</div>`;

let modalHeaderTemplate =
`<div class="modal-header">
    <div class="row">
        <div class="col-xs-6">
            <h5>Выбрать медиа</h5>
        </div>
        <div class="col-xs-6 text-right">
            <i class="fa fa-close b-curp" ng-click="cancel($event)"></i>
        </div>
    </div>
</div>`;

let breadcrumbsTemplate =
`<div class="row b-breadcrumbs" ng-if="state.breadcrumbs.length > 0">
    <a class="fa fa-home b-curp" ng-click="load('root')"></a>
     <span ng-repeat="bc in state.breadcrumbs">
         <span class="btn btn-link" ng-click="load(bc._id)" ng-bind="bc.title"></span> {{$last ? '' : '/'}}
     </span>
</div>`;

let toolbarTemplate =
`<div class="row b-toolbar">
    <div class="col-xs-12 b-toolbar__col">
        <form name="toolbarForm" ng-submit="load()">
            <div class="input-group">
                <input class="form-control"
                       type="text"
                       name="title"
                       ng-disabled="state.busy"
                       ng-model="state.query"
                       placeholder="Поиск по имени">
                <span class="input-group-btn">
                    <button ng-disabled="state.busy" type="submit" class="btn btn-toolbar" uib-tooltip="Поиск">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
    </div>
</div>
<button class="btn btn-default b-add-all-button" ng-if="state.addAllMode" ng-click="addAll()">Добавить все содержимое папки</button>`;

let modalFooterTemplate =
`<div class="modal-footer">
    <div class="row">
        <div class="col-xs-2 text-left">
            <button class="btn btn-danger btn-sm"
                    uib-tooltip="Удалить"
                    type="button"
                    ng-disabled="state.busy"
                    ng-if="state.showDeleteButton"
                    ng-click="select(null)">
                <i class="fa fa-trash"></i>
            </button>
        </div>
        <div class="col-xs-10 text-right">
            <button class="btn btn-default btn-sm" type="button" ng-click="prev()" ng-disabled="state.busy || state.medias.page <= 1">
                <i class="fa fa-arrow-circle-left"></i>
            </button>
            <span>Страниц {{ state.medias.page }} из {{ state.medias.pages }}</span>
            <button class="btn btn-default btn-sm" type="button" ng-click="next()" ng-disabled="state.busy || state.medias.page >= state.medias.pages">
                <i class="fa fa-arrow-circle-right"></i>
            </button>
        </div>
    </div>
</div>`;

let modalTemplate = `
    <div class="b-picker__modal">
        ${modalHeaderTemplate}
        <div class="modal-body">
            ${toolbarTemplate}
            ${breadcrumbsTemplate}
            <div class="b-list b-list-middle b-list-stacked">
                <div class="row b-list__item" ng-repeat="item in state.medias.docs">
                    <div class="col-xs-2 b-list__item__col">
                        <preview-media 
                            icon-class="'fa-2x b-item b-curp'"
                            item-class="'b-item b-curp'"
                            container-class="'b-list__item__col__centered'"
                            media="item"
                            action="select">
                        </preview-media>
                    </div>
                    <div class="col-xs-8 b-list__item__col">
                        <div class="b-list__item__col__lcentered">
                            <div class="b-item">
                                <div ng-if="item.type == 1">Название: <a class="b-curp" ng-click="select(item)" ng-bind="item.title"></a></div>
                                <div ng-if="item.type != 1">Название: <b  ng-bind="item.title"></b></div>
                                <div ng-if="item.type != 1">Тип: <b ng-bind="item.mimeType"></b></div>
                                <div ng-if="item.type != 1">Размер: <b ng-bind="item.size | fileSize: 2"></b></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-2 b-list__item__col">
                        <div class="b-list__item__col__rcentered" ng-if="item.type != 1">
                            <div class="b-item">
                                <button class="btn btn-default btn-sm" ng-click="select(item)" ng-disabled="state.busy" uib-tooltip="Выбрать" tooltip-placement="left">
                                    <i class="fa fa-check"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        ${modalFooterTemplate}
    </div>
`;

import _ from 'lodash';
import angular from 'angular';
import * as consts from '../../../consts';
import * as EntityType from '../../../server/models/EntityType';



const name = 'mediaPicker';
const component = {
    bindings: {
        addAllMode: '=',
        showDeleteButton: '=',
        media: '=',
        mimeTypes: '=',
        disabled: '=',
        iconClass: '=',
        selectButtonText: '=',
        onChange: '&'
    },
    template: pickerTemplate,
    controller: ['$uibModal', '$sce', 'mediaService', 'Notification', '$timeout',
        function($modal, $sce, $media, $notification, $timeout) {
            this.buttonIcon = this.iconClass || 'fa fa-file-photo-o';
            this.selectText = this.selectButtonText || 'Выбрать';
            this._addAllMode = angular.copy(this.addAllMode);
            if (!(this._addAllMode === true || this._addAllMode === false)) {
                this._addAllMode = false;
            }
            this._showDeleteButton = angular.copy(this.showDeleteButton);
            if (!(this._showDeleteButton === true || this._showDeleteButton === false)) {
                this._showDeleteButton = true;
            }

            this.select = () => {
                if (!this.disabled) {
                    let modal = $modal.open({
                        size: 'lg',
                        template: modalTemplate,
                        controller: ['$scope', 'medias', ($scope, medias) => {
                            $scope.state = {
                                addAllMode: this._addAllMode,
                                showDeleteButton: this._showDeleteButton,
                                limit: 10,
                                page: 1,
                                parent: 'root',
                                sort: 'created',
                                query: null,
                                medias: medias,
                                breadcrumbs: [],
                                busy: false
                            };

                            let pickFields = ['page', 'limit', 'parent', 'query', 'mimeTypes', 'sort'];

                            if (this.mimeTypes && this.mimeTypes.length > 0) {
                                $scope.state.mimeTypes = this.mimeTypes;
                            } else {
                                $scope.state.mimeTypes = null;
                            }

                            $scope.load = (id) => {
                                if ($scope.state.busy) return;
                                if (id && $scope.state.parent == id) { return; }
                                if (id) { $scope.state.parent = id; }

                                let query = _.pick($scope.state, pickFields);

                                Promise.all([
                                    $media.query(query).$promise,
                                    $media.breadcrumbs({parent: query.parent}).$promise
                                ]).then((results) => {
                                    $timeout(() => {
                                        $scope.state.medias = results[0];
                                        $scope.state.breadcrumbs = results[1];
                                    });
                                }).catch(() => {
                                    $notification.error(consts.STRINGS.ERRORS.internalServerError);
                                });
                            };

                            $scope.select = (media) => {
                                if (media && media.type == EntityType.FOLDER) {
                                    $scope.load(media._id);
                                } else {
                                    modal.close(media);
                                }
                            };
                            $scope.next = () => {
                                $scope.state.page = $scope.state.page + 1;
                                let query = _.pick($scope.state, pickFields);
                                $media.query(query).$promise.then((medias) => {
                                    $timeout(() => {
                                        $scope.state.medias = medias;
                                    });
                                }).catch(() => {
                                    $notification.error(consts.STRINGS.ERRORS.internalServerError);
                                });
                            };
                            $scope.prev = () => {
                                $scope.state.page = $scope.state.page - 1;
                                let query = _.pick($scope.state, pickFields);
                                $media.query(query).$promise.then((medias) => {
                                    $timeout(() => {
                                        $scope.state.medias = medias;
                                    });
                                }).catch((err) => {
                                    console.error(err);
                                    $notification.error(consts.STRINGS.ERRORS.internalServerError);
                                });
                            };
                            $scope.addAll = () => {
                                $scope.state.busy = true;
                                let params = _.pick($scope.state, pickFields);
                                $media.getAll(params).$promise.then((medias) => {
                                    modal.close(medias);
                                }).catch((err) => {
                                    console.error(err);
                                    $notification.error(consts.STRINGS.ERRORS.internalServerError);
                                    $scope.state.busy = false;
                                });
                            };
                            $scope.cancel = () => modal.dismiss('cancel');
                        }],
                        resolve: {
                            medias: () => {
                                let query = {parent: 'root'};
                                if (this.mimeTypes && this.mimeTypes.length > 0) {
                                    query.mimeTypes = this.mimeTypes;
                                }
                                return $media.query(query).$promise;
                            }
                        }
                    });
                    modal.result.then((media) => {
                        $timeout(() => {
                            this.media = media;
                            if (this.onChange()) {
                                this.onChange()(this.media);
                            }
                        });
                    });
                }
            };
        }
    ]
};

export { name, component };