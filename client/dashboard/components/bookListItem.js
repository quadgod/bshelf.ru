import * as consts from '../../../consts';
import * as EntityType from '../../../server/models/EntityType';

const name = 'bookListItem';
const component = {
    bindings: {
        book: '=',
        load: '&'
    },
    templateUrl: '/templates/dashboard/components/bookListItem.html',
    controller: ['$stateParams', '$state', '$uibModal', 'Notification', 'bookService', '$timeout',
        function ($stateParams, $state, $modal, $notification, bookService, $timeout) {
            this.delete = (book) => {
                let modal = $modal.open({
                    templateUrl: '/templates/dashboard/modals/removeConfirm.html',
                    controller: ['$scope', ($scope) => {
                        $scope.busy = false;
                        $scope.context = book.title;
                        $scope.delete = () => {
                            $scope.busy = true;
                            bookService.delete({action: book._id}).$promise.then(() => {
                                $scope.busy = false;
                                modal.close(book);
                            }).catch((err) => {
                                if (err.status == 403) {
                                    $notification.error(consts.STRINGS.ERRORS.unableToDeleteFolderHasChild);
                                } else {
                                    $notification.error(consts.STRINGS.ERRORS.internalServerError);
                                }

                                $scope.busy = false;
                            });
                        };
                        $scope.cancel = function () {
                            modal.dismiss('cancel');
                        };
                    }]
                });

                modal.result.then(() => {
                    if (this.load && this.load()) {
                        this.load()();
                    }
                });
            };
            this.edit = (book) => {
                let title = book.type == EntityType.FOLDER ? 'Редактирование жанра' : 'Редактирование серии';
                let modal = $modal.open({
                    size: 'lg',
                    templateUrl: '/templates/dashboard/modals/book/add.html',
                    controller: ['$scope', ($scope) => {
                        $scope.state = {
                            busy: false,
                            edit: true,
                            title: title,
                            model: {...book}
                        };
                        $scope.ok = (form, event) => {
                            event.preventDefault();
                            if (form.$valid) {
                                $scope.state.busy = true;
                                bookService.update($scope.state.model).$promise.then((response) => {
                                    $scope.state.busy = false;
                                    modal.close(response);
                                }).catch((err) => {
                                    if (err.status == 409 && book.type == EntityType.FOLDER) {
                                        $notification.error(consts.STRINGS.ERRORS.genreConflict);
                                    } else if (err.status == 409 && book.type == EntityType.BOOK_SERIES) {
                                        $notification.error(consts.STRINGS.ERRORS.seriesConflict);
                                    } else {
                                        $notification.error(consts.STRINGS.ERRORS.internalServerError);
                                    }
                                    $scope.state.busy = false;
                                });
                            }
                        };
                        $scope.cancel = () => {
                            modal.dismiss('cancel');
                        };
                    }]
                });
                modal.result.then(() => {
                    if (this.load && this.load()) {
                        this.load()();
                    }
                });
            };
        }
    ]
};

export { name, component };