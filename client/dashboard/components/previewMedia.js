const name = 'previewMedia';
const component = {
    bindings: {
        media: '=',
        iconClass: '=',
        itemClass: '=',
        containerClass: '=',
        action: '&'
    },
    template: `
        <div ng-if="$ctrl.media" class="{{$ctrl.containerClass}}">
            
            <i ng-if="$ctrl.media.type == 1" ng-click="$ctrl.invokeAction()" class="fa fa-folder-open-o {{$ctrl.iconClass}}"></i>
            
            <img ng-if="$ctrl.media.mimeType.indexOf('image/') != -1 && $ctrl.media.type == 3"
                 class="{{$ctrl.itemClass}}"
                 ng-src="{{$ctrl.media.fileName | sceLink}}"
                 ng-click="$ctrl.invokeAction()">
                 
            <img ng-if="$ctrl.media.mimeType == 'youtube'" 
                 class="{{$ctrl.itemClass}}"
                 ng-src="{{$ctrl.media.fileName | sceLink : 'http://img.youtube.com/vi/':'/default.jpg' }}"
                 ng-click="$ctrl.invokeAction()">
            
            <i ng-if="$ctrl.media.mimeType.indexOf('audio/') != -1 && $ctrl.media.type == 3"
               class="fa fa-file-audio-o {{$ctrl.iconClass}}"
               ng-click="$ctrl.invokeAction()"></i>
               
            <video  ng-if="$ctrl.media.mimeType.indexOf('video/') != -1 && $ctrl.media.type == 3"
                    class="{{$ctrl.itemClass}}"
                    ng-src="{{$ctrl.media.fileName | sceLink}}"
                    ng-click="$ctrl.invokeAction()">
            </video>
            
           <i   ng-if="$ctrl.media.mimeType == 'application/pdf' && $ctrl.media.type == 3"
                class="fa fa-file-pdf-o {{$ctrl.iconClass}}"
                ng-click="$ctrl.invokeAction()"></i>
               
           <i   ng-if="($ctrl.media.mimeType == 'fb2' || $ctrl.media.mimeType == 'application/epub+zip') && $ctrl.media.type == 3"
                class="fa fa-book {{$ctrl.iconClass}}"
                ng-click="$ctrl.invokeAction()"></i>
               
           <i   ng-if="$ctrl.media.mimeType == 'text/plain' && $ctrl.media.type == 3"
                class="fa fa-file-text {{::$ctrl.iconClass}}"
                ng-click="$ctrl.invokeAction()"></i>
        </div>
    `,
    controller: [function() {
        this.invokeAction = () => {
            if (this.action()) {
                this.action()(this.media);
            }
        };
    }]
};

export { name, component };

