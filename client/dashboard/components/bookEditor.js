import * as EntityType from '../../../server/models/EntityType';
import * as BookType from '../../../server/models/BookType';
import * as consts from '../../../consts';
import _ from 'lodash';

const name = 'bookEditor';
const component = {
    bindings: {
        book: '=',
        breadcrumbs: '='
    },
    templateUrl: '/templates/dashboard/components/bookEditor.html',
    controller: ['$stateParams', '$state', 'Notification', 'bookService', function ($stateParams, $state, $notification, bookService) {
        this.state = {
            parent: $stateParams.parent || 'root',
            busy: false,
            mediasDirty: false,
            tags: null,
            currentYear: (new Date()).getFullYear()
        };

        if (!this.book) {
            this.book = {
                type: EntityType.BOOK,
                bookType: BookType.AUDIO,
                medias: [],
                sortOrder: 1,
                published: false,
                mediasUnpublished: false
            };
        }

        if (this.book.tags && this.book.tags.length > 0) {
            this.state.tags = this.book.tags.join(' ').toString();
        } else {
            this.state.tags = null;
        }

        this.bookTypeChange = () => {
            if (this.book.bookType == BookType.AUDIO) {
                this.book.medias = _.filter(this.medias, (m) => {
                    return m.mimeType.indexOf('audio/') == -1;
                });
            } else {
                this.book.medias = _.filter(this.medias, (m) => {
                    return m.mimeType.indexOf('audio/') != -1;
                });
            }
            this.state.mediasDirty = true;
        };
        this.mediasChange = (medias) => {
            this.state.mediasDirty = true;
        };


        this.save = (form) => {
            if (form.$valid && this.book.medias.length > 0) {
                this.state.busy = true;
                if (!_.isUndefined(this.state.tags) && !_.isNull(this.state.tags) && !_.isEmpty(this.state.tags)) {
                    this.book.tags = _.filter(this.state.tags.toString().split(' '), (item) => {
                        return item != '';
                    }).map((item) => {
                        return item.toString();
                    });
                } else {
                    this.book.tags = [];
                }
                let model = {...this.book};
                if (!model.parent) {
                    model.parent = this.state.parent;
                }
                model.medias = model.medias.map((m) => {
                    return m._id;
                });
                model.author = model.author._id;

                if (model.bookType == BookType.AUDIO) {
                    model.performer = model.performer._id;
                } else {
                    model.performer = null;
                }

                if (!model._id) {
                    bookService.add(model).$promise.then((response) => {
                        $state.go('^', {title: null, page: 1}, {reload: true});
                    }).catch((err) => {
                        console.error(err);
                        if (err.status == 409) {
                            $notification.error(consts.STRINGS.ERRORS.bookTitleConflict);
                        } else {
                            $notification.error(consts.STRINGS.ERRORS.internalServerError);
                        }
                        this.state.busy = false;
                    });
                } else {
                    bookService.update(model).$promise.then((response) => {
                        $state.go('^', {title: null, page: 1}, {reload: true});
                    }).catch((err) => {
                        console.error(err);
                        if (err.status == 409) {
                            $notification.error(consts.STRINGS.ERRORS.bookTitleConflict);
                        } else {
                            $notification.error(consts.STRINGS.ERRORS.internalServerError);
                        }
                        this.state.busy = false;
                    });
                }
            }
        };
    }]
};

export { name, component };