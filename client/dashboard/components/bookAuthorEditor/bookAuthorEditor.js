import * as consts from '../../../../consts';

const name = 'bookAuthorEditor';
const component = {
    bindings: {
        author: '='
    },
    template: require('raw!./bookAuthorEditor.html'),
    controller: ['bookAuthorService', 'Notification', '$state', function (bookAuthorService, $notification, $state) {
        this.state = {
            busy: false
        };

        this.handleError = (err) => {
            if (err.status == 409) {
                $notification.error(consts.STRINGS.ERRORS.authorFullNameConflict);
            } else {
                $notification.error(consts.STRINGS.ERRORS.internalServerError);
            }
            this.state.busy = false;
        };

        this.save = (event, form) => {
            event.preventDefault();
            if (form.$valid) {
                this.state.busy = true;
                if (this.author._id) {
                    bookAuthorService.update(this.author).$promise.then((response) => {
                        $state.go('^', {fullName: response.fullName, page: 1});
                    }).catch(this.handleError);
                } else {
                    bookAuthorService.add(this.author).$promise.then((response) => {
                        $state.go('^', {fullName: response.fullName, page: 1});
                    }).catch(this.handleError);
                }
            }
        };
    }]
};

export { name, component };