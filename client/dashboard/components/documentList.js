import * as EntityType from '../../../server/models/EntityType';
import * as consts from '../../../consts';

const name = 'documentList';
const component = {
    bindings: {
        url: '=',
        entities: '=',
        breadcrumbs: '='
    },
    templateUrl: '/templates/dashboard/components/documentList.html',
    controller: ['$uibModal', 'entityService', 'Notification', '$stateParams', '$state', '$timeout',
    function ($modal, entityService, $notification, $stateParams, $state, $timeout) {
        this.parent = $stateParams.parent;
        this.search = {
            value: $stateParams.title,
            invoke: (event) => {
                event.preventDefault();
                $state.go(this.url, {
                    parent: this.parent,
                    page: 1,
                    limit: $stateParams.limit || 10,
                    title: this.search.value || null
                });
            }
        };
        this.load = () => {
            let params = {
                parent: this.parent,
                page: $stateParams.page || 1,
                limit: $stateParams.limit || 10,
                title: this.search.value
            };
            entityService.query(params).$promise.then((entities) => {
                if (entities.docs.length > 0) {
                    this.entities = entities;
                } else {
                    if (entities.pages < entities.page) {
                        params.page = entities.pages;
                        $state.go(this.url, params);
                    } else {
                        this.entities = entities;
                    }
                }
            }).catch((err) => {
                console.error(err);
                $notification.error(consts.STRINGS.ERRORS.internalServerError);
            });
        };
        this.addCategory = (parent) => {
            let modalInstance = $modal.open({
                templateUrl: '/templates/dashboard/modals/entity/category.html',
                controller: ['$scope', function ($scope) {
                    $scope.model = {
                        title: '',
                        sortOrder: 1,
                        body: '',
                        type: EntityType.FOLDER,
                        parent: parent,
                        url: ''
                    };
                    $scope.busy = false;
                    $scope.ok = function (form) {
                        form.$setSubmitted();
                        if (form.$valid && !$scope.busy) {
                            $scope.busy = true;
                            entityService.add($scope.model).$promise.then((response) => {
                                $scope.busy = false;
                                modalInstance.close(response.entity);
                            }).catch((err) => {
                                if (err.status == 409) {
                                    $notification.error(consts.STRINGS.ERRORS.entityFolderTitleConflict);
                                } else {
                                    $notification.error(consts.STRINGS.ERRORS.internalServerError);
                                }
                                $scope.busy = false;
                            });
                        }
                    };
                    $scope.cancel = function () {
                        modalInstance.dismiss('cancel');
                    };
                }]
            });
            modalInstance.result.then(() => this.load());
        };
        this.editCategory = (category) => {
            let categoryCopy = {...category};
            let modalInstance = $modal.open({
                templateUrl: '/templates/dashboard/modals/entity/category.html',
                controller: ['$scope', function ($scope) {
                    $scope.model = categoryCopy;
                    $scope.busy = false;
                    $scope.ok = (form) => {
                        form.$setSubmitted();
                        if (form.$valid && !$scope.busy) {
                            $scope.busy = true;
                            entityService.update($scope.model).$promise.then((response) => {
                                $scope.busy = false;
                                modalInstance.close(response);
                            }).catch((err) => {
                                if (err.status == 409) {
                                    $notification.error(consts.STRINGS.ERRORS.entityFolderTitleConflict);
                                } else {
                                    $notification.error(consts.STRINGS.ERRORS.internalServerError);
                                }
                                $scope.busy = false;
                            });
                        }
                    };

                    $scope.cancel = () => modalInstance.dismiss('cancel');
                }]
            });
            modalInstance.result.then(() => this.load());
        };
        this.delete = (entity) => {
            let modalInstance = $modal.open({
                templateUrl: '/templates/dashboard/modals/removeConfirm.html',
                controller: ['$scope', function ($scope) {
                    $scope.busy = false;
                    $scope.context = entity.title;
                    $scope.delete = function () {
                        $scope.busy = true;
                        entityService.delete({action: entity._id}).$promise.then(() => {
                            $scope.busy = false;
                            modalInstance.close(entity);
                        }).catch((err) => {
                            console.error(err);
                            if (err.status == 404) {
                                $notification.error(consts.STRINGS.ERRORS.entityNotFound);
                            } else {
                                $notification.error(consts.STRINGS.ERRORS.internalServerError);
                            }
                            $scope.busy = false;
                        });
                    };
                    $scope.cancel = function () {
                        modalInstance.dismiss('cancel');
                    };
                }]
            });

            modalInstance.result.then(() => this.load());
        };
        this.publish = (doc, publish) => {
            if (publish) {
                let modal = $modal.open({
                    templateUrl: '/templates/dashboard/modals/entity/publishConfirm.html',
                    controller: ['$scope', function ($scope) {
                        $scope.update = false;
                        $scope.ok = () => {
                            modal.close($scope.update);
                        };
                        $scope.cancel = function () {
                            modal.dismiss('cancel');
                        };
                    }]
                });

                modal.result.then((update) => {
                    $timeout(() => {
                        let docToUpdate = {...doc};
                        if (update) {
                            docToUpdate.updateCreatedDate = update;
                        }
                        doc.published = publish;
                        docToUpdate.published = publish;
                        entityService.update(docToUpdate).$promise.then((result) => {
                            doc.created = result.created;
                        }).catch((err) => {
                            console.error(err);
                            $notification.error(consts.STRINGS.ERRORS.internalServerError);
                        });
                    });
                });
            } else {
                doc.published = publish;
                entityService.update(doc).$promise.then(() => {}).catch((err) => {
                    console.error(err);
                    $notification.error(consts.STRINGS.ERRORS.internalServerError);
                });
            }
        };
    }]
};

export { name, component };