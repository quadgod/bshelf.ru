import * as consts from '../../../consts';
import clipboard from 'clipboard-js';

const name = 'userList';
const component = {
    bindings: {
        url: '=',
        users: '='
    },
    templateUrl: '/templates/dashboard/components/userList.html',
    controller: ['$stateParams', '$state', '$uibModal', 'Notification', 'userService',
        function ($stateParams, $state, $modal, $notification, userService) {
            this.search = {
                value: $stateParams.nickname || null,
                invoke: () => {
                    $state.go('.', {nickname: this.search.value, page: 1});
                }
            };
            this.resetPassword = (doc) => {
                let modal = $modal.open({
                    templateUrl: '/templates/dashboard/modals/user/resetPassword.html',
                    controller: ['$scope', '$timeout', ($scope, $timeout) => {
                        $scope.state = {
                            busy: false,
                            password: '',
                            repeatPassword: ''
                        };

                        $scope.submit = (form) => {
                            if (form.$valid && !$scope.state.busy) {
                                $scope.state.busy = true;
                                doc.password = $scope.state.password;
                                doc = _.pick(doc, ['_id', 'password']);
                                userService.resetPassword(doc).$promise.then(() => {
                                    modal.dismiss('cancel');
                                    $timeout(() => {
                                        $notification.success('Пароль успешно изменен!');
                                    });
                                }).catch((err) => {
                                    console.error(err);
                                    $notification.error(consts.STRINGS.ERRORS.internalServerError);
                                });
                            }
                        };
                        $scope.cancel = ($event) => {
                            $event.preventDefault();
                            modal.dismiss('cancel');
                        };
                        $scope.comparePasswords = (form) => {
                            if (form.repeatPassword.$error.hasOwnProperty('cmppasswords')) {
                                if ($scope.state.password != $scope.state.repeatPassword) {
                                    form.repeatPassword.$setValidity('cmppasswords', false);
                                } else {
                                    form.repeatPassword.$setValidity('cmppasswords', true);
                                }
                            } else {
                                if (form.password.$valid && form.repeatPassword.$valid && form.password.$dirty && form.repeatPassword.$dirty && $scope.state.password != $scope.state.repeatPassword) {
                                    form.repeatPassword.$setValidity('cmppasswords', false);
                                } else {
                                    form.repeatPassword.$setValidity('cmppasswords', true);
                                }
                            }
                        };
                    }]
                });
            };
            this.copyToClipboard = (text) => {
                clipboard.copy(text).then(() => {
                    $notification.info(`Текст '${text}' скопирован в буфер обмена.`);
                });
            };
        }
    ]
};

export { name, component };