import * as EntityType from '../../../server/models/EntityType';
import * as consts from '../../../consts';

const name = 'bookList';
const component = {
    bindings: {
        url: '=',
        books: '=',
        breadcrumbs: '='
    },
    templateUrl: '/templates/dashboard/components/bookList.html',
    controller: ['$stateParams', '$state', '$uibModal', 'Notification', 'bookService', '$timeout',
        function ($stateParams, $state, $modal, $notification, bookService, $timeout) {
            this.parent = $stateParams.parent;
            this.parentType = null;
            if (this.breadcrumbs.length > 0) {
                this.parentType = this.breadcrumbs[this.breadcrumbs.length - 1].type;
            }
            this.search = {
                value: $stateParams.title || null,
                invoke: () => {
                    let params = {
                        parent: this.parent,
                        page: 1,
                        limit: $stateParams.limit || 10,
                        title: this.search.value || null
                    };
                    $state.go(this.url, params);
                }
            };
            this.load = () => {
                $timeout(() => {
                    let params = {
                        parent: this.parent,
                        page: $stateParams.page || 1,
                        limit: $stateParams.limit || 10,
                        title: $stateParams.title || null
                    };
                    bookService.query(params).$promise.then((books) => {
                        if (books.docs.length > 0) {
                            this.books = books;
                        } else {
                            if (books.pages < books.page) {
                                params.page = books.pages;
                                $state.go(this.url, params);
                            } else {
                                this.books = books;
                            }
                        }
                    }).catch(() => {
                        $notification.error(consts.STRINGS.ERRORS.internalServerError);
                    });
                });
            };
            this.add = (type) => {
                let title = type == EntityType.FOLDER ? 'Добавление жанра' : 'Добавление серии';
                let modal = $modal.open({
                    size: 'lg',
                    templateUrl: '/templates/dashboard/modals/book/add.html',
                    controller: ['$scope', ($scope) => {
                        $scope.state = {
                            busy: false,
                            edit: false,
                            title: title,
                            model: {
                                type: type,
                                parent: this.parent,
                                title: '',
                                description: '',
                                url: ''
                            }
                        };
                        $scope.ok = (form, event) => {
                            event.preventDefault();
                            if (form.$valid) {
                                $scope.state.busy = true;
                                bookService.add($scope.state.model).$promise.then((response) => {
                                    $scope.state.busy = false;
                                    modal.close(response.book);
                                }).catch((err) => {
                                    if (err.status == 409 && type == EntityType.FOLDER) {
                                        $notification.error(consts.STRINGS.ERRORS.genreConflict);
                                    } else if (err.status == 409 && type == EntityType.BOOK_SERIES) {
                                        $notification.error(consts.STRINGS.ERRORS.seriesConflict);
                                    } else {
                                        $notification.error(consts.STRINGS.ERRORS.internalServerError);
                                    }
                                    $scope.state.busy = false;
                                });
                            }
                        };
                        $scope.cancel = (event) => {
                            event.preventDefault();
                            modal.dismiss('cancel');
                        };
                    }]
                });
                modal.result.then(() => this.load());
            };
        }
    ]
};

export { name, component };