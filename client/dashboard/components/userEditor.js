import _ from 'lodash';
import * as consts from '../../../consts';

class RolesModel {
    constructor(editableUser, currentUser) {
        this._editableUser = editableUser;
        this._currentUser = currentUser;
        this._roles = {
            admin: _.includes(editableUser.roles, consts.ROLE.ADMIN),
            moderator: _.includes(editableUser.roles, consts.ROLE.MODERATOR),
            user: _.includes(editableUser.roles, consts.ROLE.USER)
        }
    }
    get admin() {
        return this._roles.admin;
    }
    set admin(value) {
        if (this._currentUser._id == this._editableUser._id && this._currentUser.isAdmin) {
            this._roles.admin = true;
        } else {
            this._roles.admin = value;
        }
        this.applyValue();
    }
    get moderator() {
        return this._roles.moderator;
    }
    set moderator(value) {
        this._roles.moderator = value;
        this.applyValue();
    }
    get user() {
        return true;
    }
    set user(value) {
        this._roles.user = true;
        this.applyValue();
    }

    applyValue() {
        let roles = [];
        if (this._roles.user) {
            roles.push(consts.ROLE.USER);
        }
        if (this._roles.moderator) {
            roles.push(consts.ROLE.MODERATOR);
        }
        if (this._roles.admin) {
            roles.push(consts.ROLE.ADMIN);
        }

        this._editableUser.roles = roles;
    }
}

const name = 'userEditor';
const component = {
    bindings: {
        user: '=',
        current: '='
    },
    templateUrl: '/templates/dashboard/components/userEditor.html',
    controller: ['$timeout', '$state', 'Notification', 'userService', function ($timeout, $state, $notification, userService) {
        this.busy = false;
        this.roles = new RolesModel(this.user, this.current);
        this.avatar = null;
        this.user = this.user || {};
        this.avatarChange = (files) => {
            if (files && files.length > 0) {
                if (['image/jpeg', 'image/png'].indexOf(files[0].type) == -1) {
                    $notification.error(consts.STRINGS.ERRORS.unsupportedAvatarFormat);
                } else {
                    var formData = new FormData();
                    formData.append('_id', this.user._id);
                    formData.append('avatar', files[0]);
                    this.busy = true;
                    $('#avatar').val(null);
                    userService.uploadAvatar(formData).$promise.then((response) => {
                        this.user.avatar = response.avatar;
                        this.busy = false;
                    }).catch((err) => {
                        if (err == 404) {
                            $notification.error(consts.STRINGS.ERRORS.userNotFound);
                        } if (err.status == 415) {
                            $notification.error(consts.STRINGS.ERRORS.unsupportedAvatarFormat);
                        } else {
                            $notification.error(consts.STRINGS.ERRORS.internalServerError);
                        }
                        this.busy = false;
                    });
                }
            }
        };
        this.uploadAvatar = () => {
            if (!this.busy) {
                $('#avatar').trigger('click');
            }
        };
        this.deleteAvatar = () => {
            if (!this.busy) {
                this.busy = true;
                userService.deleteAvatar({id: this.user._id}).$promise.then((response) => {
                    this.user.avatar = response.avatar;
                    this.busy = false;
                }).catch((err) => {
                    if (err == 404) {
                        $notification.error(consts.STRINGS.ERRORS.userNotFound);
                    } else {
                        $notification.error(consts.STRINGS.ERRORS.internalServerError);
                    }
                    this.busy = false;
                });
            }
            this.user.avatar = null;
        };
        this.save = (form, $event) => {
            $event.preventDefault();
            if (form.$valid && !this.busy) {
                this.busy = true;
                userService.update(this.user).$promise.then(() => {
                    $state.go('^', {page: 1, nickname: null}, {reload: true, notify: true, inherit: false});
                }).catch((err) => {
                    if (err.status == 409) {
                        $notification.error(consts.STRINGS.ERRORS.userConflictError);
                    } else {
                        $notification.error(consts.STRINGS.ERRORS.internalServerError);
                    }
                    this.busy = false;
                });
            }
        };
    }]
};

export { name, component };