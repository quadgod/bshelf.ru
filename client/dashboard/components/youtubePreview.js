const name = 'youtubePreview';
const component = {
    bindings: {
        v: '=',
        width: '=',
        height: '=',
        autoplay: '='
    },
    template: `
        <iframe ng-if="$ctrl.v" width="{{$ctrl.width}}" height="{{$ctrl.height}}" src="{{ $ctrl.getSrc($ctrl.v) }}">
        </iframe>
    `,
    controller: ['$sce', function ($sce) {
        this.autoplay = this.autoplay || 0;
        this.getSrc = (videoId) => {
            if (videoId) {
                return `https://www.youtube.com/embed/${videoId}?autoplay=${this.autoplay}`
            } else {
                return '';
            }

        };
    }]
};

export { name, component };