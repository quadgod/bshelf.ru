import _ from 'lodash';
import angular from 'angular';
import clipboard from 'clipboard-js';

const name = 'mediaSetPicker';
const component = {
    bindings: {
        addAllMode: '=',
        medias: '=',
        mimeTypes: '=',
        onChange: '&'
    },
    templateUrl: '/templates/dashboard/components/mediaSetPicker.html',
    controller: ['$uibModal', '$sce', 'mediaService', 'Notification', '$timeout',
        function($modal, $sce, $media, $notification, $timeout) {
            this.media = null;
            this._addAllMode = angular.copy(this.addAllMode);
            if (!(this._addAllMode === true || this._addAllMode === false)) {
                this._addAllMode = false;
            }

            this.preview = (item) => {
                let modal = $modal.open({
                    size: 'lg',
                    templateUrl: '/templates/dashboard/modals/media/preview.html',
                    controller: ['$scope', ($scope) => {
                        $scope.model = item;
                        $scope.cancel = () => {
                            modal.dismiss('cancel');
                        };
                    }]
                });
            };
            this.delete = (index) => {
                this.medias.splice(index, 1);
                if (this.onChange && this.onChange()) {
                    this.onChange()(this.medias);
                }
            };
            this.addFile = (media) => {
                let mediaCopy = angular.copy(media);
                $timeout(() => {
                    this.media = null;
                    let addMedia = (__media) => {
                        if (_.filter(this.medias, (m) => {
                                return m._id == __media._id;
                            }).length == 0) {
                            this.medias.push(__media);
                            if (this.onChange && this.onChange()) {
                                this.onChange()(this.medias);
                            }
                        }
                    };

                    if (_.isArray(mediaCopy)) {
                        mediaCopy.forEach(__media => {
                            addMedia(__media);
                        });
                    } else {
                        addMedia(mediaCopy);
                    }
                });
            };
            this.copyLink = (item) => {
                if (item.mimeType == 'youtube') {
                    let link = `https://www.youtube.com/watch?v=${item.fileName}`;
                    clipboard.copy(link).then(() => {
                        $notification.info(`Ссылка '${link}' скопирована в буфер обмена.`);
                    });
                } else {
                    let host = window.location.hostname;
                    let protocol = window.location.protocol;
                    let link = `${protocol}//${host}/${item.fileName}`;
                    clipboard.copy(link).then(() => {
                        $notification.info(`Ссылка '${link}' скопирована в буфер обмена.`);
                    });
                }
            };

            this.moveUp = (index) => {
                if (index > 0) {
                    this.medias.splice(index - 1, 0, this.medias.splice(index, 1)[0]);
                }
            };
            this.moveDown = (index) => {
                if (index < (this.medias.length - 1)) {
                    this.medias.splice(index + 1, 0, this.medias.splice(index, 1)[0]);
                }
            };
        }
    ]
};

export { name, component };