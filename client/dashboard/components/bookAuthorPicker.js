import * as consts from '../../../consts';

let template = `
    <div class="b-picker">
        <button class="btn btn-default"
                ng-click="$ctrl.select($event)"
                uib-tooltip="Выбрать"
                ng-if="$ctrl.model == null"
                ng-disabled="$ctrl.disabled">
            <i class="fa fa-user-plus"></i>
        </button>
        <button class="btn btn-default"
                ng-click="$ctrl.select($event)"
                ng-disabled="$ctrl.disabled"
                ng-if="$ctrl.model != null">
            <i class="fa fa-user"></i> {{ $ctrl.model.fullName }}
        </button>
    </div>
`;

let modalTemplate = `
    <div class="b-picker__modal">
        <div class="modal-header">
            <div class="row">
                <div class="col-xs-6">
                    <h5>Выбрать автора</h5>
                </div>
                <div class="col-xs-6 text-right">
                    <i class="fa fa-close b-curp" ng-click="cancel($event)"></i>
                </div>
            </div>
        </div>
        <div class="modal-body">
            <div class="row b-toolbar">
                <div class="col-xs-12 b-toolbar__col">
                    <form name="toolbarForm" ng-submit="search($event)">
                        <div class="input-group">
                            <input class="form-control"
                                   type="text"
                                   name="fullName"
                                   ng-disabled="state.busy"
                                   ng-model="state.search"
                                   placeholder="Поиск по имени">
                            <span class="input-group-btn">
                                <button ng-disabled="state.busy" type="submit" class="btn btn-toolbar" uib-tooltip="Поиск">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="b-list b-list-small b-list-stacked">
                <div class="row b-list__item" ng-repeat="author in state.authors.docs">
                    <div class="col-xs-2 b-list__item__col">
                        <div class="b-list__item__col__centered">
                            <i class="fa fa-user fa-2x b-item"></i>
                        </div>
                    </div>
                    <div class="col-xs-8 b-list__item__col">
                        <div class="b-list__item__col__lcentered">
                            <span class="b-item">Автор: <b ng-bind="author.fullName"></b></span>
                        </div>
                    </div>
                    <div class="col-xs-2 b-list__item__col">
                        <div class="b-list__item__col__rcentered">
                            <div class="b-item">
                                <button class="btn btn-default btn-sm" ng-click="select(author)" uib-tooltip="Выбрать" tooltip-placement="left">
                                    <i class="fa fa-check"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="row">
                <div class="col-xs-2 text-left">
                    <button class="btn btn-danger btn-sm"
                            uib-tooltip="Удалить"
                            ng-disabled="state.busy"
                            ng-click="unset($event)">
                        <i class="fa fa-trash"></i>
                    </button>
                </div>
                <div class="col-xs-10 text-right">
                    <button class="btn btn-default btn-sm" ng-click="prev()" ng-disabled="state.busy || state.authors.page <= 1">
                        <i class="fa fa-arrow-circle-left"></i>
                    </button>
                    <span>Страниц {{ state.authors.page }} из {{ state.authors.pages }}</span>
                    <button class="btn btn-default btn-sm" ng-click="next()" ng-disabled="state.busy || state.authors.page >= state.authors.pages">
                        <i class="fa fa-arrow-circle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
`;

const name = 'bookAuthorPicker';
const component = {
    bindings: {
        model: '=',
        disabled: '='
    },
    template: template,
    controller: [
        '$uibModal',
        '$timeout',
        'bookAuthorService',
        'Notification', function ($modal, $timeout, $authorService, $notification) {
            this.select = ($event) => {
                $event.preventDefault();
                let modal = $modal.open({
                    size: 'md',
                    template: modalTemplate,
                    controller: ['$scope', 'authors', ($scope, authors) => {
                        $scope.state = {
                            busy: false,
                            authors: authors,
                            search: null
                        };
                        $scope.search = (event) => {
                            event.preventDefault();
                            $scope.state.busy = true;
                            $authorService.query({fullName: $scope.state.search, page: 1}).$promise.then((found) => {
                                $scope.state.authors = found;
                                $scope.state.busy = false;
                            }).catch((err) => {
                                console.error(err);
                                $notification.error(consts.STRINGS.ERRORS.internalServerError);
                                $scope.state.busy = false;
                            });
                        };
                        $scope.select = (author) => {
                            event.preventDefault();
                            modal.close(author);
                        };
                        $scope.unset = (event) => {
                            event.preventDefault();
                            modal.close(null);
                        };
                        $scope.cancel = (event) => {
                            event.preventDefault();
                            modal.dismiss('cancel');
                        };
                        $scope.next = () => {
                            $scope.state.busy = true;
                            $authorService.query({
                                fullName: $scope.state.search,
                                page: $scope.state.authors.page + 1,
                                limit: 10
                            }).$promise.then((found) => {
                                $scope.state.authors = found;
                                $scope.state.busy = false;
                            }).catch((err) => {
                                console.error(err);
                                $notification.error(consts.STRINGS.ERRORS.internalServerError);
                                $scope.state.busy = false;
                            });
                        };
                        $scope.prev = () => {
                            $scope.state.busy = true;
                            $authorService.query({
                                fullName: $scope.state.search,
                                page: $scope.state.authors.page - 1,
                                limit: 10
                            }).$promise.then((found) => {
                                $scope.state.authors = found;
                                $scope.state.busy = false;
                            }).catch((err) => {
                                console.error(err);
                                $notification.error(consts.STRINGS.ERRORS.internalServerError);
                                $scope.state.busy = false;
                            });
                        };
                    }],
                    resolve: {
                        authors: [function () {
                            return $authorService.query({page: 1, limit: 10}).$promise;
                        }]
                    }
                });
                modal.result.then((author) => {
                    this.model = author;
                });
            };
        }
    ]
};

export { name, component };