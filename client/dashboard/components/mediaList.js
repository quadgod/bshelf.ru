import EntityType from '../../../server/models/EntityType';
import * as consts from '../../../consts';
import _ from 'lodash';
import clipboard from 'clipboard-js';

const name = 'mediaList';
const component = {
    bindings: {
        url: '=',
        medias: '=',
        breadcrumbs: '='
    },
    templateUrl: '/templates/dashboard/components/mediaList.html',
    controller: ['$uibModal', 'mediaService', 'Notification', '$stateParams', '$state', function ($modal, mediaService, $notification, $stateParams, $state) {
        this.parent = $stateParams.parent;
        this.search = {
            value: $stateParams.title,
            invoke: () => {
                $state.go(this.url, {
                    parent: this.parent,
                    page: 1,
                    limit: $stateParams.limit || 10,
                    title: this.search.value || null
                });
            }
        };
        this.load = () => {
            let params = {
                parent: this.parent,
                page: $stateParams.page || 1,
                limit: $stateParams.limit || 10,
                title: this.search.value
            };
            mediaService.query(params).$promise.then((medias) => {
                if (medias.docs.length > 0) {
                    this.medias = medias;
                } else {
                    if (medias.pages < medias.page) {
                        params.page = medias.pages;
                        $state.go(this.url, params);
                    } else {
                        this.medias = medias;
                    }
                }
            }).catch(() => {
                $notification.error(consts.STRINGS.ERRORS.internalServerError);
            });
        };
        this.createFolder = (parent) => {
            let modalInstance = $modal.open({
                templateUrl: '/templates/dashboard/modals/media/folder.html',
                controller: ['$scope', function ($scope) {
                    $scope.model = {
                        title: '',
                        type: EntityType.FOLDER,
                        parent: parent
                    };
                    $scope.busy = false;
                    $scope.ok = function (form) {
                        form.$setSubmitted();
                        if (form.$valid && !$scope.busy) {
                            $scope.busy = true;
                            mediaService.add($scope.model).$promise.then((response) => {
                                $scope.busy = false;
                                modalInstance.close(response.media);
                            }).catch((err) => {
                                if (err.status == 409) {
                                    $notification.error(consts.STRINGS.ERRORS.mediaFolderConflict);
                                } else {
                                    $notification.error(consts.STRINGS.ERRORS.internalServerError);
                                }
                                $scope.busy = false;
                            });
                        }
                    };
                    $scope.cancel = function () {
                        modalInstance.dismiss('cancel');
                    };
                }]
            });
            modalInstance.result.then(() => this.load());
        };
        this.uploadFile = (parent) => {
            let modalInstance = $modal.open({
                size: 'lg',
                keyboard: false,
                backdrop: 'static',
                templateUrl: '/templates/dashboard/modals/media/fileUpload.html',
                controller: ['$scope', 'FileUploader', ($scope, FileUploader) => {
                    $scope.busy = false;
                    $scope.uploader = new FileUploader({ url: `/api/media/${this.parent}/upload` });
                    $scope.uploader.filters.push({
                        name: 'mediaFilter',
                        fn: function(item, options) {
                            let type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                            if (type == '||') {
                                type = '|' + item.name.slice(item.name.lastIndexOf('.') + 1) + '|';
                            }
                            return '|jpg|png|jpeg|bmp|gif|mp4|m4v|mp3|avi|epub+zip|epub|fb2|pdf|plain|'.indexOf(type) !== -1;
                        }
                    });
                    $scope.uploader.onErrorItem = (fileItem, response, status, headers) => {
                        $notification.error(response.error);
                    };
                    $scope.cancel = () => modalInstance.dismiss('cancel');

                    $scope.youtube = {
                        input: '',
                        valid: false,
                        videoId: null,
                        add: () => {
                            $scope.busy = true;
                            let model = {
                                title: $scope.youtube.input,
                                parent: this.parent,
                                mimeType: 'youtube',
                                type: EntityType.MEDIA_FILE
                            };
                            mediaService.add(model).$promise.then((media) => {
                                $scope.busy = false;
                                modalInstance.close(media.media);
                            }).catch((err) => {
                                if (err.status == 409) {
                                    $notification.error(consts.STRINGS.ERRORS.youtubeConflictError);
                                } else {
                                    $notification.error(consts.STRINGS.ERRORS.internalServerError);
                                }
                                $scope.busy = false;
                            });
                        }
                    };

                    let inputWatchOff = $scope.$watch('youtube.input', function (newVal, oldVal) {
                        if (newVal && _.startsWith(newVal, 'https://www.youtube.com/watch?v=')) {
                            let videoId = _.trimStart(newVal, 'https://www.youtube.com/watch?v=');
                            if (!_.isEmpty(videoId)) {
                                $scope.youtube.videoId = videoId;
                                $scope.youtube.valid = true;
                            } else {
                                $scope.youtube.videoId = null;
                                $scope.youtube.valid = false;
                            }
                        } else {
                            $scope.youtube.videoId = null;
                            $scope.youtube.valid = false;
                        }
                    });

                    $scope.$on('$destroy', function () {
                        inputWatchOff();
                    });
                }]
            });
            modalInstance.result.then(() => this.load()).catch(() => this.load());
        };
        this.copyLink = (item) => {
            if (item.mimeType == 'youtube') {
                let link = `https://www.youtube.com/watch?v=${item.fileName}`;
                clipboard.copy(link).then(() => {
                    $notification.info(`Ссылка '${link}' скопирована в буфер обмена.`);
                });
            } else {
                let host = window.location.hostname;
                let protocol = window.location.protocol;
                let link = `${protocol}//${host}${item.fileName}`;
                clipboard.copy(link).then(() => {
                    $notification.info(`Ссылка '${link}' скопирована в буфер обмена.`);
                });
            }
        };
        this.preview = (item) => {
            if (item.type == EntityType.FOLDER) {
                $state.go(this.url, {id: item._id});
            } else {
                let modalInstance = $modal.open({
                    size: 'lg',
                    templateUrl: '/templates/dashboard/modals/media/preview.html',
                    controller: ['$scope', ($scope) => {
                        $scope.model = item;
                        $scope.cancel = () => {
                            modalInstance.dismiss('cancel');
                        };
                    }]
                });
            }
        };
        this.delete = (item) => {
            let modalInstance = $modal.open({
                templateUrl: '/templates/dashboard/modals/removeConfirm.html',
                controller: ['$scope', function ($scope) {
                    $scope.busy = false;
                    $scope.context = item.title;
                    $scope.delete = function () {
                        $scope.busy = true;
                        mediaService.delete({action: item._id}).$promise.then(() => {
                            $scope.busy = false;
                            modalInstance.close(item);
                        }).catch((err) => {
                            switch (err.status) {
                                case 400:
                                    $notification.error(consts.STRINGS.ERRORS.badRequest);
                                    break;
                                case 403:
                                    if (item.type == EntityType.FOLDER) {
                                        $notification.error(consts.STRINGS.ERRORS.unableToDeleteFolderHasChild);
                                    } else {
                                        $notification.error(consts.STRINGS.ERRORS.unableToDeleteHasRefs);
                                    }
                                    break;
                                case 404:
                                    $notification.error(consts.STRINGS.ERRORS.elementNotFound);
                                    break;
                                default:
                                    $notification.error(consts.STRINGS.ERRORS.internalServerError);
                                    break;
                            }

                            $scope.busy = false;
                        });
                    };
                    $scope.cancel = function () {
                        modalInstance.dismiss('cancel');
                    };
                }]
            });

            modalInstance.result.then(() => this.load());
        };
    }]
};

export { name, component };