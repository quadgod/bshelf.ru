const name = 'userService';
const service = ['$resource', function ($resource) {
    return $resource('/api/user/:action', {}, {
        'query':                {method: 'GET'},
        'current':              {method: 'GET',     url: '/api/user/current'},
        'getById':              {method: 'GET',     params: {id: '@_id'}, url: '/api/user/:id'},
        'resetPassword':        {method: 'PATCH',    params: {id: '@_id'}, url: '/api/user/:id/reset-password'},
        'add':                  {method: 'POST'},
        'update':               {method: 'PUT',     params: {action: '@_id'}},
        'uploadAvatar':         {method: 'POST',    url: '/api/user/avatar', headers: { 'Content-Type': undefined }},
        'deleteAvatar':         {method: 'DELETE',  url: '/api/user/:id/avatar', params: {id: '@_id'}},
        'registration':         {method: 'POST', params: {action: 'registration'}},
        'login':                {method: 'POST', params: {action: 'login'}},
        'restorePassword':      {method: 'POST', params: {action: 'restore-password'}},
        'resetPasswordByToken': {method: 'POST', params: {action: 'reset-password'}}
    });
}];

export { name, service };