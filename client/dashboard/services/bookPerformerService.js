const name = 'bookPerformerService';
const service = ['$resource', function ($resource) {
    return $resource('/api/book/performer/:action', {}, {
        'query':        {method: 'GET',     params: {action: '@parent'}},
        'getById':      {method: 'GET',     params: {id: '@_id'}, url: '/api/book/performer/:id'},
        'add':          {method: 'POST'},
        'delete':       {method: 'DELETE',  params: {action: '@_id'}},
        'update':       {method: 'PUT',     params: {action: '@_id'}}
    });
}];

export { name, service };