const name = 'bookService';
const service = ['$resource', function ($resource) {
    return $resource('/api/book/:action', {}, {
        'playlist':     {method: 'GET',     url: '/api/book/:id/playlist', params: {id: '@id'}, isArray: true},
        'breadcrumbs':  {method: 'GET',     params: {parent: '@parent'}, url: '/api/book/breadcrumbs', isArray: true},
        'query':        {method: 'GET',     params: {action: '@parent'}},
        'getById':      {method: 'GET',     params: {id: '@_id'}, url: '/api/book/:id'},
        'add':          {method: 'POST'},
        'delete':       {method: 'DELETE',  params: {action: '@_id'}},
        'update':       {method: 'PUT',     params: {action: '@_id'}}
    });
}];

export { name, service };