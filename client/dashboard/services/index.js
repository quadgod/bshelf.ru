import angular from 'angular';
import * as EntityService from './entityService';
import * as MediaService from './mediaService';
import * as BookService from './bookService';
import * as BookAuthorService from './bookAuthorService';
import * as BookPerformerService from './bookPerformerService';
import * as UserService from './userService';

let services = angular.module('app.services', []);
services.service(EntityService.name, EntityService.service);
services.service(MediaService.name, MediaService.service);
services.service(BookService.name, BookService.service);
services.service(BookAuthorService.name, BookAuthorService.service);
services.service(BookPerformerService.name, BookPerformerService.service);
services.service(UserService.name, UserService.service);