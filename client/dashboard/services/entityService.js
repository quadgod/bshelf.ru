const name = 'entityService';
const service = ['$resource', function ($resource) {
    return $resource('/api/entity/:action', {}, {
        'breadcrumbs':  {method: 'GET',     params: {parent: '@parent'}, url: '/api/entity/breadcrumbs', isArray: true},
        'query':        {method: 'GET',     params: {action: '@parent'}},
        'getById':      {method: 'GET',     params: {parent: '@parent', id: '@_id'}, url: '/api/entity/:id'},
        'add':          {method: 'POST'},
        'delete':       {method: 'DELETE',  params: {action: '@_id'}},
        'update':       {method: 'PUT',     params: {action: '@_id'}}
    });
}];

export { name, service };