const name = 'bookAuthorService';
const service = ['$resource', function ($resource) {
    return $resource('/api/book/author/:action', {}, {
        'query':        {method: 'GET',     params: {action: '@parent'}},
        'getById':      {method: 'GET',     params: {id: '@_id'}, url: '/api/book/author/:id'},
        'add':          {method: 'POST'},
        'delete':       {method: 'DELETE',  params: {action: '@_id'}},
        'update':       {method: 'PUT',     params: {action: '@_id'}}
    });
}];

export { name, service };