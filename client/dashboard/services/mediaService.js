const name = 'mediaService';
const service = ['$resource', function ($resource) {
    return $resource('/api/media/:action', {}, {
        'breadcrumbs':  {method: 'GET',     params: {parent: '@parent'},        url: '/api/media/breadcrumbs', isArray: true},
        'getAll':       {method: 'GET',     params: {parent: '@parent'},        url: '/api/media/:parent/all', isArray: true},
        'query':        {method: 'GET',     params: {action: '@parent'}},
        'add':          {method: 'POST'},
        'delete':       {method: 'DELETE',  params: {action: '@_id'}}
    });
}];

export { name, service };