const path = require('path');
const webpack = require('webpack');
let ExtractTextPlugin = require('extract-text-webpack-plugin');

function isExternal(module) {
    let userRequest = module.userRequest;
    if (typeof userRequest !== 'string') {
        return false;
    }
    return userRequest.indexOf('bower_components') >= 0 || userRequest.indexOf('node_modules') >= 0;
}

module.exports = {
    entry: {
        'dashboard': './client/dashboard/app.js',
        'app': './client/web/app.js'
    },
    output: {
        path: path.join(__dirname, 'public/build'),
        filename: '[name].js'
    },
    devtool: 'source-map',
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015', 'stage-0']
                }
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract('css-loader!resolve-url!sass-loader?sourceMap', { publicPath: '/build/'})
            },
            {
                test: /\.png|\.jpe?g|\.gif$/,
                loader: 'file-loader?pubicPath=/build/images&name=./images/[hash].[ext]'
            },
            {
                test: /\.woff2?$|\.ttf$|\.eot$/,
                loader: 'file-loader?pubicPath=/build/fonts&name=./fonts/[hash].[ext]'
            },
            {
                test: /\.svg$/,
                loader: 'file-loader?pubicPath=/build/svg&name=./svg/[hash].[ext]'
            }
        ]
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: 'app.vendor',
            chunks: ['app'],
            minChunks: function(module) {
                return isExternal(module);
            }
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'dashboard.vendor',
            chunks: ['dashboard'],
            minChunks: function(module) {
                return isExternal(module);
            }
        }),
        new ExtractTextPlugin('css/[name].css', {
            allChunks: true
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        })
    ]
};